<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------
namespace Wechat\Logic;

use Wechat\Event\BaseEvent;
/**
 * 缓存回复类
 * 针对有缓存回复需求的板块进行缓存的进一步处理
 * 比如活动的报名、建议的回复
 * @author Administrator
 */
class CacheLogic extends BaseEvent {

    /**
     * 检查openId的缓存，并根据缓存中指定的模块控制器及方法传入缓存中存储的参数
     * 存储所需的数据格式实例：
     * array(
     * 'action' => array(
     * 'controller' => "Admin/Cache,Event" , //需要后续处理的控制器及命名空间
     * 'method' => 'reply' , //需要后续处理的公共方法
     * ) ,
     * 'needs' => array( //需要的数据
     * 'action_name' => 'listCache', //必须有，这是进入上述公共方法后必须要做的下一步方法名称
     * 'keywords' => array_keys($listCache),  //可选 需要的相关数据，这里展示是keywords，你可以自定义键值和键名
     * ..........  //可以缓存更多内容，只要是你需要在后续处理中使用到的
     * ) ,
     *    )
     *
     * @param string $openid    微信粉丝openid
     * @param string $keyword   如果是文本消息流程进来的需要传入客户发送的关键字
     *
     * @author Tealun Du
     * @return bool
     */
    public function cacheReply($openid , $keyword = '') {

        $data = S($openid);

        //对缓存处理控制器为本类自身控制器的，不用提取action_name
        if($data['action']['controller'] ==  "Cache,Logic" ){
            $method = $data['action']['method'];//读取缓存中需要的处理方法名
            $param = $data['needs'];
            $param['openid'] = $openid;
            $param['keyword'] = $keyword;

            $reply = $this->$method($param);
            if(!$reply){
                return false;
            }
            return $reply;
        }

        //提取缓存中存储的处理流程和方法
        $action = str2arr($data['action']['controller']); //读取缓存中需要的处理控制器和类名
        $method = $data['action']['method'];//读取缓存中需要的处理方法名

        //将消息体内的OPENID 和 keyword 传入所需数据中
        $data['needs']['openid'] = $openid;
        $data['needs']['keyword'] = $keyword;

        //调用指定的处理方法处理缓存
        $reply = A($action[0] , $action[1])->$method($data['needs']);

        //没有回复内容时返回false
        if(!$reply){
            return false;
        }

        return $reply;

    }


    /**
     * 回复缓存的图文数据
     *
     * @param       array           $param      缓存的必要数据
     * @return      array|bool
     * @internal    param   string  $openid     客户openid
     * @internal    param   string  $keyword    客户发送的关键词
     * 本方法需要缓存的图文信息数据，数据存储在缓存数组的['news']键名中
     */
    public function newsCache($param) {

        $keyword = $param['keyword'];
        $keywords = str2arr($param['keywords']);

        if(!in_array($keyword,$keywords)){
            return false;
        }

        $openid = $param['openid'];
        $cache = S($openid);
        $news = $cache['news']; //读取缓存的图文数据
        return set_response_arr($news,"news"); //将缓存图文回复给客户
    }

    /**
     * 转接客服
     * 客户确认后转接到客服（随机指派客服帐号）
     *
     * @param array $param 缓存的必要数据
     *                       本方法直接调用set_response_arr()函数将消息转发到多客服
     */
    public function serviceCache($param) {
        S($param['openid'] , NULL); //清除客户缓存
        return set_response_arr($param['service']?$param['service']:'',$param['service']?'setService':'service'); //消息流转发到多客服
    }
}