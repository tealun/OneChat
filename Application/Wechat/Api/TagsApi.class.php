<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Wechat\Api;

use Wechat\Api\Api;

class TagsApi extends Api {
    /**
     * 构造方法，实例化操作模型
     */
    protected function _init() {
    }

    public function getTags() {
        return get_wechat_tags();
    }

}
