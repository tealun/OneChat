<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Wechat\Api;

use Wechat\Api\Api;
use Wechat\Model\TchatQrcodeModel;

class QrcodeApi extends Api {
    /**
     * 构造方法，实例化操作模型
     */
    protected function _init() {
        $this->model = new TchatQrcodeModel();
    }

    /**
     * 创建一个二维码
     *
     * @param        $module
     * @param string $scene
     * @param string $expireSeconds
     *
     * @return mix 根据临时或永久二维码不同要求，返回ticket或存储的id
     */
    public function create($module , $scene='' , $expireSeconds = '') {
        if ( !empty( $expireSeconds ) ) {//准备临时二维码需要的数据
            $sceneArr['expire_seconds'] = $expireSeconds;
            $sceneArr['action_name'] = "QR_SCENE";
            //使用一个大于1000000小于最大32位整型非0数之间的随机数作为scene_id
            $randId = rand(1000000,4294967295);
            $sceneArr['action_info'] = array( 'scene' => array( 'scene_id' => $randId ) );
        } else {//准备永久二维码需要的数据及要存储到数据库的数据
            $data['scene'] = $scene;//场景名称，用于后台查看
            $data['module'] = $module;
            $data['ticket'] = $module . time();//ticket未获取暂时用不重复的字符串占位
            $data['create_time'] = time();
            $data['action_name'] = "QR_LIMIT_SCENE";
            $id = $this->model->add($data);//新建一个二维码条目，获得id去获得ticket
            $id == false && $this->error('新创建二维码失败');
            /* 用id作为scene_id获取ticket */
            $sceneArr['action_name'] = "QR_LIMIT_SCENE";
            $sceneArr['action_info'] = array( 'scene' => array( 'scene_id' => $id ) );
        }

        //创建二维码ticket
        $ticket = get_qr_ticket($sceneArr);

        if ( false !=$ticket ) {//判断ticket键值是否存在来判断是否获取成功

            if(!empty($expireSeconds)){//如果是要创建临时二维码直接返回ticket
                return $ticket;

            }else{//如果是要创建永久二维码，更新刚刚创建的二维码条目ID的Ticket
                $data['id'] = $id;//成功后将ticket写入刚刚生成的条目
                $data['ticket'] = $ticket;
                $data['update_time'] = time();
                $re = $this->model->save($data);
                $re === 0 && $this->error('更新新创建的二维码失败');
                return $id;
            }

        } else {//返回出错信息
            $this->getError();
        }
    }

    /**
     * 获取二维码条目详情
     *
     * @param $id  int Qrcode Id 或者 ticket 字符串
     *
     * @return array 该条目的数组
     */
    public function getInfo($id) {
        return $this->model->getInfo($id);
    }


    /**
     * 获取出错信息
     * @return mixed
     */
    public function getError() {
        return $this->model->getError();
    }

    /**
     * 二维码扫描数+1
     *
     * @param $ticket 二维码TICKET
     */
    public function qrcodePlusOne($ticket) {
        $this->model->qrcodePlusOne($ticket);
    }

}
