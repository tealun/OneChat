<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Wechat\Api;
use Think\Controller;
define( 'WECHAT_MODEL_PATH' , dirname(dirname(__FILE__)) );

//载入配置文件
require_cache(WECHAT_MODEL_PATH . '/Conf/config.php');

//载入函数库文件
require_cache(WECHAT_MODEL_PATH . '/Common/wechat.php');

/**
 * Wechat API调用控制器层
 * 调用方法 A('Wechat/Qrcode', 'Api')->getInfo($id);
 */
abstract class Api extends Controller{

    /**
     * API调用模型实例
     * @access  protected
     * @var object
     */
    protected $model;

    /**
     * 构造方法，检测相关配置
     */
    public function __construct() {
        /* 读取站点配置 */
        $config = api('Config/lists');
        C($config); //添加配置

        //相关配置检测
        C('WECHAT_GHID') || E('微信配置错误：缺少WECHAT_GHID公众号原始账号');
        C('WECHAT_NAME') || E('微信配置错误：缺少WECHAT_NAME公众号账号');
        C('WECHAT_NICKNAME') || E('微信配置错误：缺少WECHAT_NICKNAME公众号昵称');
        C('WECHAT_TOKEN') || E('微信配置错误：缺少WECHAT_TOKEN公众号对接代码');
        C('WECHAT_APP_ID') || E('微信配置错误：缺少WECHAT_APP_ID公众号对接授权ID');
        C('WECHAT_APP_SECRET') || E('微信配置错误：缺少WECHAT_APP_SECRET公众号对接授权密钥');
        C('WECHAT_ACCOUNT_RZ') || E('微信配置错误：缺少WECHAT_ACCOUNT_RZ公众号账号认证状态');
        $this->_init();
    }

    /**
     * 抽象方法，用于设置模型实例
     */
    abstract protected function _init();

}
