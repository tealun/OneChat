<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Wechat\Api;

use Wechat\Api\Api;

class MenuApi extends Api {
    /**
     * 构造方法，实例化操作模型
     */
    protected function _init() {

    }

    public function setMenu($data) {
        return set_wechat_menu($data);
    }

    public function getMenu(){
        return get_wechat_menu();
    }

    public function addConditionalMenu($data){
        return add_conditional_menu($data);
    }

    public function delConditionalMenu($menuid){
        return del_conditional_menu($menuid);
    }

    public function delAllMenu(){
        return del_wechat_menu();
    }


}
