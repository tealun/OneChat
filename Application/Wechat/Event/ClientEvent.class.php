<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------
namespace Wechat\Event;
use Wechat\Model\TchatClientModel;
class ClientEvent extends BaseEvent {
    
    private $Client;
    
    function __construct(){
        $this -> Client = new TchatClientModel();
        parent::__construct();
    }

    public function update($openId , $eventKey , $ticket) {
        
        if ( check_wechat_rz() ) {
            //如果是认证服务号，更新一次客户的详细信息
            $data = get_client_info($openId);
            $clientId = $this->Client->getClientId($openId);
            if ( $clientId ) {//如果有存储，则设置ID值
                $data['id'] = $clientId;
            }

            if ( !empty( $ticket ) ) {//检测tiket是否为空
                $data['event_key'] = $eventKey; //事件KEY值，qrscene_为前缀，后面为二维码的参数值 本系统中其实就是二维码条目ID
            }

        } else {//公众号未认证且未存储过客户信息的情况下只存储openid
            $data['openid'] = $openId;
        }

        $data['subscribe'] = '1';
        $data['subscribe_time'] = time();

        return $this->Client->update($data);
    }


    /**
     * 获取客户条目详情
     *
     * @param $id  int Id 或者 openId 字符串
     *
     * @return array 该条目的数组
     */
    public function getInfo($id) {
        return $this->Client->detail($id);
    }

    /**
     * 获取客户Id
     *
     * @param $id  int Qrcode Id 或者 ticket 字符串
     *
     * @return array 该条目的数组
     */
    public function getClientId($openId) {
        return $this->Client->getClientId($openId);
    }

    /**
     * 绑定帐号
     *
     * @param string $openid    关注帐号的openid
     * @param array  $data      本方法需要的数据，绑定帐号用临时二维码,所以一般是缓存的tiket中的数据.
     *                          array('uid'=>uid)
     *
     * @return array
     */
    public function bind($openid,$data){
        $message = $this->Client->bindUser($openid,$data['uid'],$data['ticket']);
        return set_response_arr($message);
    }

    /**
     * 获取出错信息
     * @return mixed
     */
    public function getError() {
        return $this->Client->getError();
    }

    /**
     * 取消关注
     *
     * @param $openId str 客户openId
     */
    public function unSubscribe($openId) {
        $this->Client->unSubscribe($openId);
    }
}