<?php
namespace Wechat\Event;
use Wechat\Model\TchatQrcodeModel;
class QrcodeEvent extends BaseEvent {

    /**
     * 根据不同的二维码用途，返回不同的数据格式
     *
     * @param string $openid 客户的openId
     * @param string $ticket 二维码ticket
     *
     * @return array
     */
    public function qrcodeReply($openid , $ticket) {

        if($s = S($ticket)) {//查看有么有缓存该ticket，用于临时二维码的缓存时效处理
            //判断是否是临时二维码
            if ( $s['action_name'] == 'QR_SCENE' ) {
                //执行临时二维码中缓存的方法名，并将用户openid和缓存的需要的数据传入
                return A($s['module'].'/'.$s['controller'] ,$s['mdr'])->$s['action']($openid , $s['data']);
            }
        }else{
            $Qrcode = new TchatQrcodeModel();
            $info = $Qrcode->getInfo($ticket);

            //没有查到该二维码数据时的回复内容
            if(!$info){
                //到事件设置中查找“扫描二维码”时的默认回复流程
                $map['id'] = array('eq',2);//设置回复条目查找规则
                return A('Wechat/Reply' , 'Event')->reply('TchatEvents',$map,$openid);
            }

            //查找到二维码的回复内容
            $Qrcode->qrcodePlusOne($ticket);
            $map['id'] = array('eq',$info['id']);//设置回复条目查找规则

            return A('Wechat/Reply' , 'Event')->reply('TchatQrcode',$map,$openid);

        }
    }

}