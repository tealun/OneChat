<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------
namespace Wechat\Event;

use Think\Controller;
define( 'WECHAT_MODEL_PATH' , dirname(dirname(__FILE__)) );
//载入配置文件
require_cache(WECHAT_MODEL_PATH . '/Conf/config.php');
//载入函数库文件
require_cache(WECHAT_MODEL_PATH . '/Common/wechat.php');
/**
 * 客户消息处理类基类
 */
class BaseEvent extends Controller{

}
