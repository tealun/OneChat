<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------
namespace Wechat\Event;
/**
 * 客户事件类型消息处理类
 */
class EventEvent extends BaseEvent {

    /**
     * 微信事件推送消息处理
     *
     * @param string    $openId     客户唯一openid
     * @param string    $event      事件类型
     * @param array     $eventData  事件附带属性
     *
     * @return array
     *
     * @internal param string   $eventKey       事件KEY值
     * @internal param string   $ticket         二维码的ticket，可用来换取二维码图片
     *
     * @author Tealun Du
     */
    public function eventHandle($openId , $event , $eventData) {

        $eventKey = $eventData['EventKey'];
        $ticket = $eventData['Ticket'];

        switch ( $event ) {

            /* 关注事件 */
            case 'subscribe' :

                //更新客户资料
                A('Wechat/Client' , 'Event')->update($openId , $eventKey , $ticket);

                //判断是否是认证服务号，并且有二维码的ticket
                if ( check_wechat_rz() && !empty( $ticket ) ) {//扫描带参数的二维码关注时回复
                    return A('Wechat/Qrcode' , 'Event')->qrcodeReply($openId , $ticket); //判断条件成立则返回对应二维码的回复内容
                }

                break;

            /* 取消关注事件 */
            case 'unsubscribe' :
                //取消掉关注
                A('Wechat/Client' , 'Event')->unSubscribe($openId);
                return set_response_arr('');

            /* 扫描事件 */
            case 'SCAN' :

                //判断是否是认证服务号，并且有二维码的ticket
                if ( check_wechat_rz() && !empty( $ticket ) ) {

                    return A('Wechat/Qrcode' , 'Event')->qrcodeReply($openId , $ticket); //判断条件成立则返回对应二维码的回复内容
                }
                break;

            /* 自定义菜单点击事件 */
            case 'CLICK' :

                // 获取对应资源
                $id = M('Tchat_menu')->where('`event_key` = "' . $eventKey . '"')->getField('id');

                if ( !$id ) {
                    $content = "菜单配置错误，请稍后重试。";

                    if ( check_wechat_service()  ) {//检测是否开启多客服，开启则提示可联系客服
                        $content .= "\n--------------------\n是否转接到在线客服咨询?\n回复“1”或“是”立刻转接\n(1分钟内有效)";

                        /*转接客服缓存*/
                        S($openId , array(
                            'action' => array(
                                'controller' => "Cache,Logic" , //需要后续处理的控制器及命名空间
                                'method' => 'serviceCache' , //需要后续处理的公共方法
                            ) ,
                            'needs' => array(
                                'keyword' => "1,是" , //取缓存数组的键名作为关键字数组
                                'params' => '' //传入到上述方法的公共参数
                            )
                        ) , 60);
                    }

                    return set_response_arr($content);
                } else {

                    $map = array('id'=>$id);

                    return A('Wechat/Reply' , 'Event')->reply('TchatMenu',$map,$openId);
                }
                break;

            case 'user_get_card':
            case 'user_gifting_card':
            case 'user_del_card':
            case 'User_pay_from_pay_cell':
            case 'user_view_card':
            case 'user_enter_session_from_card':
            case 'update_member_card':

                return A('Wechat/Card' , 'Event')->cardReply($event,$eventData);
                break;
            /* 模版消息发送成功事件 */
            case 'TEMPLATESENDJOBFINISH' :
                $re['openid'] = $eventData['FromUserName'];
                $re['createTime'] = $eventData['CreateTime'];
                $re['messageId'] = $eventData['MsgID'];
                $re['status'] = $eventData['Status'];
                A('Wechat/Template' , 'Event')->templateFinish($re);
                exit;
            default:
                return set_response_arr("系统正在开发本接口，敬请关注。");

        }
        //根据EVENT值找到EVENT表中相应的回复类型

        $map = array( 'event_type' => $event );

        return A('Wechat/Reply' , 'Event')->reply('TchatEvents',$map,$openId);
    }

}
