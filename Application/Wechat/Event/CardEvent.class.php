<?php
namespace Wechat\Event;

class CardEvent extends BaseEvent {

    public function cardReply($event,$eventData){
        switch ($event){
            case 'user_get_card':
                $content = "主人~卡券领取成功啦，请您注意使用时效及使用要求哦。";
                return set_response_arr($content);
                break;

            case 'user_gifting_card':
                $content = "报告主人~卡券转赠成功啦，请注意提醒您的好友接收哦。";
                return set_response_arr($content);
                break;

            case 'user_del_card':
                $content = "好遗憾啊，主人您刚刚删除了我们的卡券，是我们做的不够好吗？\n您可以将您的意见或建议发送给我们哦，我们会珍视您的每一次反馈。";
                return set_response_arr($content);
                break;

            case 'User_pay_from_pay_cell':
                $content = "太棒了~主人您已成功付款！\n好期待好期待您的下次光临呢。";
                return set_response_arr($content);
                break;

            case 'user_view_card':
                $content = "主人，您知道吗？您的会员卡后宫佳丽三千，能被您瞅这么一眼，我心里好鸡冻呢。";
                return set_response_arr($content);
                break;

            case 'user_enter_session_from_card':
                $content = "主人~您来看我啦？我是您的忠实小卡，记得常来哦。";
                return set_response_arr($content);
                break;

            case 'update_member_card':
                $content = "主人注意哦，您的会员卡有了新的变动。";
                return set_response_arr($content);
                break;

            default:
                return false;
        }
    }

}