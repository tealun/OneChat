<?php
namespace Wechat\Event;

class LocationEvent extends BaseEvent {

    /**
     * 对位置类型消息进行处理
     *
     * @param $openId  客户openid
     * @param $location 图片ID
     */
    public function locationHandle($openId , $location) {
        return set_response_arr('已经收到您的位置信息');
    }
}