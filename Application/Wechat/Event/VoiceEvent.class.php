<?php
namespace Wechat\Event;

class VoiceEvent extends BaseEvent{


    /**
     * 对语音类型消息进行处理
     *
     * @param $openId  客户openid
     * @param $mediaId 图片ID
     */
    public function voiceHandle($openId , $mediaId) {
        return set_response_arr('已经收到您的语音信息');
    }
}