<?php
namespace Wechat\Event;

class ReplyEvent extends BaseEvent {

    /**
     * 回复内容
     * 对传入数组根据回复类型进行判断，并从不同的内容模型中获取回复资源，整合后返回
     */
    public function reply($model,$map,$openid) {

        $reply = M($model)->where($map)->find();

        if(!$reply){
            return set_response_arr('对不起，该项回复配置有问题，请联系管理员。');
        }

        $flow = M('TchatReplyFlow')->where('`id` ='.$reply['flow_id'])->find();

        if(!$flow){
            return set_response_arr('对不起，没有查找到该项目配置中回复的模版。');
        }

        $flow['action_data'] = $reply['action_data'];
        $flow['openid'] = $openid;
        //分发到对应的模块操作
        return A($flow['module'].'/'.$flow['controller'],$flow['layer'])->reply($flow);

    }
}