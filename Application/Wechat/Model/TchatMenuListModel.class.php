<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.dutous.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: TalunDu <tealun@tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Model;

use Think\Model;

/**
 * 自定义菜单模型
 */
class TchatMenuListModel extends Model {

    /* 自动验证规则 */
    protected $_validate = array(
        array( 'name' , 'checkName' , '标识已经存在' , self::VALUE_VALIDATE , 'callback' , self::MODEL_BOTH ) ,
        array( 'name' , 'require' , '标识不能为空' , self::MUST_VALIDATE , 'regex' , self::MODEL_BOTH ) ,
        array( 'title' , 'require' , '名称不能为空' , self::MUST_VALIDATE , 'regex' , self::MODEL_BOTH ) ,
    );

    /* 自动完成规则 */
    protected $_auto = array(
        array( 'name' , 'htmlspecialchars' , self::MODEL_BOTH , 'function' ) ,
    );

    /**
     * 获取菜单列表详细信息
     * 源自分类模型
     *
     * @param  int   $menuid    菜单列表的MENUID或标识
     * @param  boolean $field 查询字段
     *
     * @return array     菜单信息
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function info($menuid , $field = true) {
        /* 获取菜单信息 */
        $map = array();
        if ( is_numeric($menuid) ) { //通过ID查询
            $map['menuid'] = $menuid;
        } else { //通过标识查询
            $map['name'] = $menuid;
        }

        return $this->field($field)->where($map)->find();
    }

}
