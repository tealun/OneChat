<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.dutous.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: TalunDu <tealun@tealun.com>
// +----------------------------------------------------------------------

namespace Wechat\Model;

use Think\Model;

/**
 * 带参数二维码模型
 */
class TchatQrcodeModel extends Model {

    /* 自动验证规则 */
    protected $_validate = array( array( 'ticket' , 'checkTicket' , '该二维码已经存在' , self::VALUE_VALIDATE , 'callback' , self::MODEL_BOTH ) );

    /* 自动完成规则 */
    protected $_auto = array(
        array( 'create_time' , 'getCreateTime' , self::MODEL_INSERT , 'callback' ) ,
        array( 'update_time' , 'NOW_TIME' , self::MODEL_INSERT , 'function' ) ,
    );

    /**
     * 新增或更新二维码
     *
     * @param array $data 手动传入的数据
     *
     * @return boolean fasle 失败 ， int  成功 返回新增数据ID
     * @author huajie <banhuajie@163.com>
     */
    public function update($data = NULL) {
        /* 获取数据对象 */
        $data = $this->create($data);

        if ( empty( $data ) ) {
            return false;
        }

        /* 添加或新增基础内容 */
        if ( empty( $data['id'] ) ) { //新增数据
            $id = $this->add(); //添加二维码条目
            if ( !$id ) {
                $this->error = '新增二维码出错！';

                return false;
            }
        } else { //更新数据
            $status = $this->save();
            if ( false === $status ) {
                $this->error = '更新二维码出错！';

                return false;
            } else {
                $id = $data['id'];
            }
        }

        return $id;
    }

    /**
     * 根据ID或TICKET数组来批量查询二维码信息
     *
     * @param $id  查询字段值
     * @param $arr 字段值数组
     */
    public function getInfo($id , $field = true) {

        /* 根据传入的ID值的数据类型判定查询字段名称 */
        if ( is_numeric($id) ) {
            $map['id'] = $id;
        } else {
            $map['ticket'] = $id;
        }
        /* 根据条件返回查询数据 */
        if ( $map ) {
            return $this->where($map)->field($field)->find();
        } else {
            return false;
        }

    }

    /**
     * 二维码扫描次数+1
     *
     * @param $ticket
     */
    public function qrcodePlusOne($ticket) {
        $this->getByTicket($ticket);
        $this->scan_num++;
        $this->save();
    }

    /**
     * 创建时间不写则取当前时间
     * @return int 时间戳
     * @author huajie <banhuajie@163.com>
     */
    protected function getCreateTime() {
        $create_time = I('post.create_time');

        return $create_time ? strtotime($create_time) : NOW_TIME;
    }


    /**
     * 检查ticket是否已存在
     * @return true无重复，false已存在
     */
    protected function checkTicket() {
        $ticket = I('post.ticket');
        $map = array( 'ticket' => $ticket );
        $res = $this->where($map)->getField('id');
        if ( $res ) {
            return false;
        }

        return true;
    }

}
