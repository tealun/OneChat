<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Wechat\Model;

use Think\Model;

/**
 * 客户资料模型
 */
class TchatClientModel extends Model {

    /* 自动验证规则 */
    protected $_validate = array();

    /* 自动完成规则 */
    protected $_auto = array(

    );

    /**
     * 获取客户详细信息
     *
     * @param string $openId 客户openId
     * @param array  $field  想要查询的字段数组
     */
    public function detail($id , $field = true) {
        if ( is_numeric($id) ) {
            $map['id'] = $id;
        } else {
            $map['openid'] = $id;
        }

        $info =  $this->where($map)->field($field)->find();

        if(!$info && !is_numeric($id)){
            $info = get_client_info($id);
            $re = $this->update($info);
            if($re){
                $info =  $this->where($map)->field($field)->find();
            }
        }

        return $info;
    }

    /**
     * 根据用户UID获取绑定的openid
     * @param $uid
     * @return mixed
     */
    public function getOpenidByUid($uid){
        $map['uid']=$uid;
        return $this->where($map)->getField('openid');
    }

    /**
     * 根据用户id获取绑定的openid
     * @param $id
     * @return mixed
     */
    public function getOpenidById($id){
        if(is_array($id)){
            $map['id']=array('in',$id);
            return $this->where($map)->getField('openid',true);
        }elseif(is_numeric($id)){
            $map['id']=$id;
            return $this->where($map)->getField('openid');
        }else{
            return $id;
        }

    }

    /**
     * 根据用户绑定的openid获取id
     * @param $id
     * @return mixed
     */
    public function getIdByOpenid($openid){
        if(is_array($openid)){
            $map['openid']=array('in',$openid);
            $id = $this->where($map)->getField('id',true);
        }else{
            $map['openid']=array('eq',$openid);
            $id = $this->where($map)->getField('id');
        }
        return $id;
    }

    /**
     * 根据ID更新现有客户资料
     *
     * @param $id
     * @param $data
     */
    public function update($data = NULL) {


        if(is_array($data['tagid_list'])){
            $data['tagid_list'] = arr2str($data['tagid_list']);
        }

        /* 获取数据对象 */
        $data = $this->create($data);
        if ( empty( $data ) ) {
            $this->error = '数据为空！';

            return $this->getError();
        }

        /* 添加或新增基础内容 */
        if ( empty( $data['id'] ) ) { //新增数据
            $id = $this->add();
            if ( !$id ) {
                $this->error = '新增客户出错！';

                return $this->getError();
            } else {
                return $id;//新增正确返回id
            }
        } else { //更新数据
            $status = $this->save(); //更新基础内容
            if ( false === $status ) {
                $this->error = '更新客户出错！';

                return $this->getError();
            } else {
                return $status;//更新正确返回影响条目数
            }
        }
    }


    /**
     * 获取客户ID
     * 如果不存在，则根据openId新建客户资料
     * 如果公众帐号通过了微信认证则提取客户在微信服务器上的资料存储到本地
     * 没有认证的帐号则忽略提取，直接存储openId
     * 因此开发时如果涉及到存储客户信息或者查看客户信息，最好是先通过openId调用本方法查找对应的客户ID，然后再进行查看或存储
     * 这样会自动从微信服务器上下载更新客户资料并返回ID值
     *
     * @param string $openId
     */
    public function getClientId($openId) {
        /* 查看客户是否已存在 */
        $id = $this->where(array( 'openid' => $openId ))->getField('id');

        /* 客户不存在时 */
        if ( !$id ) { //未认证情况下，仅存储客户openid
            $data = array(
                'openid' => $openId ,
            );

            /* 查看认证情况 */
            if ( check_wechat_rz() ) $data = get_client_info($openId);//已认证则从服务器获取该客户资料
            $this->create($data);//创建客户数据
            $id = $this->add();//新增数据
        }

        return $id;
    }

    /**
     * 取消关注
     *
     * @param $id
     */
    public function unSubscribe($id) {
        if ( is_numeric($id) ) {
            $map['id'] = $id;
        } else {
            $map['openid'] = $id;
        }
        $data['subscribe'] = 0;
        $this->where($map)->save($data);
    }

    /**
     * 绑定userId
     * @param $openid
     * @param $uid
     * @param $ticket
     * @return string
     */
    public function bindUser($openid,$uid,$ticket){

        if($data= $this->detail($openid)){
            if($data['uid'] == $uid)  return '您已绑定过此帐号，请不要重复绑定';
        }else{
            $data = get_client_info($openid);
        }
        $data['uid']=$uid;

        $re = $this->update($data);
        if($re){
            S($ticket,array(
                'data'=>array(
                    'ticket'=>$ticket,
                    'openid'=>$openid,
                    'uid'=>$uid
                )
            ),15);
            $username = get_username($data['uid']);
            $message  =  '恭喜您，帐号绑定成功！绑定帐号：'.$username;
        }else if($re === 0){
            $message = '您已绑定过此帐号，请不要重复绑定';
        }else{
            $message = '绑定失败，请稍后重新尝试';
        }
        return $message;
    }

}