<?php

// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 * 素材管理控制器
 */
use Wechat\Api\WechatApi;
class WechatMaterialController extends WechatController {

    protected $Wechat;

    protected function _initialize() {
        $this->Wechat = new WechatApi();
        $this->jsonAllTags();
        parent::_initialize();
    }

    public function index() {

        $this->meta_title = '微信素材控制面板';
        $this->display();
    }

    /**
     * 在线素材管理
     */
    public function online(){

        if($type = I('get.type')){
            $p = I('get.p')?I('get.p'):0;
            $offset = $p?$p*20+1:0;
            $count = 20;
            $meterial = batch_get_material($type,$offset,$count);
            if(!is_array($meterial)){
                $this->error($meterial);
                exit;
            }
            $Media = M('TchatMedia');
            switch($type){
                case 'image':
                    foreach($meterial['item'] as $key => $vo){
                        $pic = $Media->where('`media_id` = "'.$vo['media_id'].'"')->getField('file_id');
                        if($pic){
                            $meterial['item'][$key]['url'] = get_cover($pic,'path');
                        }
                    }
                    $this->assign('images',$meterial);
                    break;
                case 'video':
                    $this->assign('videos',$meterial);
                    break;
                case 'voice':
                    $this->assign('voices',$meterial);
                    break;
                case 'news':
                    $this->assign('news',$meterial);
                    break;
                default:

            }
        }

        $meterialCount = get_meterial_count();
        $this->assign('meterialCount',$meterialCount);
        $this->display('online'.$type);
    }

    /**
     * 链接收藏管理
     */
    public function url() {
        $this->getLists('Url');
        $this->meta_title = 'URL链接素材管理';
    }

    /**
     * 获取数据列表
     */
    protected function getLists($model, $map = '') {
        $list = $this->lists($model , $map);
        col_to_string($list);
        $this->assign('_list' , $list);
    }

    /**
     * 创建一条微信素材
     * 可用于本地上传好文件后，同步上传至微信服务器之前先创建一条素材，获得media_id后可再更新此条记录
     * 也可上传到本地后，直接去获取media_id，再用本方法存储记录。
     *
     * @param   string  $type       素材类型
     * @param   int     $file_id    对应的本地存储数据表中的ID值
     *                               图片、缩略图 对应表 picture
     *                               语音对应表 tchat_music
     *                               视频对应表 tchat_video
     * @param string    $media_id   微信公众平台生成的mediaId
     *
     * @return int 添加后的记录ID
     */
    protected function createMedia($type,$file_id=0,$media_id=''){
        $Media = M("TchatMedia");
        $newMedia = array(
            'media_type'=>$type,
            'status'=>'0',
            'uid'=>UID,
            'create_time'=>time(),
            'update_time'=>time()
        );

        //本地存储的文件的id
        if($file_id){
            $newMedia['file_id']=$file_id;
        }

        //服务器获取到的对应media_id
        if($media_id){
            $newMedia['media_id']=$media_id;
        }

        //添加记录返回数据记录ID
        return $Media->data($newMedia)->add();
    }

    protected function deleteMedia($id){
        if(!$id){
            $this->error('素材ID指定错误');
            exit;
        }

        $success = 0;
        $successMsg = '';
        $error = 0;
        $errorMsg = '';

        if(!is_numeric($id)){
            $id = str2arr($id);
        }

        if(!is_array($id)){
            $re = $this->deleteMediaItem($id);
            if($re['status'] == 0){
                $error ++;
                $errorMsg .= $re['info'].'[id='.$id.']';
            }else{
                $success ++;
            }
        }else{
            foreach($id as $v){
                $re = $this->deleteMediaItem($v);
                if($re['status'] == 0){
                    $error ++;
                    $errorMsg .= $re['info'].'[id='.$v.']';
                }else{
                    $success ++;
                }
            }
        }

        $str = '本次上传成功'.$success.'个，失败'.$error.'个'.$errorMsg;
        $return =  array(
            'status'=>1,
            'info'=>$str
        );
        return $return;
    }

    private function deleteMediaItem($id){
        $Media = M('TchatMedia');
        $info = $Media->where('`id` ="'.$id.'"')->find();

        if(!$info){
            return array(
                'status'=>0,
                'info'=> '素材获取错误，该素材不存在'
            );
        }

        //删除微信端素材
        $re = delete_material($info['media_id']);
        if($re !== 0){
            return array(
                'status'=>0,
                'info'=> '删除微信端线上素材失败'
            );
        }

        //删除数据库条目
        $re = $Media->delete($info['id']);

        if(!$re){
            return array(
                'status'=>0,
                'info'=> '素材数据库条目删除失败'
            );
        }

        //删除文件数据库条目
        $type = $info['type'];
        $fid = $info['file_id'];

        switch($type){
            case "voice":
                $model = "TchatMusic";
                break;
            case "video":
                $model = "TchatVideo";
                break;
            default:
                $path = get_cover($fid,'path');
                $model = "Picture";
        }

        $res = M($model)->delete($fid);
        if(!$res){
            return array(
                'status'=>0,
                'info'=> '文件数据库删除失败'
            );
        }

        //删除物理文件
        $real_path = $_SERVER['DOCUMENT_ROOT']. $path;
        $res = unlink($real_path);
        if(!$res){
            return array(
                'status'=>0,
                'info'=> '物理文件删除失败'
            );
        }

        return array(
            'status'=>1,
            'info'=> '素材删除成功'
        );
    }


    private function jsonAllTags(){
        $allTags = get_wechat_tags();
        foreach($allTags as $k=>$v){
            $allTags[$k]['text'] = $v['name'];
            unset($allTags[$k]['name'],$allTags[$k]['count']);
        }
        $allTags = JSON($allTags);

        $this->assign('allTags',$allTags);
    }
}
