<?php

// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Wechat\Api\WechatApi;
/**
 * 素材管理控制器
 */
class WechatPictureController extends WechatMaterialController {

    /**
     * 图片素材列表
     */
    public function index(){
        //构建查询方式
        $Media = M('TchatMedia');

        $map = array(
            'media_type'=>'image'
        );

        $p = I('param.p')?I('param.p'):1;
        //查询符合条件的图片素材列表
        $list = $Media->where($map)->page($p,20)->select();

        //开始查找每个列表所包含的本地图片
        $Picture = M('Picture');
        foreach($list as $k => $v){
            //查找对应的图片
            $list[$k]['path'] = get_cover($v['file_id'],'path');
        }

        $count      = $Media ->where($map)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数

        $show       = $Page->show();// 分页显示输出
        //赋值前端调用数据
        $this->assign('_list' , $list);
        $this->assign('page',$show);// 赋值分页输出
        $this->meta_title = '图片素材列表';
        $this->display();

    }

    /**
     * 新增图文素材
     */
    public function addPicture(){
        if(IS_POST||IS_AJAX){
            $pics = I('post.pic_id');
            $pics = str2arr($pics);
            $success = 0;
            $error = 0;
            new WechatApi();
            foreach($pics as $pic){
                $material = add_material(get_cover($pic,'path'),'image');
                if($material){
                        $Media = M('TchatMedia');
                        $data = array(
                            'media_id'=>$material,
                            'file_id' => $pic,
                            'media_type'=>'image',
                            'status'=>1,
                            'uid'=>UID,
                            'create_time'=>time(),
                            'update_time'=>time()
                        );
                        $re = $Media->add($data);
                        if($re){
                            $success++;
                        }else{
                            M('Picture')->where('`id`="'.$pic.'"')->delete();
                            $error++;
                        }
                }else{
                    M('Picture')->where('`id`="'.$pic.'"')->delete();
                    $error++;
                }

            }

            $str = '本次上传成功'.$success.'个，失败'.$error.'个';

            $this->success($str,U('index'),5000);
        }

        $this->meta_title = '添加图片素材';
        $this->display();
    }

    /**
     * 删除图文素材
     * 删除分为只删除本地、只删除服务器以及同时删除三种情况
     *
     * @param int   $id     图文素材在tchat_media表中的记录ID
     */
    public function deletePicture(){
        if(IS_POST||IS_AJAX){
            $id = I('param.id');
            $return = $this->deleteMedia($id);
            $this->ajaxReturn($return);
        }else{
            $this->error('非法访问');
        }
    }

    /**
     * 下载微信端的图文素材
     * 这里是下载已经在系统中存储过的素材
     *
     * @param int   $media_id   微信服务器上的图文素材media_id
     */
    public function downloadPicture($media_id){

    }

}
