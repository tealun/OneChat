<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Wechat\Api\QrCodeApi;

/**
 *二维码管理控制器

 */
class WechatQrcodeController extends WechatController {

    /**
     * 二维码首页列表方法
     */
    public function index() {
        $QrCode = M('TchatQrcode');
        if ( I('get.p') ) {
            $page = I('get.p');
        } else {
            $page = 1;
        }
        $list = $QrCode
            ->where('`status` = 1 AND `module` ="Admin"')
            ->order('create_time desc')
            ->page($page . ',10')
            ->select();
        new QrCodeApi();
        foreach($list as $k => $v){
            $list[$k]['qrpic'] = get_qr_code($v['id']);
        }

        $list = col_to_string($list);

        $this->setPage('TchatQrcode');
        //col_to_string($list);
        $this->meta_title = '二维码列表';
        $this->assign('_list' , $list);
        $this->display(); // 输出模板
    }

    /**
     * 创建二维码
     */
    public function create() {
        $info['model_id'] = '51';

        //获取二维码模型
        $model = M('Model')->where(array( 'id' => $info['model_id'] ))->find();
        //获取表单字段排序
        $fields = get_model_attribute($model['id']);

        //获取回复模版
        $fields = parent::getReplyFlow($fields);
        $this->assign('fields' , $fields);
        $this->assign('model' , $model);

        if ( I('get.id') ) {
            $info['id'] = I('get.id');
            //查找指定二维码的相关信息
            $QrCode = new QrCodeApi();
            $data = $QrCode->getInfo(I('get.id'));
            $this->assign('info',$info);
            $this->assign('data' , $data);
            $this->meta_title = '编辑二维码';
            $this->display('edit');
        } else {

            $this->assign('info',$info);
            $this->meta_title = '新增二维码';
            $this->display();
        }
    }


    /**
     *获取一个二维码ticket
     * @return array
     */
    public function getTicket() {
        if ( IS_POST || IS_AJAX ) {
            $data = I('post.');
            $QrCode = new QrCodeApi();
            $qrId = $QrCode->create('Admin' , $data['scene'] , $data['expireSeconds']);
            $qr = array(
                'id' => $qrId ,//如果是创建临时二维码，id为ticket
                'image' => get_qr_code($qrId)
            );
            $this->ajaxReturn($qr , 'json');
        }
    }

    /**
     * 存储二维码回复
     */
    public function update() {

        if ( IS_POST || IS_AJAX ) {
            $data = I('post.');
            !$data['scene'] && $this->error('必须设置应用场景');
            !$data['flow_id'] && $this->error('没有回复内容，必须指定回复内容');
            if(!$data['id']){
                $QrCode = new QrCodeApi();
                $data['id'] = $QrCode->create('Admin' , $data['scene'] );
            }
            $Qrcode= D('TchatQrcode');
            $rs = $Qrcode->update($data);
            if ( !$rs) {
                $this->error($Qrcode->getError());
            } else{
                $this->success('操作成功',U('index'));
            }
        }
    }

    /**
     * 设置一条或者多条数据的状态
     * @author huajie <banhuajie@163.com>
     */
    public function setStatus($model = 'Tchat_qrcode') {
        return parent::setStatus('Tchat_qrcode');
    }

    /**
     * 通过二维码TICKET展示二维码图片
     *
     * @param int $id
     */
    public function showQrcode() {
        if ( IS_GET ) {
            $QrCode = new QrCodeApi();
            $data = $QrCode->getInfo(I('get.id'));
            if ( IS_NULL($data) ) {
                $this->display('index');
            }
            $data['qr_code'] = get_qr_code($data['ticket']);
            $this->assign('data' , $data);
            $this->meta_title = '查看二维码';
            $this->display('show');
        } else {
            $this->display('index');
        }

    }

    /**
     * 检测是否超过有效期秒数限制
     */
    protected function expireSeconed($time) {
        if ( $time >= 1800 ) {
            return false;
        } else {
            return true;
        }
    }

}
