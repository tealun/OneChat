<?php

// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Common\Api\DocumentApi;
/**
 * 素材管理控制器
 */
class WechatNewsController extends WechatMaterialController {

    protected function _initialize() {
        parent::_initialize();
    }

    /**
     * 图文素材列表
     */
    public function index(){
        //构建查询方式
        $Media = M('TchatMedia');
        $map = array(
            'media_type'=>'news'
        );
        //查询符合条件的图文素材列表
        $list = $Media->where($map)->select();

        //开始查找每个列表所包含的本地素材文章
        $Article = M('TchatArticles');
        foreach($list as $k => $v){
            $list[$k]['articles'] = $Article->where('`media_id` = '.$v['id'])->order('sort asc')->select();
            $count = count($list[$k]['articles']);
            if($count == 0){
                unset( $list[$k] );
            }else{
                for($i=0;$i<$count;$i++){
                    //查找对应的封面图片，用文章存储的thumb_media_id查找图片在系统中存储的id
                    $mediaId = $list[$k]['articles'][$i]['thumb_media_id'];
                    $list[$k]['articles'][$i]['cover_id'] = $Media->where(array('media_id'=>$mediaId))->getField('file_id');
                }
            }
        }
        //赋值前端调用数据
        $this->assign('_list' , $list);
        $this->meta_title = '图文素材列表';
        $this->display();

    }

    /**
     * 新增图文素材
     */
    public function addNews(){
        $this->meta_title = '添加图文素材';
        $this->display();
    }

    /**
     * 编辑图文素材
     */
    public function editNews($id){
        if(!$id){
            $this->error('请指定编辑的素材');
        }
        $map=array(
            'media_id'=>$id
        );
        $Articles = M('TchatArticles');
        $count = $Articles
            ->where($map)
            ->count();
        if($count == 0){
            $this->error('指定素材没有文章，请确认输入正确');
        }
        $articles = $Articles
            ->where($map)
            ->order('sort asc')
            ->select();

        $Media = M('tchat_media');
        for($i=0;$i<$count;$i++){
            //查找对应的封面图片，用文章存储的thumb_media_id查找图片在系统中存储的id
            $mediaId = $articles[$i]['thumb_media_id'];
            $articles[$i]['cover_id'] = $Media->where(array('media_id'=>$mediaId))->getField('file_id');
        }

        $this->assign('articles',$articles);
        $this->meta_title = '编辑图文素材';
        $this->display();
    }

    /**
     * 新增本地图文素材文章
     */
    public function addTchatArticle(){
        if(IS_POST){
            $data = I('post.');
            $article = $data['info'];
            $article['sort'] = '0';
            $article['author'] = get_nickname(UID);

            $article = $this->reBuildArticle($article);
            $article['model_id'] = 0;
            $article['article_id'] = 0;

            $id = M('tchatArticles')->data($article)->add();

            if($id){
                $data['info']['cover']=get_cover($data['info']['cover_id'],'path');
                $data['info']['index']=$data['info']['cover'];
                $data['info']['id']=$id;
                $data['info']['article_id']=$id;
                $data['info']['itemType']='article';
                $return = array(
                    'status'=>1,
                    'data'=>$data
                );
            }else{
                $return = array(
                    'status'=>0,
                    'info'=>'创建素材文章出错'
                );
            }

            $this->ajaxReturn($return);
        }else{
            $this->error('非法访问');
        }
    }

    /**
     *查找系统文章
     */
    public function findDocument(){
        if(IS_POST){
            $id = I('post.docId');
            if($id){
                //查找文档内容
                $Document = new DocumentApi();
                $info  = $Document->info($id);

                //判断查询结果
                if($info['errcode']){
                    $return['info']     =   '查询失败，错误原因：'.$info['errmsg'];
                    $return['status']   =   0;
                }else{
                    //赋值article_id，删除自带的id
                    $info['article_id']=$info['id'];
                    $info['itemType']='document';
                    unset($info['id']);
                    $return['info']     =   $info;
                    $return['status']   =   1;
                }

                $this->ajaxReturn($return);
            } else{
                $this->error('访问错误！');
            }
        }else{
            $this->error('非法访问');
        }
    }

    /**
     * 更新图文素材
     *
     */
    public function updateNews(){
        if(IS_POST){
            $data = I('post.');

            //检测提交过来的图文
            if(!$data['articles']){
                $return = array(
                    'status'=>0,
                    'info'=>'没有添加图文项目！'
                );
                $this->ajaxReturn($return);
                exit;
            }

            //创建新的media_id
            if(!$data['media_id']){
                $id = $this->createMedia('news');
                if(!$id){
                    $return = array(
                        'status'=>0,
                        'info'=>'创建新的媒体存储项出错！'
                    );
                    $this->ajaxReturn($return);
                    exit;
                }
                $data['media_id'] = $id;
            }


            //将文章写入素材文章数据库
            $Document = new DocumentApi();
            $TchatArticle = M('TchatArticles');
            $error =array();//定义错误记录数组
            $errmsg = '';//定义错误消息
            $articles = json_decode($data['articles'],true);

            foreach($articles as $key=>$value){

                preg_match('/([^\d]+)(\d+)/',$value['id'],$match);
                $type = $match[1];
                $id = $match[2];
                switch($type){
                    case"document":
                        // 获取文档详细数据
                        $article = $Document->info($id);
                        //将新建或传入的图文MEDIAIA传入文章
                        $article['media_id']=$data['media_id'];
                        $article['sort']= $key;
                        //非本地图文素材文章要重新整理格式
                        $article = $this->reBuildArticle($article);
                        //查找当前图文素材里有没有该篇文章对应的素材
                        $map = array(
                            'model_id'=>$article['model_id'],
                            'article_id'=>$article['id'],
                            'media_id'=>$article['media_id']
                        );
                        $id= $TchatArticle ->where($map)->getField('id');
                        if($id){//有就赋值该素材ID，并更新当前内容到该条目中
                            $article['id']=$id;
                            $result = $TchatArticle->data($article)->save();
                            if($result === false) {
                                //记录更新错误信息
                                $error['update'][]=array($article['id']);
                            }
                        }else{//没有就新增一条素材
                            unset($article['id']);//必须删除自身的ID，因为会去新数据库中建立或查找，会与新数据库数据冲突
                            $id = $TchatArticle->data($article)->add();
                            //记录新增错误信息
                            if(!$id){$error['add'][]=array($article['model_id'],$article['article_id']);}
                        }
                        break;
                    case"article":
                        //将新建或传入的图文MEDIAIA传入文章
                        $article['id']=$id;
                        $article['media_id']=$data['media_id'];
                        $article['sort']= $key;
                        $result = $TchatArticle->data($article)->save();
                        if($result === false) {
                            //记录更新错误信息
                            $error['update'][]=array($article['id']);
                        }
                        break;
                    default:
                        break;
                }
            }

            //检查是否有错误信息记录
            if(!empty($error)){
                $addCount = count($error['add']);
                $updateCount = count($error['update']);

                if($addCount > 0){//新增记录是否有错误信息
                    $errmsg .= '新增'.$addCount.'项出错，';
                    foreach($error['add'] as $k => $v){
                        $errmsg .= '[模型'.$v['model_id'].'编号'.$v['article_id'].']';
                    }
                    $errmsg .= '。';
                }

                if($updateCount > 0){//更新记录是否有错误信息
                    $errmsg .= '更新'.$updateCount.'项错误';
                    foreach($error['update'] as $k => $v){
                        $errmsg .= '[素材文章编号'.$v.']';
                    }
                    $errmsg .= '。';
                }
            }

            $return = array(
                'status'=>1,
                'info'=>'操作完成！',
                'local_id'=>$data['media_id'],
                'url'=>U('index')
            );

            $this->ajaxReturn($return);
        }
    }

    /**
     * 删除图文素材
     * 删除素材将在本地和服务器端统一删除
     *
     * @param int   $id     图文素材在tchat_media表中的记录ID
     */
    public function deleteNews($id){

        $Media = M('TchatMedia');
        $info = $Media->find($id);
        if(!$info){
            $this->error('非法图文素材ID');
            exit;
        }

        if(!empty($info['media_id'])){
            $meida_id = $info['media_id'];
            $res = delete_material($meida_id);
            if($res !== 0){
                $this->error('删除线上图文素材错误，错误原因：'.$res);
                exit;
            }
        }

        $Articles = M('TchatArticles');
        $map['media_id'] = $id;
        $res = $Articles->where($map)->delete();
        if(!$res){
            $this->error('删除本地数据出错！');
        }

        $res = $Media->delete($id);
        if($res){
            $this->success('删除成功！');
        }else{
            $this->error('删除媒体数据库列表错误。');
        }

    }
    /**
     * 上传图文消息
     * 用于本地图文消息编辑后的上传
     * 上传完成后会有media_id字段返回，要保存到受影响的记录的media_id字段中
     *
     * @param int   $id     图文素材在tchat_media表中的记录ID
     */
    public function uploadNews(){
        if(IS_POST||IS_AJAX){
            $id = I('post.id');

            $Media = M('TchatMedia');
            $info = $Media->find($id);

            if(!$info){
                $this->error('非法图文素材ID');
                exit;
            }
            if($info['status'] == 1){
                $this->error('已经同步，请勿重复提交，如已在微信后台更改，请点击下载。');
                exit;
            }

            $Articles = M('TchatArticles');
            $lists = $Articles
                ->where('`media_id` ="'.$id.'"')
                ->order('sort')
                ->field('id,title,thumb_media_id,author,digest,show_cover_pic,content,content_source_url')
                ->select();

            foreach($lists as $k => $v){
                //将文章中图片上传到微信服务器并替换
                $lists[$k]['content'] = $this->uploadContentImg($v['content']);
                //保存到系统微信文章中
                $data = array(
                    'id'=>$v['id'],
                    'content'=>$lists[$k]['content'],
                    'status'=>1
                );
                $Articles->save($data);
                unset($lists[$k]['id']);
            }

            $media_id = add_news($lists);
            if(is_numeric($media_id)){
                $this->error(get_wechat_response($media_id));
                exit;
            }

            //更改素材数据库状态
            $info['media_id'] = $media_id;
            $info['status']    =1;
            $Media->save($info);

            $this->success('图文素材['.$media_id.']上传成功',U('index'));
        }else{
            $this->error('非法访问！');
        }
    }

    /**
     * 下载微信端的图文素材
     * 这里是下载已经在系统中存储过的素材
     *
     * @param int   $media_id   微信服务器上的图文素材media_id
     */
    public function downloadNews($media_id){

    }

    /**
     * 重构文章数据
     * TODO 新增相册类型，并与文章区分，依靠model_id Url注意变更
     * @param $article
     * @return mixed
     */
    private function reBuildArticle($article){

        //生成素材用的缩略图MediaId
        if($article['sort'] === '0'){//判断用那个图片作为缩略图
            $cover=$article['cover'];
        }else{
            $cover=$article['index'];
        }
        $article['thumb_media_id'] = $this->uploadThumb($cover);
        //是否显示缩略图 TODO 是否采用系统配置
        $article['show_cover_pic'] = 1;

        //设置作者和简介
        $article['author']= $article['author']?:get_username($article['uid']);
        $article['digest']= $article['description'];

        //设置原文地址
        if($article['link_id']){
            $article['content_source_url'] = $article['link_id'];
        }else{
            //注意，此处调用的函数是从Document中文章的地址
            $article['content_source_url'] = get_article_url($article['id']);
        }

        //补充其他字段信息
        $article['article_id'] = $article['id'];
        $article['status'] = 0;
        $article['uid'] = UID;
        $article['create_time']=time();
        $article['update_time']=time();

        return $article;
    }

    private function uploadThumb($filePath){

        $file['md5']  = md5($_SERVER['DOCUMENT_ROOT'].__ROOT__ .$filePath);
        $file['sha1'] = sha1($_SERVER['DOCUMENT_ROOT'].__ROOT__ .$filePath);

        //查看系统随机分配的图片是否在图片数据库中注册过
        $pic = D('Picture')->isFile($file);
        if($pic){//如果有就赋值图片ID
            $fileId = $pic['id'];
        }else{//没有就将随机图片写入图片数据库
            $file['path']=$file;
            $file['status']=1;
            $file['create_time']=time();
            $fileId = M('Picture')->add($file);
        }

        //查看是否有该文件注册过的微信图文缩略图
        $map = array(//构建查询条件
            'file_id'=>array('eq',$fileId),
            'media_type'=>'thumb'
        );
        $thumb = M('TchatMedia')->where($map)->getField('media_id');

        if($thumb){//有就直接调用存储的缩略图mediaId
            $thumb_media_id = $thumb;
        }else{//没有就创建一个缩略图mediaId
            $pic = get_cover($fileId,'path');
            //清除检查文件大小缓存
            clearstatcache();
            $thumb_media_id = add_material($pic,'thumb');//去微信服务器创建
            $this->createMedia('thumb',$fileId,$thumb_media_id);//存储到本地数据库
        }

        return $thumb_media_id;
    }

    /**
     * 上传文章内图片
     *
     * @param $content
     * @return mixed
     */
    private function uploadContentImg($content){

        $pattern="/<[img|IMG].*?src=[\'|\"](.*?(?:[\.gif|\.jpg|\.png]))[\'|\"].*?[\/]?>/";

        if(preg_match_all($pattern,$content,$match)){
            foreach($match[1] as $v){
                // 如果图片没有上传到微信服务器
                if ( strpos( $v, 'qpic.cn' ) === false ) {
                    //替换文章内容img中的src地址
                    $url = uploading_content_img($v);
                    $content = str_replace($v,$url,$content);
                }
            }
        }
        return $content;
        var_dump($content);
    }

}
