<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 *相册板块后台管理控制器

 */
class AlbumController extends AdminController {

    /**
     * 相册列表
     */
    public function index() {
        $status = I('get.status');

        $map['status']=$status==='0'?0:1;

        if(I('get.display')){
            $map['display']=I('get.display');
        }
        $order = 'sort,id desc';

        $this->getLists('TchatAlbum',$map,$order);
        $this->meta_title = '相册列表';
        $this->display();
    }

    /**
     * 相册详情
     */
    public function detail() {
        /* 详情页面的上传图片操作 */
        if(IS_POST){
            $data= I('post.');
            $Picture = M('TchatPicture');

            if(!$data['pic_id']){
                $this->error('请先上传图片');
            }
            if(!$data['title']){
                $this->error('请先设置图片标题');
            }
            if(!$data['description']){
                $this->error('请先设置图片描述');
            }
            if(!$data['album_id']){
                $this->error('没有指定图片所属相册');
            }

            $pics = explode(',',$data['pic_id']);

            $f = 0;//过滤已存在图片
            foreach($pics as $k => $pic){
                $where = array(
                    'pic_id'=>$pic,
                    'album_id'=>$data['album_id']
                );
                $check = $Picture->where($where)->count();
                if($check > 0){
                    unset($pics[$k]);
                    $f++;
                }
            }
            if(empty($pics)){//过滤后没有新图片时
                $this->error('您所上传的所有图片均已存储，本次无添加。');
            }
            $f = $f?'其中'.$f.'张已存在本相册，故此忽略，':'';

            unset($data['pic_id']);//删除提交数据中['pic_id'],已重新组织了新的pic_id为$pics
            $i=0;$e=0;//赋值成功和错误的数量
            foreach($pics as $pic){
                $data['pic_id']=$pic;
                $re = $Picture->data($data)->add();
                $re?$i++:$e++;//添加成功和失败数量
            }

            if($i > 0){//当有成功数量时
                $this->success('本次上传成功'.$i.'张图片,'.$f.'失败'.$e.'张图片');
            }else{
                $this->error('本次存储图片失败，共计'.$e.'张图片');
            }

        /* 获取最后一次上传的图片信息操作 */
        }elseif(I('get.id')&&I('get.getMeta')){
            $info = M('TchatPicture')->where('`album_id` = "'.I('get.id').'"')->order('id DESC')->find();
            if(!$info){
                $this->error('本相册还没有上传过图片');
            }
            $meta['title'] = $info['title'];
            $meta['author'] = $info['author'];
            $meta['keywords'] = $info['keywords'];
            $meta['description'] = $info['description'];
            $data = array(
                'status'=>1,
                'info'=>'获取成功',
                'meta'=>$meta
            );
            $this->ajaxReturn($data);

        /* 设置相册封面操作 */
        }elseif(I('get.id')&&I('get.setCover')){
            $Album = M('TchatAlbum');
            $data['id'] = I('id');
            $data['cover_id'] = I('setCover');
            $re = $Album->save($data);
            if(!$re){
                $this->error('设置失败！！');
            }else{
                $this->success('设置成功！');
            }

        }

        /*获取相册属性信息*/
        $id = I('get.id');
        $info = D('tchat_album')->detail($id);
        $this->assign('info',$info);

        /* 获取相册内图片数据 */
        $Picture = M('TchatPicture');
        $p = I('p')?I('p'):1;
        $where = array(
            'album_id'=>$id
        );
        $list = $Picture->where($where)->page($p , 10)->select();
        $this->assign('_list' , $list);//赋值图片列表

        if ( false === $list ) {//获取为0时是返回int0，因此失败用恒等于false判断
            $this->error('获取图册列表数据失败！');
        }
        //整理页码数据
        $total = $Picture->where($where)->count();
        $Page       = new \Think\Page($total,10);// 实例化分页类 传入总记录数和每页显示的记录数
        $show       = $Page->show();// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出

        $this->meta_title = '相册['.$info['title'].']详情';
        $this->display();
    }

    /**
     * 创建相册
     */
    public function create() {
        //获取相册模型
        $model = M('Model')->where(array( 'id' => 61 ))->find();
        $info['model_id'] = '61';
        //获取表单字段排序
        $fields = get_model_attribute($model['id']);
        //缓存所有相册分类
        $this->jsonAllCategories();
        $this->assign('info' , $info);
        $this->assign('fields' , $fields);
        $this->assign('model' , $model);
        $this->meta_title = '创建新相册';
        $this->display();
    }

    /**
     * 编辑相册
     */
    public function edit() {
        $id= I('get.id');
        if(!$id) {
            $this->error('请指定要编辑的相册。');
        }elseif($id == 1){
            $this->error('此收集箱为系统保留相册，禁止编辑。');
        }
        /*获取一条记录的详细数据*/
        $TchatAlbum = D('TchatAlbum');
        $data = $TchatAlbum->detail($id);
        if ( !$data ) {
            $this->error($TchatAlbum->getError());
        }

        //赋值data变量，将作为字段的已有数据值对应设置为字段值
        $this->assign('data' , $data);
        $info['id'] = $data['id'];
        //获取相册模型
        $model = M('Model')->where(array( 'id' => 61 ))->find();
        $info['model_id'] = '61';
        //获取表单字段排序
        $fields = get_model_attribute($model['id']);

        //缓存所有相册分类
        $this->jsonAllCategories();
        $this->assign('info' , $info);
        $this->assign('fields' , $fields);
        $this->assign('model' , $model);
        $this->meta_title = '创建新相册';
        $this->display('create');
    }

    /**
     * 更新相册
     */
    public function update() {
        if ( IS_POST || IS_AJAX ) {
            $Album = D('TchatAlbum');
            $res = $Album->update();
            if ( !$res ) {
                $this->error($Album->getError());
            } else {
                $this->success($_POST['id'] ? '更新成功' : '新增成功' , U('index') , 1);
            }
        } else {
            $this->error('访问错误' , U('index') , 3);
        }
    }

    /**
     * 更新状态
     */
    public function setStatus($model = 'TchatAlbum') {
        $albumCategory = I('get.albumCategory');
        if($albumCategory){
            $model = 'TchatAlbumCategory';
        }
        return parent::setStatus($model);
    }

    /**
     * 彻底删除
     */
    public function remove($id=''){
        if(IS_POST || IS_AJAX){

            $id = I('get.id');
            if(!$id){
                $this->error('必须指定要删除的相册');
            }

            if(!is_array($id)){
                $id = str2arr($id);
            }
            foreach($id as $k => $v){
                if($v == 1){
                    $this->error('收集箱是系统保留相册，禁止删除。');
                    exit;
                }
            }

            $Album = M('TchatAlbum');
            $re = $Album->limit(count($id))->delete(arr2str($id));

            if($re === false){
                $this->error('删除相册数据操作出错');
                exit;
            }elseif($re === 0){
                $this->error('没有删除任何相册');
                exit;
            }
            $string = '删除相册成功！';

            $string .= $this->removePic($id);

            $this->success($string,'',5);

        }else{
            $this->error('非法操作！');
        }
    }


    /**
     * 删除相册内图片
     */
    public function deletePic(){

        if(IS_POST||IS_AJAX){
            $ids = I('ids');
            if(empty($ids)){
                $this->error('请选择要删除的图片');
            }
            $string = $this->removePic('',$ids);
            $this->success($string,'',5);
        }else{
            $this->error('非法访问');
        }

    }

    /**
     * 移动相册内图片
     */
    public function movePic(){
        if(IS_POST||IS_AJAX){
            $ids = I('ids');
            $album_id = I('album_id');
            $Pictures = M('TchatPicture');
            $map['album_id']= array('eq',$album_id);
            $i = 0;$f = 0;
            foreach($ids as $id){
                $pic_id = $Pictures->where('`id`="'.$id.'"')->getField('pic_id');
                $map['pic_id']= array('eq',$pic_id);
                $count = $Pictures -> where($map)->count();//检测目标相册是否有同样的图片
                if($count){
                    $Pictures->limit(1)->delete($id);//删除移出图片
                    $f++;
                }else{
                    $Pictures->album_id = $album_id;
                    $Pictures -> save($id);//更改要移出的图片的归属相册id
                }
            }

            $this->success('成功移动'.$i.'张,目标相册原有'.$f.'张');
        }else{
            $this->error('非法访问');
        }
    }

    /**
     * 相册分类管理
     */
    public function category(){
        $status = I('get.status');
        $map = array(
            'status'=>$status==='0'?0:1
        );
        $this->getLists('TchatAlbumCategory',$map);
        $this->meta_title = '相册分类';
        $this->display();
    }

    /**
     * 新增相册分类
     */
    public function addCategory(){
        $this->meta_title = '新增相册分类';
        $this->display();
    }

    /**
     * 编辑相册分类
     */
    public function editCategory($id){
        if(!$id){
            $this->error('必须指定相册分类');
        }
        $Category = D('TchatAlbumCategory');
        $info = $Category->detail($id);
        $this->assign('info',$info);
        $this->meta_title = '编辑相册分类';
        $this->display('addCategory');
    }

    /**
     * 更新相册分类
     */
    public function updateCategory(){
        $id = I('post.id');
        $Category = D('TchatAlbumCategory');
        $status = $Category->update();
        if($status){
            if(empty($id)){
                $this->success('新增相册分类成功',U('category'),3);
            }else{
                $this->success('更新相册分类成功',U('category'),3);
            }
        }else{
            $this->error($Category->getError());
        }
    }


    /**
     * 删除相册分类
     */
    public function deleteCategory(){
        $Category = D('TchatAlbumCategory');
    }

    /**
     * 获取数据列表
     */
    private function getLists($model,$map = '',$order='') {
        $list = $this->lists($model , $map,$order);
        col_to_string($list);
        if($list && $model == 'TchatAlbum'){
            $list = $this->rebuildAlbum($list);
        }
        $this->assign('_list' , $list);
    }

    /**
     *生成供前台使用的分类json数据
     *
     */
    private function jsonAllCategories(){
        $Category = M('TchatAlbumCategory');
        $map = array('status'=>1);
        $find = $Category->where($map)->getField('id,title');
        if($find){
            $i =0;
            foreach($find as $k=>$v){
                $cats[$i]['id'] = $k;
                $cats[$i]['title'] = $v;
                $i++;
            }
            unset($i);
        }else{
            $cats = '';
        }
        $cats = JSON($cats);

        $this->assign('cats',$cats);
    }

    /**
     * 重新组织各个相册列表数据
     *
     * @param $list
     * @return mixed
     */
    private function rebuildAlbum($list){
        $Category = M('TchatAlbumCategory');
        $Picture = M('TchatPicture');
        foreach($list as $k=>$v){
            $list[$k]['category']=$Category->where('`id` ="'.$v['cat_id'].'"')->getField('title');
            $count =$Picture->where('album_id = "'.$v['id'].'"')->count();
            $list[$k]['count']=$count;
            if($count){//查到该相册有图片
                $map['album_id'] =array('eq',$v['id']);
                $i=0;
                if(!$v['cover_id']){
                    $limit = 4;
                }else{
                    $list[$k]['img'][$i]=get_cover($v['cover_id'],'path');
                    $map['pic_id'] = array('neq',$v['cover_id']);
                    $limit = 3;
                    $i++;
                }
                //最多取最新的4张
                $arr = $Picture->where($map)->order('id DESC')->limit($limit)->getField('pic_id',true);
                for($i;$i<4;$i++){

                        if($limit < 4){
                            if($arr[$i-1]){
                                $list[$k]['img'][$i]=get_cover($arr[$i-1],'path');
                            }
                        }else{
                            if($arr[$i]) {
                                $list[$k]['img'][$i] = get_cover($arr[$i], 'path');
                            }
                        }

                }
            }

        }
        return $list;
    }


    private function removePic($album_id=array(),$ids=array()){

        //进入删除相册内存储的图片信息流程
        $TchatPicture = M('TchatPicture');

        $map = array();

        if(!empty($album_id)){
            if(!is_array($album_id)){
                $album_id = str2arr($album_id);
            }
            $map['album_id'] = array('in',$album_id);//这是为删除相册内图片准备的条件
        }

        if(!empty($ids)){
            if(!is_array($ids)){
                $ids = str2arr($ids);
            }
            $map['id'] = array('in',$ids);//这是为删除相册内图片准备的条件
        }

        if(empty($map)){
            $this->error('必须指定要删除的图片');
        }

        $pics = $TchatPicture->where($map)->getField('id,pic_id',true);
        if(empty($pics)){
            $string = '相册内没有图片';
        }else{

        $re = $TchatPicture->limit('"'.count($pics).'"')->delete(arr2str(array_keys($pics)));//删除Tchat相册存储的图片数据
        if($re === false){
            $this->error('删除相册内图片存储数据出错');
            exit;
        }elseif($re === 0){
            $this->error('没有删除任何相册内图片存储数据');
            exit;
        }
        $string = '相册内图片存储数据删除成功';

        $i = 0;
        foreach($pics as $key => $pic){//检测要删除的图片在其他相册中是否存在，存在则不删除OneThink图片库数据
            $count = $TchatPicture->where('`pic_id` ="'.$pic.'"')->count();
            if($count){
                unset($pics[$key]);
                $i++;
            }
        }
        //所有删除图片均在其他相册时
        if($i > 0){
            $string .= '由于部分图片也在其他相册，因此未删除物理数据。';
        }

        //删除系统数据库存储及物理数据
        $string .= $this->deleteFile('Picture',$pics);
        }
        return $string;
    }

    private function deleteFile($model,$ids){
        if(is_array($ids)){
            $ids = arr2str($ids);
        }
        //开始删除Onethink数据库信息
        $Model = M($model);
        $paths = $Model->where('`id` in ('.$ids.')')->getField('path',true);//先获取物理路径

        $re = $Model->delete($ids);//删除Onethink系统存储的图片数据
        if($re === false){
            $this->error('删除系统文件数据出错');
            exit;
        }elseif($re === 0){
            $this->error('没有删除任何系统存储的文件数据');
            exit;
        }
        $string = '删除系统文件成功！';

        $i=0;$e=0;
        foreach($paths as $file){
            $file = $_SERVER['DOCUMENT_ROOT'].__ROOT__.$file;
            unlink($file)?$i++:$e++;
        }

        $string .= '删除图片物理文件，成功'.$i.'个，失败'.$e.'个！';

        return $string;
    }

}