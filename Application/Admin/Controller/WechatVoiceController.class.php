<?php

// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Common\Api\DocumentApi;
use Wechat\Api\WechatApi;
/**
 * 素材管理控制器
 */
class WechatVoiceController extends WechatMaterialController {

    /**
     * 图片素材列表
     */
    public function index(){
        //构建查询方式
        $Media = M('TchatMedia');

        $map = array(
            'media_type'=>'voice'
        );

        $p = I('param.p')?I('param.p'):1;
        //查询符合条件的图片素材列表
        $list = $Media->where($map)->page($p,20)->select();

        //开始查找每个列表所包含的本地图片
        $Picture = M('File');
        foreach($list as $k => $v){
            //Fixme 查找对应的文件路径
            $list[$k]['path'] = get_cover($v['file_id'],'path');
        }

        $count      = $Media ->where($map)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,20);// 实例化分页类 传入总记录数和每页显示的记录数

        $show       = $Page->show();// 分页显示输出
        //赋值前端调用数据
        $this->assign('_list' , $list);
        $this->assign('page',$show);// 赋值分页输出
        $this->meta_title = '语音素材列表';
        $this->display();

    }

    /**
     * 新增语音素材
     */
    public function addVoice(){
        if(IS_POST||IS_AJAX){
            $voices = I('post.pic_id');
            $pics = str2arr($voices);
            $success = 0;
            $error = 0;
            new WechatApi();
            foreach($voices as $voice){
                $material = add_material(get_file_url($voice,'path'),'voice');

            }

            $str = '本次上传成功'.$success.'个，失败'.$error.'个';
            $return =  array(
                'status'=>1,
                'info'=>$str
            );
            $this->ajaxReturn($return);
        }

        $this->meta_title = '添加语音素材';
        $this->display();
    }

    /**
     * 删除语音素材
     * 删除分为只删除本地、只删除服务器以及同时删除三种情况
     *
     * @param int   $id     图文素材在tchat_media表中的记录ID
     */
    public function deleteVoice($id){

    }

    /**
     * 下载微信端的语音素材
     * 这里是下载已经在系统中存储过的素材
     *
     * @param string   $media_id   微信服务器上的图文素材media_id
     */
    public function downloadVoice($media_id){

    }

}
