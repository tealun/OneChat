<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Common\Api\MailApi;
use User\Api\AccountApi;
use Wechat\Api\ClientApi;
use Wechat\Api\QrCodeApi;

/**
 * 后台对前端信息的控制器
 */
class HomeController extends AdminController {

    /**
     * 前台配置菜单页面
     * @author Tealun Du
     */
    public function index() {
        $this->meta_title = '前台控制';
        $this->display();
    }

}
