<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;
use Wechat\Api\WechatApi;

/**
 * 微信后台管理控制器

 */
class WechatController extends AdminController {

    protected function _initialize() {
        parent::_initialize();
    }

    public function index() {
        $this->assign('meta_title' , '微信管理');
        $this->display();
    }

    /**
     * 内容导航
     * 为添加分类及文章时查找分类和文章的ID及链接提供导航
     */
    public function contentGuide($segment = '') {
        $map = array(
            'status' => 1
        );

        switch ( $segment ) {
            case 'category':
                $guideList = D('Category')->where($map)->select();
                break;

            case 'article':
                $Document = D('Document');
                //只查询pid为0的文章
                $map['pid'] = 0;
                $guideList = $this->lists($Document , $map , 'update_time desc');
                int_to_string($guideList);
                break;

            case'text':
                $guideList = M('Tchat_text')->select();
                break;
            default:
                return false;
                break;
        }
        $this->assign('segment' , $segment);
        $this->assign('guideList' , $guideList);
        $this->display('contentguide');
    }

    public function testWechatConnect(){
        new WechatApi();
        echo save_access_token();
    }

    /**
     * 对select数据进行分页
     *
     * @param       $model
     * @param array $option
     */
    public function setPage($model , $option = array()) {
        $Model = M($model);
        $total = $Model->where($option)->count();
        $page = new \Think\Page($total , 10);
        if ( $total > 10 ) {
            $page->setConfig('theme' , '%FIRST% %UP_PAGE% %LINK_PAGE% %DOWN_PAGE% %END% %HEADER%');
        }
        $p = $page->show();
        $this->assign('_page' , $p ? $p : '');

    }

    /**
     * 获取回复流信息
     * @param $flow_id
     * @param $data
     * @return mixed
     */
    protected function getReplyData($flow_id,$data){
        $flow = M('TchatReplyFlow')->where('`id` = '.$flow_id)->getField('id,module,layer,controller,action_name,title,action_tip',true);
        $flow=$flow[$flow_id];
        //获取回复数据
        $action = array('action_name'=>$flow['action_name'],'action_data'=>$data);
        $reply = A($flow['module'].'/'.$flow['controller'],$flow['layer'])->reply($action);
        switch ($reply[1]){
            case 'text':
            case NULL:
                $flow['action_data'] = $reply[0];
                break;
            case 'news':
                $flow['action_data'] = '回复'.$flow['title'].'图文消息，获取的数据标识为:'.$data;
                break;
            default:
                break;

        }

        unset($flow['id']);//注销掉回复模版的ID，会与项目的ID冲突

        return $flow;
    }

    /**
     * 获取回复流
     * @param $fields
     * @return mixed
     */
    protected function getReplyFlow($fields){

        for ($i = 1; $i <= count($fields); $i++) {
            foreach ($fields[$i] as $k => $field) {
                switch ($field['name']) {
                    case 'flow_id':
                        $extra = $field['extra'];
                        $fields[$i][$k]['extra'] = $this->setReplyExtra($extra);
                        break;
                    case'appid':
                        $mini = M('Tchat_miniprogram')->select();
                        if($mini){
                            $extra = $field['extra'];
                            $fields[$i][$k]['extra'] = $this->setMiniProgramExtra($extra,$mini);
                        }
                        break;
                    default:

                }
            }
        }

        return $fields;
    }

    /**
     * 设置回复流附加数据
     * @param $extra
     * @return string
     */
    private function setReplyExtra($extra){
        $flows = M('TchatReplyFlow')->where('`status` = 1')->select();

        foreach($flows as $flow){
            $extra .="\r\n".$flow['id'].':'.$flow['title'];
            $data[$flow['id']] = array(
                'need'=>$flow['need_data']?true:false,
                'tip' =>$flow['action_tip'],
                'value' =>$flow['default'],
                'holder' =>$flow['data_tip'],
                'title' =>$flow['title'],
            );
        }

        $data = JSON($data);
        $this->assign('replyData',$data);
        return $extra;
    }
    /**
     * 设置小程序附加数据
     * @param $extra
     * @return string
     */
    private function setMiniProgramExtra($extra,$mini){
        $extra?$extra:'';

        foreach($mini as $k =>$v){
            $k===0?
                $extra .=$v['appid'].':'.$v['name']
                :
                $extra .="\r\n".$v['appid'].':'.$v['name'];
        }
        return $extra;
    }

}