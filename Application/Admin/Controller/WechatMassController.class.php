<?php

// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Wechat\Api\WechatApi;
/**
 * 群发消息管理控制器

 */
class WechatMassController extends WechatController {

    protected function _initialize() {
        $this->Wechat = new WechatApi();
        parent::_initialize();
    }
    /**
     * 创建新的群发消息
     *
     * @param $copyId num 要复制的消息ID，为空时不复制直接创建新消息
     */
    public function createMassMessage($copyId = '') {

        if ( IS_POST || IS_AJAX ) {
            if ( I('post.cateId') ) {
                $catIdarr = M('Category')->where(array( 'id' => I('post.cateId') , 'pid' => I('post.cateId') , '_logic' => 'OR' ))->getField('id' , true);
                $map['category_id'] = array( 'in' , $catIdarr );//在指定目录及其子目录下的文档

                /*根据条件取出相应记录，限制10条*/
                $articles = M('Document')->where($map)->getField('id,title' , 10);
                $articles = JSON($articles , true);
                $this->ajaxReturn($articles);
            }
        }

        $tree = D('Category')->getTree(0 , 'id,title,sort');
        $this->assign('tree' , $tree);
        C('_SYS_GET_CATEGORY_TREE_' , true); //标记系统获取分类树模板
        $this->meta_title = '新建群发消息';
        $this->display('create');
    }

    /**
     * 编辑一条群发消息
     * 只针对状态为本地草稿的才可编辑
     */
    public function editMassMessage() {
        $tree = D('Category')->getTree(0 , 'id,title,sort');
        $this->assign('tree' , $tree);
        C('_SYS_GET_CATEGORY_TREE_' , true); //标记系统获取分类树模板
        $this->meta_title = '编辑群发消息';
        $this->display('edit');
    }

    /**
     * 保存草稿
     * 用于保存编辑中的草稿方法
     */
    public function saveDraft() {
        if ( IS_POST || IS_AJAX ) {
            $data = I('.post');

            return D('Mass_message')->update($data);
        }
    }

    /**
     * 查看已发送的消息
     */
    public function listMassMessages() {
        $Document = D('Tchat_mass_massage');
        $map = array( 'status' => array( 'gt' , 1 ) , 'uid' => UID );
        $list = $this->lists($Document , $map);

        $this->assign('list' , $list);
        $this->meta_title = '查看已发送的消息';
        $this->display('list');
    }

    /**
     * 草稿箱
     * @author huajie <banhuajie@163.com>
     */
    public function draftBox() {
        $Document = D('Tchat_mass_massage');
        $map = array( 'status' => 3 , 'uid' => UID );
        $list = $this->lists($Document , $map);

        $this->assign('list' , $list);
        $this->meta_title = '群发消息草稿箱';
        $this->display();
    }

    /**
     * 回收站
     */
    public function recycle() {
        $map['status'] = array( 'eq' , -1 );
        $this->getLists($map);
        $this->meta_title = '群发消息回收站';
        $this->display(); // 输出模板
    }

    public function getArticles() {

    }

    /**
     * 触发群发
     * 群发有两种方式，一种是根据标签群发，一种是根据OPENID群发
     */
    public function sendMassMessage() {
        if ( IS_POST || IS_AJAX ) {
            $data = I("post.");
            if(!$data['tag']){
                $next = S('massNextOpenid')?S('massNextOpenid'):'';
                $openidList = get_wechat_client($next);
                if($openidList['errcode']){
                    $this ->error('获取全部用户信息出错！错误内容为：'.$openidList['errmsg']);
                    exit;
                }
                $touser = $openidList['data']['openid'];
                $nextOpenid = $openidList['data']['next_openid'];
            }else{
                $touser = $data['tag'];
            }
            $res = mass_send($touser,$data['message'],$data['type']);
            if($res['errcode']){
                $this->error('发送消息错误，错误内容为：'.$res['errcode'].$res['errmsg']);
            }

            if($nextOpenid){
                S('massNextOpenid',$nextOpenid,300);
                $this->success('提交了10000个粉丝的发送，是否继续发送其他人？是请继续提交,5分钟内有效');
            }

            /*返回发送状态*/
             $this->success($res['errmsg']);
        }

    }

    /**
     * 设置状态
     * 设置一条群发消息的状态，主要是本地删除和本地恢复操作
     * -1为本地删除[回收站] 0为本地[草稿]未上传 1为已上传微信端[待发送] 2为已提交在[发送中]可刷新状态 3为发送[成功] 4为发送[失败]可重发
     */
    public function setStatus() {

    }

    /**
     * 刷新消息的发送状态
     * 当消息上传到微信端后，消息状态为发送中时，可通过本方法获取一条群发消息的最新状态，并更新到状态中
     *
     * @param $messageId string 微信服务器上的msg_id
     */
    public function renewSendStatus($messageId) {

    }

    /**
     * 从本地和服务器删除一条群发消息
     *
     * @param $messageId string 微信服务器上的msg_id
     */
    public function deleteMessage($messageId) {

    }

    /**
     * 群发预览
     * 将编辑好的消息发送到指定微信号预览排版和样式
     *
     * @param string    $mediaId    预览的素材media_id
     * @param string    $type       预览的素材类型
     *                               图文:"mpnews" 、文本:"text"、语音：:"voice"
     *                               图片:"image" 、视频:"mpvideo" 、卡券:"wxcard"
     * @param string    $towxname   发送到的微信号
     * @param string    $openId     发送到的微信openid，与towxname二选一，都写微信平台优先取用towxname
     *
     * @author Tealun Du
     */
    public function previewMassMessage() {
        if ( IS_POST || IS_AJAX ) {
            $message = I('post.message');
            $towxname = I('post.towxname');
            $type = I('post.type');
            $res = mass_preview($message,$type,$towxname);
            /*返回预览状态*/
            if ( $res === 0 ) { //判断是否成功,恒等于0为成功
                $this->success('已发送到指定账号，请注意查看');
            } else {//错误时返回错误提示
                $this->error($res);
            }
        }
    }

    /**
     * 上传图文消息
     */
    private function uploadNews($news = array()) {
        $url = "https://api.weixin.qq.com/cgi-bin/media/uploadnews?access_token=" . get_access_token();
    }

}
