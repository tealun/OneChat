<?php
namespace Admin\Controller;

/**
 * 友情链接管理类
 * @package Admin\Controller
 */
class FriendshipController extends AdminController {
    function _initialize() {
        parent::_initialize();
    }

    /**
     * 友情链接列表
     */
    public function index() {
        $this->getLists();
        $this->meta_title = '友情链接列表';
        $this->display();
    }

    /**
     * 添加友情链接
     */
    public function create(){
        //获取友情链接模型
        $model = M('Model')->where(array( 'name' => 'friendship' ))->find();
        $info['model_id'] = $model['id'];
        //获取表单字段排序
        $fields = get_model_attribute($model['id']);
        $this->assign('info' , $info);
        $this->assign('fields' , $fields);
        $this->assign('model' , $model);
        $this->meta_title = '新增友情链接';
        $this->display();
    }

    /**
     * 编辑友情链接
     */
    public function edit(){
        if(I('get.id')){
            $id = I('get.id');
            if(!$id)  $this->error('未指定链接');

            //获取相册模型
            $model = M('Model')->where(array( 'name' => 'friendship' ))->find();
            $info['model_id'] = $model['id'];

            //获取表单字段排序
            $fields = get_model_attribute($model['id']);

            $data = M('friendship')->find($id);
            if(!$data) $this->error('未找到该链接');
            $info['id'] = $data['id'];
            $this->assign('info' , $info);
            $this->assign('fields' , $fields);
            $this->assign('model' , $model);
            $this->assign('data',$data);
            $this->meta_title = '编辑友情链接';
            $this->display('create');

        }
    }
    /**
     * 更新友情链接
     */
    public function update(){
        if ( IS_POST || IS_AJAX ) {
            $data = I('post.');

            $Model = M('Friendship');
            if($data['id']){
                $data['update_time']=time();
                $res = $Model->data($data)->save();
                if($res > 0 && $res !== NULL){
                    $this->success('更新成功', U('index') , 1);
                }else{
                    $this->error('更新出错' , U('index') , 3);
                }

            }else{
                $data['create_time']=time();
                $data['update_time']=time();
                $res = $Model->data($data)->add();
                if($res > 0){
                    $this->success('新增成功', U('index') , 1);
                }else{
                    $this->error('新增出错' , U('index') , 3);
                }

            }

        } else {
            $this->error('访问错误' , U('index') , 3);
        }
    }


    /**
     * 更改状态
     * @see Application/Admin/Controller/AdminController::setStatus()
     */
    public function setStatus($model = 'friendship') {
        return parent::setStatus('friendship');
    }

    /**
     * 获取数据列表
     */
    private function getLists($map = '') {
        $list = $this->lists('friendship' , $map);
        col_to_string($list);
        $this->assign('_list' , $list);
    }

}