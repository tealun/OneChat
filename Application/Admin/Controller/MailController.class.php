<?php
namespace Admin\Controller;

/**
 * 系统邮件管理类
 * TODO 使用MailAPI
 * @package Admin\Controller
 */
class MailController extends AdminController {
    function _initialize() {
        parent::_initialize();
    }

    /**
     * 邮件控制列表
     */
    public function index() {
        $this->getLists();
        $this->meta_title = '邮件模版列表';
        $this->display();
    }

    /**
     * 查看邮件详情
     */
    public function info(){
        if(I('get.id')){
            $id = I('get.id');
            $info = D('TchatMail')->info($id);

            $this->meta_title = '查看邮件详情';
            $this->assign('info',$info);
            $this->display();
        }
    }

    /**
     * 查看邮件详情
     */
    public function create(){
            $this->meta_title = '新建邮件模版';
            $this->display();
    }

    /**
     * 查看邮件详情
     */
    public function edit(){
        if(I('get.id')){
            $id = I('get.id');
            if(!$id)  $this->error('未指定模版');

            $info = D('TchatMail')->info($id);

            if($info['mail_type'] == '0')  $this->error('您不能编辑系统自带邮件模版');

            $this->meta_title = '编辑邮件模版';
            $this->assign('info',$info);
            $this->display('create');
        }
    }
    /**
     * 查看邮件详情
     */
    public function update(){
        if ( IS_POST || IS_AJAX ) {
            $res = D('TchatMail')->update();
            if ( !$res ) {
                $this->error(D('TchatMail')->getError());
            } else {
                $this->success($_POST['id'] ? '更新成功' : '新增成功' , U('index') , 1);
            }
        } else {
            $this->error('访问错误' , U('index') , 3);
        }
    }

    /**
     * 自定义发邮件
     *可以在这里发送邮件测试或者发布站点通知
     *
     * @author TealunDu <tealun@guilins.com>
     */
    public function mail(){
        if(IS_POST || IS_AJAX){
            $data = I('post.');
            $re = $this->sendMail($data);
            if($re['status'] == 0){
                $this->error($re['info'],'',10);
            }else{
                $this->success($re['info'],'',10);
            }
        }
        $this->meta_title = '发送邮件';
        $this->display();
    }


    /**
     * 发送邮件
     * @param $data
     *
     * @return array
     *
     * @author TealunDu <tealun@guilins.com>
     */
    private function sendMail($data){

        if($mailId = $data['mailId']){
            $mailset = D('TchatMail')->info($mailId);
            $title = $mailset['mail_title'];
            $body = $mailset['mail_content'];
        }else{
            $title = $data['title'];
            $body = $data['body'];
        }

        $to = $data['to'];
        $att = $data['att'];

        if($to){
            $re = send_mail($to,$title,$body,$att);
            return $re;
        }

    }

    /**
     * 更改状态
     * @see Application/Admin/Controller/AdminController::setStatus()
     */
    public function setStatus($model = 'Tchat_mail') {
        return parent::setStatus('Tchat_mail');
    }

    /**
     * 获取数据列表
     */
    private function getLists($map = '') {
        $list = $this->lists('Tchat_mail' , $map);
        col_to_string($list);
        $this->assign('_list' , $list);
    }

}