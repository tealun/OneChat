<?php

// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

use Wechat\Api\WechatApi;
/**
 * 微信客户管理控制器

 */
class WechatTestController extends WechatController {

    protected $Wechat = null;

    function _initialize(){
        $this->Wechat = new WechatApi();
        parent::_initialize();
    }

    /**
     *
     */
    public function index() {

        $this->meta_title = '微信公众号互动测试模块';
        $this->display(); // 输出模板
    }

}