<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Controller;

/**
 *微信应用模块后台管理控制器

 */
class AppsController extends AdminController {

    /**
     * 微信应用列表
     */
    public function index() {

        $this->display();
    }

    /**
     * 微信应用详情
     */
    public function detail() {

        $this->display();
    }

    /**
     * 添加微信应用
     */
    public function add() {

        $this->display();
    }

    /**
     * 配置微信应用
     */
    public function set() {

        $this->display();
    }

    /**
     * 更新微信应用
     */
    public function update() {

    }

}