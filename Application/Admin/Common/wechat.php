<?php

/**
 * 验证是否属于管理组
 *
 * @param int $uid   用户ID
 * @param int $group 分组ID
 *
 * @return bool
 */
function in_group($uid , $group) {
    $Auth = M('AuthGroupAccess');
    $map = array(
        'uid' => array( 'eq' , $uid ) ,
        'group_id' => array( 'eq' , $group )
    );
    $re = $Auth->where($map)->count();

    return $re ? true : false;
}

/**
 * select返回的数组进行整数映射转换
 *
 * @param array $map                         映射关系二维数组  array(
 *                                           '字段名1'=>array(映射关系数组),
 *                                           '字段名2'=>array(映射关系数组),
 *                                           ......
 *                                           )
 *
 * @author 朱亚杰 <zhuyajie@topthink.net>
 * @return array
 *         array(
 *         array('id'=>1,'title'=>'标题','status'=>'1','status_text'=>'正常')
 *         ....
 *         )

 */
function col_to_string(&$data) {
    $flows = M('TchatReplyFlow')->where('`status` = 1')->getField('id,title',true);

    $map = array(
        'status' => array( 1 => '正常' , -1 => '已删' , 0 => '禁用' ) ,
        'flow_id'=>$flows
    );
    if ( $data === false || $data === NULL ) {
        return $data;
    }
    $data = (array)$data;
    foreach ( $data as $key => $row ) {
        foreach ( $map as $col => $pair ) {
            if ( isset( $row[$col] ) && isset( $pair[$row[$col]] ) ) {
                $data[$key][$col . '_text'] = $pair[$row[$col]];
            }
        }
    }

    return $data;
}

/**
 * 获取二维码扫描关注人数
 */
function get_scan_sub_count($id) {
    $sum = M('Tchat_client')->where('`event_key` = "qrscene_' . $id . '"')->count();

    return $sum;
}

/**
 * 获取通用英文字段名对应的中文名称
 *
 * @param string $k 字段名英文
 *
 * @return string 中文名称
 */
function get_member_key($k) {
    $arr = array(
        "uid"=>"注册ID",                         "username"=>"注册帐号",             "email"=>"绑定邮箱",                 "mobile"=>"绑定手机",
        "remark"=>"备注",                        "nickname"=>"昵称",                 "subscribe"=>"关注状态",            "subscribe_time"=>"关注时间",
        "sex"=>"性别",                            "birthday"=>"出生日期",            "qq"=>"QQ号码",                       "score"=>"积分",
        "login"=>"登陆次数",                     "reg_ip"=>"注册IP",                 "reg_time"=>"注册时间",             "last_login_ip"=>"上次登录IP",
        "last_login_time"=>"上次登录时间",     "status"=>"状态",                    "img"=>"图片",                       "headimgurl"=>"头像",
        "weibo"=>"微博",                         "weixin"=>"微信",                    "country"=>"国家",                   "province"=>"省份",
        "city"=>"城市",                           "street"=>"街道",                   "honour"=>"我的头衔",               "description"=>"描述或简介",
        "language"=>"语言",                      "tagid_list"=>"标签ID列表",        "tags"=>"标签",                      "tag_id"=>"标签",
        "create_time"=>"创建时间",              "update_time"=>"更新时间",         "id"=>"编号",                         "menuid"=>"菜单编号",
        "title"=>"标题",                         "name"=>"姓名或标识",                      "client_platform_type"=>"系统平台","ucenter"=>"账号信息",
        "member"=>"会员中心",                   "wechat"=>"微信"
    );

    $re = $arr[$k]?$arr[$k]:$k;

    return $re;
}
