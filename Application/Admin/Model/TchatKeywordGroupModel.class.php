<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: huajie <banhuajie@163.com>
// +----------------------------------------------------------------------

namespace Admin\Model;

use Think\Model;
use Admin\Model\AuthGroupModel;

/**
 * 关键词分组模型
 */
class TchatKeywordGroupModel extends Model {

    /* 自动验证规则 */
    protected $_validate = array(
        array( 'name' , 'checkName' , '名称已经存在' , self::VALUE_VALIDATE , 'callback' , self::MODEL_BOTH ) ,
        array( 'name' , 'require' , '名称不能为空' , self::MUST_VALIDATE , 'regex' , self::MODEL_BOTH ) ,
        array( 'name' , '1,12' , '标题长度不能超过12个字符' , self::MUST_VALIDATE , 'length' , self::MODEL_BOTH ) ,
        array( 'deadline' , '/^\d{4,4}-\d{1,2}-\d{1,2}(\s\d{1,2}:\d{1,2}(:\d{1,2})?)?$/' , '日期格式不合法,请使用"年-月-日 时:分"格式,全部为数字' , self::VALUE_VALIDATE , 'regex' , self::MODEL_BOTH ) ,
    );

    /* 自动完成规则 */
    protected $_auto = array(
        array( 'uid' , 'is_login' , self::MODEL_INSERT , 'function' ) ,
        array( 'name' , 'htmlspecialchars' , self::MODEL_BOTH , 'function' ) ,
        array( 'start_time' , 'getStartTime' , self::MODEL_BOTH , 'callback' ) ,
        array( 'deadline' , 'getDeadTime' , self::MODEL_BOTH , 'callback' ) ,
        array( 'create_time' , 'time' , self::MODEL_INSERT , 'function' ) ,
        array( 'update_time' , 'time' , self::MODEL_BOTH , 'function' ) ,
    );


    /**
     * 获取详情页数据
     *
     * @param  integer $id 关键词组ID
     *
     * @return array       详细数据
     */
    public function detail($id) {
        /* 获取关键词列表数据 */
        $info = $this->field(true)->find($id);
        if ( !( is_array($info) || 1 !== $info['status'] ) ) {
            $this->error = '关键词组被禁用或已删除！';

            return false;
        }
         return $info;
    }

    /**
     * 新增或更新一个关键词组
     *
     * @param array $data 手动传入的数据
     *
     * @return boolean fasle 失败 ， int  成功 返回完整的数据
     * @author huajie <banhuajie@163.com>
     */
    public function update($data = NULL) {
        $keywordStr = $data['keyword'];
        /* 获取数据对象 */
        $data = $this->create($data);
        if ( empty( $data ) ) {
            return false;
        }

        /* 添加或新增基础内容 */
        if ( empty( $data['id'] ) ) { //新增数据
            $id = $this->add(); //添加关键词分组条目
            if ( !$id ) {
                $this->error = '新增关键词分组出错！';
                return false;
            }
        } else { //更新数据
            $status = $this->save(); //更新基础内容
            if ( false === $status ) {
                $this->error = '更新关键词分组出错！';

                return false;
            }
        }
        return $data;
    }

    /**
     * 生效时间不写则为0
     * @return int 时间戳
     * @author huajie <banhuajie@163.com>
     */
    protected function getStartTime() {
        $start_time = I('post.start_time');

        return $start_time ? strtotime($start_time) : '0';
    }

    /**
     * 失效时间不写则为0
     * @return int 时间戳
     * @author huajie <banhuajie@163.com>
     */
    protected function getDeadTime() {
        $dead_time = I('post.deadline');

        return $dead_time ? strtotime($dead_time) : '0';
    }


    /**
     * 检查标识是否已存在(只需在同一根节点下不重复)
     *
     * @param string $name
     *
     * @return true无重复，false已存在
     * @author huajie <banhuajie@163.com>
     */
    protected function checkName() {
        $name = I('post.name');
        $id = I('post.id' , 0);

        $map = array( 'name' => $name , 'id' => array( 'neq' , $id ) ,'status'=>array('eq',1));
        $res = $this->where($map)->getField('id');
        if ( $res ) {
            return false;
        }

        return true;
    }


}
