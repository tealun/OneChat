<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.dutous.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: TalunDu <tealun@tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Model;

use Think\Model;
use Wechat\Api\MenuApi;
/**
 * 自定义菜单模型
 */
class TchatMenuListModel extends Model {

    /* 自动验证规则 */
    protected $_validate = array(
        array( 'name' , 'checkName' , '标识已经存在' , self::VALUE_VALIDATE , 'callback' , 1 ) ,
        array( 'title' , 'require' , '名称不能为空' , self::MUST_VALIDATE , 'regex' , 3 ) ,
    );

    /* 自动完成规则 */
    protected $_auto = array(
        array( 'name' , 'get_rand_chars' , 1 , 'function' ) ,
        array('menuid','getRandMenuid',1,'callback'),
        array('create_time','time',1,'function'),
        array('update_time','time',3,'function')
    );

    /**
     * 获取菜单列表详细信息
     * 源自分类模型
     *
     * @param  int   $id    个性化菜单ID
     * @param  boolean $field 查询字段
     *
     * @return array     菜单信息
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function info($id , $field = true) {
        /* 获取菜单信息 */
        $map = array();
        if ( is_numeric($id) ) { //通过ID查询
            $map['id'] = $id;
        } else { //通过标识查询
            $map['name'] = $id;
        }

        return $this->field($field)->where($map)->find();
    }

    /**
     * 更新菜单列表信息
     * @return boolean 更新状态
     */
    public function update() {
        $data = $this->create();
        if ( !$data ) { //数据对象创建错误
            return false;
        }
        $data['status'] = 0;//凡是编辑过的数据或新增的数据全部设置为未发布
        if(!$data['id']){//没有设置id则作为新增操作
            $res = $this->add();
            if(!$res){
                $this->error = '新增个性化菜单失败';
            }
        }else{
            $res = $this->save();
            if(false === $res || 0 === $res){
                $this->error = '更新个性化菜单失败';
            }
        }
        return $res;
    }

    /**
     * 删除个性化菜单
     * @param array|mixed $menuid
     *
     * @return bool
     */
    public function delete($menuid){
        $Menu = new MenuApi();
        $re = $Menu->delConditionalMenu($menuid);
        if($re){
            return parent::delete($menuid)?true:false;
        }else{
            return false;
        }
    }


    /**
     * 发布个性化菜单
     *
     * @param int   $menuid 要发布的本地个性化菜单menuid
     * @param array $data   要发布的菜单内容
     *
     * @return array|bool|mixed|string
     */
    public function publish($id,$data){

        if(!$data['button']){
            $re = array('errcode'=> -1,'errmsg'=>'要发布的数据传入或获取错误');
            return $re;
        }

        if(!F('defaultMenuSet')){
            $re = array('errcode'=> -2,'errmsg'=>'要发布个性化菜单，必须先发布默认自定义菜单');
            return $re;
        }

        //获取个性化菜单设置
        $menuList = $this->info($id,'tag_id,sex,country,province,city,client_platform_type,language');

        foreach($menuList as $key => $value){
            if(!empty($value) || $value != 0){
                $data["matchrule"][$key] = $value;
            }
        }

        if(empty($data['matchrule'])){
            $re = array('errcode'=> -3,'errmsg'=>'要发布个性化菜单，必须指定适用范围');
            return $re;
        }

        $Menu = new MenuApi();

        //重新发布个性化菜单
        $re = $Menu->addConditionalMenu($data);
        if($re['menuid']){
            //更新个性化菜单成功时，删除服务器上对应的菜单，因为再次发布会重新生成菜单的ID
            $menuid = $this->where('`id` ='.$id)->getField('menuid');
            $Menu->delConditionalMenu($menuid);

            $data['id'] = $id;
            $data['menuid'] = $re['menuid'];//赋值新的menuid
            $data['status'] = 1;//设置状态为已发布

            $re = $this->data($data)->save();

            if($re > 0){
                $re = array('errcode'=> 0,'errmsg'=>'OK');
            }else{
                $re = array('errcode'=> -4,'errmsg'=>'更新本地个性化菜单的ID失败');
            }
        }

            return $re;

    }

    /**
     * 检查标识是否已存在(只需在同一根节点下不重复)
     *
     * @param string $name
     *
     * @return true无重复，false已存在
     * @author huajie <banhuajie@163.com>
     */
    protected function checkName() {
        $name = I('post.name');
        $map = array('name' => $name );
        $res = $this->where($map)->getField('menuid');
        if ( $res ) {
            return false;
        }

        return true;
    }

    public function getRandMenuid(){
        $id = rand(100000000,999999999);
        $re = $this->where('`menuid` = '.$id)->find();
        /* 添加或更新数据 */
        if ( !$re) {
            return $id;
        } else {
            return $this->getRandMenuid();
        }
    }

}
