<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Model;

use Think\Model;

/**
 * 客户资料模型
 */
class TchatClientModel extends Model {

    /**
     * 获取客户详细信息
     *
     * @param string $openId 客户openId
     * @param array  $field  想要查询的字段数组
     */
    public function detail($id , $field = true) {
        if ( is_numeric($id) ) {
            $map['id'] = $id;
        } else {
            $map['openid'] = $id;
        }

        $info = $this->where($map)->field($field)->find();

        return $info;
    }

    /**
     * 根据ID更新现有客户资料
     *
     * @param $id
     * @param $data
     */
    public function update($data = NULL) {
        /* 获取数据对象 */
        $data = $this->create($data);
        if ( empty( $data ) ) {
            return false;
        }

        /* 添加或新增基础内容 */
        if ( empty( $data['id'] ) ) { //新增数据
            $id = $this->add();
            if ( !$id ) {
                $this->error = '新增客户出错！';

                return false;
            }
        } else { //更新数据
            $status = $this->save(); //更新基础内容
            if ( false === $status ) {
                $this->error = '更新客户出错！';

                return false;
            } else {
                return $status;
            }
        }
    }

}