<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.dutous.com All rights reserved.
// +----------------------------------------------------------------------
// | Author: TalunDu <tealun@tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Model;

use Think\Model;

/**
 * 自定义菜单模型
 */
class TchatMailModel extends Model {

    /* 自动验证规则 */
    protected $_validate = array(
        array( 'name' , 'checkName' , '名称已经存在' , self::VALUE_VALIDATE , 'callback' , self::MODEL_BOTH ) ,
        array( 'name' , 'require' , '名称不能为空' , self::MUST_VALIDATE , 'regex' , self::MODEL_BOTH ) ,
    );

    /* 自动完成规则 */
    protected $_auto = array(
        array( 'name' , 'htmlspecialchars' , self::MODEL_BOTH , 'function' ) ,
        array( 'mail_content' , 'htmlspecialchars' , self::MODEL_BOTH , 'function' ) ,
        array( 'create_time' , 'getCreateTime' , self::MODEL_INSERT , 'callback' ) ,
        array( 'update_time' , 'getCreateTime' , self::MODEL_BOTH , 'callback' ) ,
    );

    /**
     * 获取菜单详细信息
     * 源自分类模型
     *
     * @param  milit   $id    菜单ID或标识
     * @param  boolean $field 查询字段
     *
     * @return array     菜单信息
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function info($id , $field = true) {
        /* 获取菜单信息 */
        $map = array();
        if ( is_numeric($id) ) { //通过ID查询
            $map['id'] = $id;
        } else { //通过标识查询
            $map['name'] = $id;
        }

        return $this->field($field)->where($map)->find();
    }

    /**
     * 更新菜单信息
     * @return boolean 更新状态
     */
    public function update() {
        $data = $this->create();

        if ( !$data ) { //数据对象创建错误
            $this->error = '没有传入数据';
            return false;
        }

        /* 添加或更新数据 */
        if ( empty( $data['id'] ) ) {
            $res = $this->add();
            if(!$res){
                $this->error = '添加数据错误';
                return false;
            }
        } else {
            $res = $this->save();
            if(!$res){
                $this->error = '更新数据错误';
                return false;
            }
        }

        return $res;
    }

    /**
     * 检查标识是否已存在(只需在同一根节点下不重复)
     * @return true无重复 ，false已存在
     * @internal param string $name
     *
     */
    protected function checkName() {
        $name = I('post.name');

        $map = array(  'name' => $name );
        $res = $this->where($map)->getField('id');
        if ( $res ) {
            return false;
        }

        return true;
    }

    /**
     * 创建时间不写则取当前时间
     * @return int 时间戳
     * @author huajie <banhuajie@163.com>
     */
    protected function getCreateTime() {
        $create_time = I('post.create_time');

        return $create_time ? strtotime($create_time) : NOW_TIME;
    }
}
