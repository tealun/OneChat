<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Model;

use Think\Model;

/**
 * 相册模型
 */
class TchatAlbumModel extends Model {
    /* 自动验证规则 */
    protected $_validate = array(
        array( 'title' , 'require' , '名称不能为空' , self::MUST_VALIDATE , 'regex' , self::MODEL_BOTH ) ,
        array( 'title' , '1,50' , '标题长度不能超过50个字符' , self::MUST_VALIDATE , 'length' , self::MODEL_BOTH ) ,
        array( 'cat_id' , 'checkCategory' , '请选择指定的分类' , self::MUST_VALIDATE , 'callback' , self::MODEL_BOTH ) ,
    );

    /* 自动完成规则 */
    protected $_auto = array(
        array( 'title' , 'htmlspecialchars' , self::MODEL_BOTH , 'function' ) ,
        array( 'description' , 'htmlspecialchars' , self::MODEL_BOTH , 'function' ) ,
        array( 'create_time' , 'time' , self::MODEL_INSERT , 'function' ) ,
        array( 'update_time' , 'time' , self::MODEL_BOTH , 'function' ) ,
    );

    /**
     * 获取相册详细信息
     * @return mixed
     */
    public function detail($id , $field = true) {
        if ( is_numeric($id) ) {
            $map['id'] = $id;
        } else {
            $map['name'] = $id;
        }

        $info = $this->where($map)->field($field)->find();

        return $info;
    }

    /**
     * 更新相册资料
     *
     * @param $id
     * @param $data
     */
    public function update($data = NULL) {
        /* 获取数据对象 */
        $data = $this->create($data);
        if ( empty( $data ) ) {
            return false;
        }

        /* 添加或新增基础内容 */
        if ( empty( $data['id'] ) ) { //新增数据
            $status = $this->add();
            if ( !$status ) {
                $this->error = '新增相册出错！';
                return false;
            }
        } else { //更新数据
            $status = $this->save(); //更新基础内容
            if ( false === $status ) {
                $this->error = '更新相册出错！';
                return false;
            }
        }
        return $status;
    }


    /**
     * 自动创建名称标识
     *
     * @return string
     */
    protected function  checkCategory(){
        $cat_id = I('post.cat_id');
        if($cat_id && $cat_id>0){
            $re = M('TchatAlbumCategory')->where('`id`="'.$cat_id.'"')->count();
            return $re>0 ? true : false;
        }else{
            return false;
        }

    }
}