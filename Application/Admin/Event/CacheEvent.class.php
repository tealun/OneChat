<?php
// +----------------------------------------------------------------------
// | OneChat [ 做实用的网站微信内容交互管理平台 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2016 http://www.juexin.pro All rights reserved.
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@guilins.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Admin\Event;

use Think\Controller;
use Wechat\Api\WechatApi;

/**
 * 后台微信缓存回复逻辑
 * data采用统一的数据格式:
 */
class CacheEvent {

    private $data = array();
    private $wechat;
    /**
     * 回复微信流程
     *
     * @param $qrCode
     *
     * @return array
     */
    public function reply($data) {

        $this->wechat = new WechatApi();
        $this->data = $data;
        $reply = $this->$data['action_name']();
        if(!$reply){
            return false;
        }
        return $reply;
    }

    private function listCache(){

        $keyword =  $this->data['keyword'];

        $keywords = $this->data['keywords'];

        /* 判断是否是数组，是就不变动，不是就将字符串转换为数组 */
        $keywords = is_array($keywords) ? $keywords : str2arr($keywords);

        if ( in_array($keyword, $keywords)) {//判断客户发送的关键词是否与缓存继续处理指令相同
            $id = $this->data['listCache'][$keyword];
            $map['id'] = array('eq',$id);
            $flow = M('TchatKeywordGroup')->where($map)->getField('id,flow_id,action_data');
            //查找到相应的回复处理流程
            $reply = M('TchatReplyFlow')->where('`id` = ' . $flow[$id]['flow_id'])->find();
            $reply['action_data'] = $flow[$id]['action_data'];
            $reply['openid'] = $this->data['openid'];
            //跨模块调用回复
            return A($reply['module'] . '/' . $reply['controller'], $reply['layer'])->reply($reply);
        } else {
            return false;
        }
    }

}
