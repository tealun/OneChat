<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Wap\Controller;

/**
 * 全站文档搜索控制器
 * 在全站文章中搜索内容
 */
class SearchController extends HomeController {

    /* 全站搜索 */
    public function index() {

        $keyword = I('param.keyword');
        if(!$keyword){
            $this->error('请输入关键词');
        }
        $p = I('param.p')?I('param.p'):1;

        $map['title']=array('LIKE','%'.$keyword.'%');
        $map['status']=array('eq','1');

        $Document = M('Document'); // 实例化User对象
        // 进行分页数据查询 注意page方法的参数的前面部分是当前的页数使用 $_GET[p]获取
        $list =$Document ->where($map)->page($p,25)->getField('id,title,category_id,view,description,update_time');

        $this->assign('list',$list);// 赋值数据集
        $count      = $Document ->where($map)->count();// 查询满足要求的总记录数
        $Page       = new \Think\Page($count,25);// 实例化分页类 传入总记录数和每页显示的记录数

        $Page->parameter['keyword']   =   $keyword;

        $show       = $Page->show();// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出
        $this->assign('keyword',$keyword);// 赋值分页输出
        $this->meta_title = '搜索结果';
        $this->display();
    }

}