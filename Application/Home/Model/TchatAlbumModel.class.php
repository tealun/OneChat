<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Model;

use Think\Model;

/**
 * 相册模型
 */
class TchatAlbumModel extends Model {

    protected $_auto = array(
        array('likes', 'checkAlbumLike', self::MODEL_UPDATE, 'callback')
    );

    /**
     * 获取相册详细信息
     *
     * @param  milit   $id    相册id
     * @param  boolean $field 查询字段
     *
     * @return array     相册信息
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function info($id , $field = true) {
        /* 获取客户信息 */
        $map = array();
        if ( is_numeric($id) ) { //通过ID查询
            $map['id'] = $id;
        } else { //通过标识查询
            $map['name'] = $id;
        }

        return $this->field($field)->where($map)->find();

    }

    /**
     * TODO like数据增加检测
     * @return bool
     */
    protected function checkAlbumLike(){
        return true;
    }

    /**
     * 查询后解析扩展信息
     * @param  array $data 分类数据
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    protected function _after_find(&$data, $options){
        /* 分割关键字 */
        if(!empty($data['keywords'])){
            $data['keywords'] = explode(',', $data['keywords']);
        }

        /* 数组化扩展属性 */
        if(!empty($data['extend'])){

            $extend = preg_split('/[;\r\n]+/s',$data['extend']);
            $data['extend']=array();
            foreach($extend as $value){
                if(!empty($value)){
                    $arr = explode(':',$value);
                    $data['extend'][$arr[0]]=$arr[1];
                }
            }

        }

    }

    /**
     * 查询后解析扩展信息
     * @param  array $data 分类数据
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    protected function _after_select(&$result, $options){

        foreach($result as $key => $data){

            /* 分割关键字 */
            if(!empty($data['keywords'])){
                $result[$key]['keywords'] = explode(',', $data['keywords']);
            }

            /* 数组化扩展属性 */
            if(!empty($data['extend'])){

                $extend = preg_split('/[;\r\n]+/s',$data['extend']);
                $result[$key]['extend']=array();
                foreach($extend as $value){
                    if(!empty($value)){
                        $arr = explode(':',$value);
                        $result[$key]['extend'][$arr[0]]=$arr[1];
                    }
                }

            }

        }

    }

}
