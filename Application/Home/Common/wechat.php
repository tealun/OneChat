<?php

function timeTip($time) {
    $num = time()-$time;
    if ( $num < 3600 ) {
        $num = floor($num / 60);
        $re = $num . '分钟';
    } elseif ( 3600 <= $num && $num < 86400 ) {
        $num = floor($num / 3600);
        $re = $num . '小时';
    } elseif ( 86400 <= $num && $num < 604800 ) {
        $num = floor($num / 86400);
        $re = $num . '天';
    } elseif ( 604800 <= $num && $num < 31449600 ) {
        $num = floor($num / 604800);
        $re = $num . '周';
    } elseif ( 31449600 <= $num ) {
        $num = floor($num / 31449600);
        $re = $num . '年';
    }

    return $re;
}

/**
 * 查找模型字段名
 */
function get_model_column_name($model_id , $column) {
    /* 非法ID */
    if ( empty( $model_id ) || !is_numeric($model_id) ) {
        return '';
    }

    $map = array( 'model_id' => $model_id );

    return M('attribute')->where($map)->getFieldByName($column , 'title');
}

/**查找用户信息
 *
 * @param        $uid
 * @param string $field
 *
 * @return mixed|完整的数据
 */
function get_user_info($uid , $field = '') {
    $map = array( 'uid' => $uid );
    $re = M('member')->where($map)->getField($field);
    if ( $field == 'img' ) {
        return get_cover($re , 'path');
    } else {
        return $re;
    }
}

/**查找指定分类的指定字段内容
 *
 * @param        $id
 * @param string $field
 *
 * @return mixed|string
 */
function get_category_info($id , $field = "") {

    if ( !$field ) {
        return '没有指定查找字段';
    } else {
        $map = array(
            'id' => array( 'eq' , $id ) ,
            'status' => array( 'eq' , 1 )
        );
        $re = M('category')->where($map)->getField($field);

        return $re;
    }
}

/**
 * 判断是否存在指定分类的子分类
 *
 * @param   int $id 分类ID
 *
 * @return  bool
 */
function check_category_sub($id) {
    $map = array(
        'pid' => array( 'eq' , $id ) ,
        'status' => array( 'eq' , 1 )
    );
    $re = M('category')->where($map)->count();

    return $re ? true : false;
}

/**
 * 获取指定分类的子分类
 *
 * @param   int $id 分类ID
 *
 * @return  bool
 */
function get_category_sub($id) {
    $map = array(
        'pid' => array( 'eq' , $id ) ,
        'status' => array( 'eq' , 1 )
    );
    $re = M('category')->where($map)->getField('id',true);
    return $re ? arr2str($re) : '';
}

/**
 * 通过分类标识查找分类名称
 *
 * @param   string $name 分类的识别标识
 *
 * @return  string          分类名称
 */
function get_category_title_by_name($name) {
    $id = M('category')->where(array( 'name' => $name ))->getField('id');

    return $id ? get_category_title($id) : '分类标识输入有误';
}

function get_album_info($id,$model='TchatAlbum',$field=''){
    if(!$id){
        return false;
    }else{
        $Category = M($model);
        if(empty($field)){
            return $Category->find($id);
        }else{
            return $Category->where('`id` = "'.$id .'"')->getField($field);
        }
    }
}

/**
 * 获取面包屑
 * 基于分类生成分类面包屑
 *
 * @param int    $id    当前分类ID
 * @param string $split 分割符
 *
 * @return string
 */
function get_bread($id , $split = '/',$model='') {

    if(!empty($model)){
        switch($model){
            case 'Album':
                $cateId = get_album_info($id,'TchatAlbum','cat_id');
                $str = get_bread($cateId,'/','TchatAlbumCategory');
                $str .= ' ' . $split . ' <a href="' . U('Home/Album/detail/', array( 'id' => $id )) . '" alt="' . get_album_info($id,'TchatAlbum','title') . '">' . get_album_info($id,'TchatAlbumCategory','title') . '</a>';
                break;
            case 'AlbumCategory':
                $str = '<a href="' . U('Home/Index/index') . '" alt="首页" >首页</a>';
                $str .= ' ' . $split . ' <a href="' . U('Home/Album/lists/', array( 'category' => $id )) . '" alt="' . get_album_info($id,'TchatAlbumCategory','title') . '">' . get_album_info($id,'TchatAlbumCategory','title') . '</a>';
                break;
            default:
                $str = ' ' . $split .' 未定义模型';
        }
    }else{
        $pid = get_category_info($id , 'pid');
        $temp = get_category_info($id , 'allow_publish') ? 'lists' : 'index';

        $str = $pid ?
            get_bread($pid) :
            '<a href="' . U('Home/Index/index') . '" alt="首页" >首页</a>';

        $str .= ' ' . $split . ' <a href="' . U('Home/Article/' . $temp , array( 'category' => $id )) . '" alt="' . get_category_title($id) . '">' . get_category_title($id) . '</a>';
    }


    return $str;
}

/**
 *获取无标签文档详情
 *
 * @param $id
 * @return mixed|string
 */
function get_document_content($id){
    $content = M('document_article')->where('`id`='.$id)->getField('content');
    $content = SpHtml2Text($content);
    return $content;
}


?>
