<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;

/**
 * 相册模型控制器
 * 相册模型列表和详情
 */
class AlbumController extends HomeController {

    /* 相册模型列表页 */
    public function lists() {

        /* 分类信息 */
        $category = $this->category();

        $p = I('get.p')?I('get.p'):1;
        /* 获取当前分类列表 */
        $Album = D('TchatAlbum');
        $where = array(
            'display'=>1,
            'status'=>1,
            'cat_id'=>$category['id']
        );
        $list = $Album
            ->where($where)
            ->order('id DESC')
            ->page($p , $category['list_row'])
            ->select();

        if ( false === $list ) {
            $this->error('获取图册列表数据失败！');
        }
        $total = $Album->where($where)->count();

        $list = $this->rebuildAlbum($list);
         /* 获取模板 */
        if ( !empty( $category['template_lists'] ) ) { //分类已定制模板
            $tmpl = $category['template_lists'];
        } else { //使用默认模板
            $tmpl = 'Album/lists';
        }
        $list_row = (int)$category['list_row'];
        $Page       = new \Think\Page($total,$list_row);// 实例化分页类 传入总记录数和每页显示的记录数
        $show       = $Page->show();// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出

        /* 模板赋值并渲染模板 */
        $this->assign('category' , $category);
        /*为全站SEO添加info赋值*/
        $this->assign('info' , $category);
        $this->assign('list' , $list);
        $this->display($tmpl);
    }

    /* 相册模型详情页 */
    public function detail() {
        $id=I('id');
        $p = I('p')?I('p'):1;
        /* 标识正确性检测 */
        if ( !( $id && is_numeric($id) ) ) {
            $this->error('图册ID错误！');
        }

        /* 获取详细信息 */
        $Album = D('TchatAlbum');
        $info = $Album->info($id);
        if ( !$info ) {
            $this->error($Album->getError());
        }

        /* 分类信息 */
        $category = $this->category($info['cat_id']);

        /* 获取模板 */
        if ( !empty( $info['template'] ) ) {//已定制模板
            $tmpl = $info['template'];
        } elseif ( !empty( $category['template_detail'] ) ) { //分类已定制模板
            $tmpl = $category['template_detail'];
        } else { //使用默认模板
            $tmpl = 'Album/detail';
        }

        /*获取图片*/
        $Picture = M('TchatPicture');
        $map = array(
            'album_id'=>$info['id']
        );
        $pics = $Picture->where($map)->page($p,$info['list_row'])->select();

        /*图片数据分页*/
        $total = $Picture->where($map)->count();
        $list_row = (int)$info['list_row'];
        $Page       = new \Think\Page($total,$list_row);// 实例化分页类 传入总记录数和每页显示的记录数
        $show       = $Page->show();// 分页显示输出
        $this->assign('page',$show);// 赋值分页输出
        /* 更新浏览数
        $map = array( 'id' => $id );
        $Album->where($map)->setInc('view');
        */
        /* 模板赋值并渲染模板 */
        $this->assign('category' , $category);
        $this->assign('info' , $info);
        $this->assign('pictures' , $pics);
        $this->display($tmpl);
    }

    public function picture(){

    }

    public function likeAlbum(){

    }

    public function likePicture(){

    }

    public function favAlbum(){

    }

    public function favPicture(){

    }

    /* 相册分类检测 */
    private function category($id = 0) {
        /* 标识正确性检测 */
        $id = $id ? $id : I('get.category' , 0);
        if ( empty( $id ) ) {
            $this->error('请指定分类！');
        }

        /* 获取分类信息 */
        $category = D('TchatAlbumCategory')->info($id);
        if ( $category && 1 == $category['status'] ) {
            switch ( $category['display'] ) {
                case 0:
                    $this->error('对不起，该分类下图册禁止显示！');
                    break;
                case 2:
                    /* 用户登录检测 */
                    is_login() || $this->error('您还没有登录，请先登录！' , U('User/login'));
                    break;
                default:
                    return $category;
            }
        } else {
            $this->error('图册分类不存在或被禁用！');
        }
    }

    /**
     * 重新组织各个相册列表数据
     *
     * @param $list
     * @return mixed
     */
    private function rebuildAlbum($list){
        $Category = M('TchatAlbumCategory');
        $Picture = M('TchatPicture');
        foreach($list as $k=>$v){
            $list[$k]['category']=$Category->where('`id` ="'.$v['cat_id'].'"')->getField('title');
            $count =$Picture->where('album_id = "'.$v['id'].'"')->count();
            $list[$k]['count']=$count;
            if($count){//查到该相册有图片
                $map['album_id'] =array('eq',$v['id']);
                $i=0;
                if(!$v['cover_id']){
                    $limit = 4;
                }else{
                    $list[$k]['img'][$i]=get_cover($v['cover_id'],'path');
                    $map['pic_id'] = array('neq',$v['cover_id']);
                    $limit = 3;
                    $i++;
                }
                //最多取最新的4张
                $arr = $Picture->where($map)->order('id DESC')->limit($limit)->getField('pic_id',true);
                for($i;$i<4;$i++){

                    if($limit < 4){
                        if($arr[$i-1]){
                            $list[$k]['img'][$i]=get_cover($arr[$i-1],'path');
                        }
                    }else{
                        if($arr[$i]) {
                            $list[$k]['img'][$i] = get_cover($arr[$i], 'path');
                        }
                    }

                }
            }

        }
        return $list;
    }
}
