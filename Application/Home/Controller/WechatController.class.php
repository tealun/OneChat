<?php
// +----------------------------------------------------------------------
// | OneChat
// +----------------------------------------------------------------------
// | Copyright (c) 2014 http://www.tealun.com
// +----------------------------------------------------------------------
// | Author: Tealun Du <tealun@tealun.com> <http://www.tealun.com>
// +----------------------------------------------------------------------

namespace Home\Controller;

use Think\Controller;
use Wechat\Api\WechatApi;
/**
 * 微信接口处理类
 */
class WechatController extends Controller {

    /**
     *微信接入流程
     */
    public function index() {
        $WeChat = new WechatApi();
            //如果不是接入验证，则进入客户消息响应流程
            $WeChat->reply();
    }

}