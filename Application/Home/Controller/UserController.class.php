<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Controller;

use User\Api\UserApi;

//TCHAT 新增类库
use User\Api\AccountApi;
use Common\Api\MailApi;

/**
 * 用户控制器
 * 包括用户中心，用户登录及注册
 */
class UserController extends HomeController {

    /* 用户中心首页 */
    public function index() {

    }

    /* 注册页面 */
    public function register($username = '' , $password = '' , $repassword = '' , $email = '' , $verify = '') {
        if ( !C('USER_ALLOW_REGISTER') ) {
            $this->error('注册已关闭');
        }
        if ( IS_POST ) { //注册用户
            /* 检测验证码 */
            if ( !check_verify($verify) ) {
                $this->error('验证码输入错误！');
            }

            /* 检测密码 */
            if ( $password != $repassword ) {
                $this->error('密码和重复密码不一致！');
            }

            /* 调用注册接口注册用户 */
            $User = new UserApi;
            $uid = $User->register($username , $password , $email);
            if ( 0 < $uid ) { //注册成功
                //OneChat 新增发送邮件通知
                $Mail = new MailApi();
                $user = array(
                    'uid'=>$uid,
                    'nickname'=>$username,
                    'username'=>$username,
                    'password'=>$password,
                    'email'=>$email,
                    'phone'=>'',
                );
                $re = $Mail->sendMail($uid , 1,$user);
                if ( $re['status'] == 0 ) {
                    $this->success('注册成功！,但未成功发送注册邮件，错误代码为：' . $re['info'] , U('login'),10);
                } else {
                    $this->success('注册成功！已将注册邮件发送至您的邮箱。' , U('login') , 10);
                }

            } else { //注册失败，显示错误信息
                $this->error($this->showRegError($uid));
            }

        } else { //显示注册表单
            $this->display();
        }
    }

    /* 登录页面 */
    public function login($username = '' , $password = '' , $verify = '') {
        if ( IS_POST ) { //登录验证
            /* 检测验证码 */
            if ( !check_verify($verify) ) {
                $this->error('验证码输入错误！');
            }

            /* 调用UC登录接口登录 */
            $user = new UserApi;
            $uid = $user->login($username , $password);
            if ( 0 < $uid ) { //UC登录成功
                /* 登录用户 */
                $Member = D('Member');
                if ( $Member->login($uid) ) { //登录用户
                    //TODO:跳转到登录前页面
                    $this->success('登录成功！' , U('Home/Index/index'));
                } else {
                    $this->error($Member->getError());
                }

            } else { //登录失败
                switch ( $uid ) {
                    case -1:
                        $error = '用户不存在或被禁用！';
                        break; //系统级别禁用
                    case -2:
                        $error = '密码错误！';
                        break;
                    default:
                        $error = '未知错误！';
                        break; // 0-接口参数错误（调试阶段使用）
                }
                $this->error($error);
            }

        } else { //显示登录表单
            $this->display();
        }
    }

    /* 退出登录 */
    public function logout() {
        if ( is_login() ) {
            D('Member')->logout();
            $this->success('退出成功！' , U('User/login'));
        } else {
            $this->redirect('User/login');
        }
    }

    /* 验证码，用于登录和注册 */
    public function verify() {
        $verify = new \Think\Verify();
        $verify->entry(1);
    }

    /**
     * 获取用户注册错误信息
     *
     * @param  integer $code 错误编码
     *
     * @return string        错误信息
     */
    private function showRegError($code = 0) {
        switch ( $code ) {
            case -1:
                $error = '用户名长度必须在16个字符以内！';
                break;
            case -2:
                $error = '用户名被禁止注册！';
                break;
            case -3:
                $error = '用户名被占用！';
                break;
            case -4:
                $error = '密码长度必须在6-30个字符之间！';
                break;
            case -5:
                $error = '邮箱格式不正确！';
                break;
            case -6:
                $error = '邮箱长度必须在1-32个字符之间！';
                break;
            case -7:
                $error = '邮箱被禁止注册！';
                break;
            case -8:
                $error = '邮箱被占用！';
                break;
            case -9:
                $error = '手机格式不正确！';
                break;
            case -10:
                $error = '手机被禁止注册！';
                break;
            case -11:
                $error = '手机号被占用！';
                break;
            default:
                $error = '未知错误';
        }

        return $error;
    }


    /**
     * 修改密码提交
     * @author huajie <banhuajie@163.com>
     */
    public function profile() {
        if ( !is_login() ) {
            $this->error('您还没有登陆' , U('User/login'));
        }
        if ( IS_POST ) {
            //获取参数
            $uid = is_login();
            $password = I('post.old');
            $repassword = I('post.repassword');
            $data['password'] = I('post.password');
            empty( $password ) && $this->error('请输入原密码');
            empty( $data['password'] ) && $this->error('请输入新密码');
            empty( $repassword ) && $this->error('请输入确认密码');

            if ( $data['password'] !== $repassword ) {
                $this->error('您输入的新密码与确认密码不一致');
            }

            $Api = new UserApi();
            $res = $Api->updateInfo($uid , $password , $data);
            if ( $res['status'] ) {
                $this->success('修改密码成功！');
            } else {
                $this->error($res['info']);
            }
        } else {
            $this->display();
        }
    }

    /*OneChat 新增功能*/

    /**
     * 忘记密码的重置功能
     *
     * 使用客户帐号或手机号重置密码
     * 生成唯一的验证链接发送值客户注册邮箱
     *
     * @author TealunDu <tealun@guilins.com>
     */
    public function forget() {
        if ( IS_POST || IS_AJAX ) {

            $inputName = I('post.inputName');
            $verify = I('post.verify');
            /* 检测验证码 */
            if ( !check_verify($verify) ) {
                $this->error('验证码输入错误！');
            }
            //获取用户名或邮箱
            if ( is_numeric($inputName) ) {
                $map['mobile'] = array( 'eq' , $inputName );
            } else {
                $inputName = (string)$inputName;
                $map['username'] = $inputName;
            }

            $User = M('UcenterMember');
            $uid = $User->where($map)->getField('id');

            if ( !$uid ) {
                $this->error('没有查找到该账户，请检查输入是否正确。');
            } else {

                //生成重置密码链接。时效24小时，使用一次后失效
                $Account = new AccountApi();
                $url = $Account->buildResetPasswordUrl($uid);

                if ( !$url ) {
                    $this->error('很抱歉，您今天已经提交过3次重置密码申请了，请24小时后再试' , U('index') , 15);
                }
                $Mail = new MailApi();
                $re = $Mail->sendMail($uid , 3 , array( 'urlResetPassword' => $url ));
                if ( $re['status'] == 0 ) {
                    $this->error('发生错误，未能发送重置邮件到您的邮箱，请联系管理员，错误代码为：' . $re['info'] , '' , 15);
                } else {
                    $this->success('已将邮件发送至您的邮箱，请您注意查收重置密码链接。' , U('index') , 15);
                }
                //发送邮件到注册邮箱
            }
        } else {
            $this->display();
        }
    }

    /**
     * 重置密码
     * 修改密码成功后会注销掉token的缓存，确保只能点击一次该链接
     * 但保留resetuid的缓存，因为设定了一天只能申请三次重置，只能等resetUid的缓存过期后才能重新申请
     *
     * @author TealunDu <tealun@guilins.com>
     */
    public function resetPassWord() {
        //确认是否是有效POST提交
        if ( IS_POST || IS_AJAX ) {

            //获取重置的用户的必要参数
            $token = I('post.to');
            $code = I('post.co');
            $email = I('post.email');

            //检测POST数据的有效性
            $Account = new AccountApi();
            $check = $Account->checkResetCode($token , $code , $email);
            if ( $check == -2 ) {
                $this->error('链接有误，to码或co码不能为空！');
            } else if ( $check == -1 ) {
                $this->error('您输入的验证邮箱不正确，请重新输入！');
            } else if ( $check == 0 ) {
                $this->error('co码验证未通过，请联系管理员！');
            } else if ( $check == 1 ) {
                $repassword = I('post.repassword');
                $password = I('post.password');

                empty( $password ) && $this->error('请输入新密码！');
                empty( $repassword ) && $this->error('请输入确认密码！');

                if ( $password !== $repassword ) {
                    $this->error('您输入的新密码与确认密码不一致');
                }

                //获取该token的缓存
                $s = S($token);
                $uid = think_decrypt($s['uid'] , UC_AUTH_KEY);//还原加密过的UID

                //修改密码
                $res = $this->updatePassword($uid , $password);

                if ( $res === false ) {
                    $this->error('修改密码失败，请联系管理员' , '' , 15);
                } else if ( $res === 0 ) {
                    $this->error('没有查找到该用户，或者您输入的是与原密码一致的密码，请更换新密码尝试');
                } else {//成功修改密码
                    //注销掉Token缓存
                    S($token , NULL);
                    //发送新密码已重置邮件
                    //生成重置密码链接。时效24小时，使用一次后失效
                    $Account = new AccountApi();
                    $url = $Account->buildResetPasswordUrl($uid,true);
                    $Mail = new MailApi();
                    $re = $Mail->sendMail($uid , 4 , array( 'urlResetPassword' => $url ));
                    if ( $re['status'] == 0 ) {
                        $this->error('发生错误，未能发送重置成功邮件到您的邮箱，请联系管理员，错误代码为：' . $re['info'] , '' , 15);
                    } else {

                        $this->success('恭喜，您的密码已重置成功。' , U('login') , 15);
                    }
                }
            } else {
                $this->error('验证未通过，请联系管理员！');
            }

        } else {
            //确认是否是有效链接
            if ( I('get.to') && I('get.co') ) {

                $Account = new AccountApi();
                if ( !$Account->checkResetUrl(I('get.to')) ) {
                    $this->error('链接不存在或已失效，请重新申请');
                } else {
                    $resetData['to'] = I('get.to');
                    $resetData['co'] = I('get.co');
                    $this->assign('resetData' , $resetData);
                    //有效链接显示页面
                    $this->display();
                }
            } else {
                $this->error('请确认您的链接正确');
            }
        }
    }

    /**
     * 重置密码提交
     *
     * @param int       $uid        要重设密码的账户id
     * @param string    $password   重设的密码
     *
     * @return bool
     *
     * @author TealunDu <tealun@guilins.com>
     */
    private function updatePassword($uid , $password) {

        $data['password'] = think_ucenter_md5($password , UC_AUTH_KEY);
        $User = M('UcenterMember');
        //更新用户信息
        $data = $User->create($data);

        if ( $data ) {
            return $User->where(array( 'id' => $uid ))->save($data);
        }

        return false;
    }

}
