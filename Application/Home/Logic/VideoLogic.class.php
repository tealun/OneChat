<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace Home\Logic;

/**
 * 文档模型子模型 - 文章模型
 */
class VideoLogic extends BaseLogic {
    /* 自动验证规则 */
    protected $_validate = array(
        array( 'content' , 'require' , '内容不能为空！' , self::MUST_VALIDATE , 'regex' , self::MODEL_BOTH ) ,
    );

    /* 自动完成规则 */
    protected $_auto = array();


    /**
     * 获取模型详细信息
     *
     * @param  integer $id 文档ID
     *
     * @return array       当前模型详细信息
     */
    public function detail($id) {
        $data = $this->field(true)->find($id);//获取视频文档扩展信息
        $data = array_merge($data , D('Document')->find($id));//获取视频文档基本信息并合并成DATA
        if ( !$data ) {
            $this->error = '获取详细信息出错！';

            return false;
        }
        if ( $data['pid'] != "0" ) {
            //当前文档为子文档时，查找其父文档以及同级子文档，同级文档组成文章列表
            $data['father'] = D('Document')->field(array( 'id' , 'title' , 'description' , 'tags' ))->find($data['pid']);
            $data['tags'] = str2arr($data['father']['tags']);//指定父视频的标签
            $data['list'] = D('Document')->where(array( 'pid' => $data['pid'] ))->field(array( 'id' , 'title,description' ))->select();
        } else {
            $data['father'] = array( 'id' => $id , 'title' => $data['title'] , 'description' => $data['description'] );
            $data['tags'] = str2arr($data['tags']);//指定视频的标签
            //如果当前文档为根文档，则查找其子文档，组成文章列表
            $data['list'] = D('Document')->where(array( 'pid' => $id ))->field(array( 'id' , 'title,description' ))->select();
        }

        $data['count'] = count($data['list']);//统计所有子视频的数量
        $data['tid'] == 0 ? $author = $data['uid'] : $author = (int)$data['tid'];
        $data['author'] = M('Member')->where(array( 'uid' => $author ))->find();//获取当前视频作者信息
        $data['playCode'] = $this->findPlayCode($data['video_url']);

        return $data;
    }

    /**
     * 新增或添加一条文章详情
     *
     * @param  number $id 文章ID
     *
     * @return boolean    true-操作成功，false-操作失败
     * @author 麦当苗儿 <zuojiazi@vip.qq.com>
     */
    public function update($id) {
        /* 获取文章数据 */ //TODO: 根据不同用户获取允许更改或添加的字段
        $data = $this->create();
        if ( !$data ) {
            return false;
        }

        /* 添加或更新数据 */
        if ( empty( $data['id'] ) ) {//新增数据
            $data['id'] = $id;
            $id = $this->add($data);
            if ( !$id ) {
                $this->error = '新增详细内容失败！';

                return false;
            }
        } else { //更新数据
            $status = $this->save($data);
            if ( false === $status ) {
                $this->error = '更新详细内容失败！';

                return false;
            }
        }

        return true;
    }

    /**
     * 转换视频播放代码
     * 从网址中解析出视频播放代码
     * TODO 找出其他网站视频播放的方法
     *
     * @param $url string 视频的播放地址
     *
     * @return mixed string
     */
    private function findPlayCode($url) {
        $preg = array(
            'tudou' => '/tudou\.com/' ,
            'youku' => '/youku\.com/' ,
            'qq' => '/qq\.com/'
        );

        foreach ( $preg as $k => $value ) {
            if ( preg_match($value , $url) ) {
                $pregResult = $k;
            }
        }

        if ( $pregResult ) {
            switch ( $pregResult ) {
                case'tudou'://土豆视频输出
                    preg_match('/\/programs\//' , $url , $style); //检测是否是视频本身页面的播放地址
                    if ( $style ) {
                        preg_match('/view\/(.+)\/?/' , $url , $matchs); //获取视频的VID
                        $vid = $matchs[1];
                    } else {//不是最终视频地址，就在URL中查找VID，一般这种地址，土豆会给一个.html后缀
                        preg_match('/(.+\/)?(.+)(?=\.html)/' , $url , $matchs);//紧挨该后缀名的就是VID
                        $vid = $matchs[2];
                    }
                    //赋值代码
                    $code = '<object width="100%" height="600"><param name="movie" value="http://www.tudou.com/v/' . $vid . '"></param><param name="allowFullScreen" value="true"></param><param name="allowscriptaccess" value="always"></param><param name="wmode" value="opaque"></param><embed src="http://www.tudou.com/v/' . $vid . '" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" wmode="opaque" width="100%" height="600"></embed></object>';
                    break;
                case'youku'://优酷视频输出
                    $htmlPage = file_get_contents($url);//直接拉取优酷的页面代码，因为按照其他方式用来做优酷的代码输出会出错。直接从页面中提取播放代码就可以
                    preg_match('/(\<iframe ).*(src=".*" ).*(allowfullscreen.*iframe>)/' , $htmlPage , $matchs);//提取播放代码'


                    unset( $htmlPage );//注销掉页面的缓存变量
                    $code = '<div class="embed-responsive embed-responsive-4by3">';
                    $code .= $matchs[1] . $matchs[2] . $matchs[3];
                    $code .= '</div>';

                    break;
                case 'qq':
                    preg_match('/vid=(.+)/' , $url , $matchs);//获取腾讯视频网址中的VID，最喜欢这个，最容易提取，就在URL的最后面给出
                    $vid = $matchs[1];
                    //赋值代码
                    $code = '<iframe class="embed-responsive-item"
                            src="http://v.qq.com/iframe/player.html?vid=' . $vid . '"auto=0
                            allowfullscreen="1" frameborder="0"></iframe>';
                    break;
                default:
                    $code = '<div class="jumbotron"><h1>啊哦，出错了！</h1>
                                <p>非常抱歉，这个视频正在网上遨游，系统暂不支持除腾讯、优酷和土豆外的其他视频播放地址，敬请期待升级</p>
                                <p>您可以暂时先到该网站直接观看。<a class="btn btn-primary" href="' . $url . '" target="_blank" role="button">现在就去！</a></p>
                                </div>';
            }
        }

        //返回播放代码
        return $code;
    }

}
