<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace Common\Api;
class MailApi {

    /**
     * 供全局调用的邮件发送API端口
     * 说明：
     * 系统级别是在邮件模型的构建函数中设置的替换数组
     * 代码级别就是你在构建方法中设置的一些替换数组
     * 自定义级别就是邮件模版数据表中设定的替换数组
     * 生效覆盖顺序是：自定义级别->代码级别->系统级别
     * 数组构成： array(
     *              '邮件模版中的标签名称'=>'要替换的内容',
     *              'urlResetPassword'=>$url
     *                  )
     *
     * @param   int     $uid        收件人uid
     * @param   int     $mailId     发送的邮件模版编号
     * @param   array   $preg       代码级别的模版标签替换数组
     * @param   array   $attachment 要添加的附件
     *
*@return array
     */
    public function sendMail($uid,$mailId,$preg=array(),$attachment=array()) {
        $to = M('UcenterMember')->where('`id` = '.$uid)->getField('email');
        $mail = D('TchatMail')->getMail($uid,$mailId,$preg);
        return send_mail($to,$mail['mail_title'],$mail['mail_content'],$attachment);
    }

}