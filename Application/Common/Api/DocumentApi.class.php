<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------

namespace Common\Api;
class DocumentApi {
    public function info($id){
        $Article = M('document');
        $map = array(
            'status'=>array('eq',1),
            'display'=>array('eq',1),
        );
        $info = $Article->where($map)->find($id);
        if(!$info){
            $info = array(
                'errcode'=>0,
                'errmsg'=>'没有找到指定文章'
            );
        }

        $info = $this->resetArticle($info);

        return $info;
    }

    /**
     * 重新整理文章内容详情
     *
     * @param $info
     * @return mixed
     */
    private function resetArticle($info){

        foreach($info as $key => $value){
            switch ($key){
                case 'uid':
                    $info[$key] = get_nickname($value);
                    break;
                case 'category_id':
                    $info[$key] = get_category_title($value);
                    break;
                case 'create_time':
                case 'update_time':
                case 'deadline':
                    if(!empty($value)  && $value > 0 ){
                        $info[$key] = time_format($value);
                    }
                    break;
                case 'cover_id':
                    if($value > 0){
                        $info['cover'] = get_cover($value,'path');
                    }else{
                        $info['cover'] = __ROOT__ . '/Public/Home/images/onechat/big/' . rand(1 , 29) . '.jpg';
                    }
                    break;
                case 'index_pic':
                    if($value > 0){
                        $info['index'] = get_cover($value,'path');
                    }else{
                        $info['index'] = __ROOT__ . '/Public/Home/images/onechat/small/' . rand(1 , 29) . '.jpg';
                    }
                    break;
                case 'link_id':
                    if($value > 0){
                        $info[$key] = get_link($value);
                    }
                    break;
                case 'model_id':
                    $info['model'] = get_table_field($value,'id','title','model');
                    $modelName = get_table_field($value,'id','name','model');
                    $info['content'] = M('Document'.ucfirst($modelName))
                        ->where('`id` = '.$info['id'])->getField('content');
                    if($value == 56){//视频类型文档
                        $info['video_url'] = M('Document'.ucfirst($modelName))
                            ->where('`id` = '.$info['id'])->getField('video_url');
                    }
                    break;
                case 'tags':
                    if(!empty($value)){
                        $info[$key] = str2arr($value);
                    }
                    break;
                default:
            }

        }
        return $info;
    }
}