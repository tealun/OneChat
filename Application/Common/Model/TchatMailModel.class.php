<?php

namespace Common\Model;

use Think\Model;
use Admin\Model\AuthGroupModel;

/**
 * 邮件模型
 */
class TchatMailModel extends Model {

    protected $preg =array();

    /**
     * 初始化邮件模型
     */
    protected function _initialize() {
        //系统级模版标签替换设置
        $this->preg = array(
            'webSite'=>C('WEB_SITE_TITLE'),
            'dateTime'=>date('Y-m-d H:i:s',time())
        );
    }
    /**
     * 获取邮件内容
     *
     * @param   int     $uid    受影响的用户uid
     * @param   int     $id     要获取的邮件模版编号
     * @param  array    $preg   模版标签替换数组 array( '匹配内容1'=>'替换内容1',['匹配内容2'=>'替换内容2'],...)
     * @return mixed            返回邮件标题、内容
     *
     */
    public function getMail($uid=UID,$id,$preg=array()){
        $info = $this->find($id);
        $preg['signName']= !empty($info['sign_name'])?$info['sign_name']:'<p>Tchat内容管理平台</p>';//将邮件签名信息赋值到替换模版

        //此处为代码级别的模版标签替换，必须先于下面的自定义级别模版标签替换调用
        $this->setPreg($uid,$preg);

        //此处为自定义级别的模版标签替换,也是有冲突的模版标签最终生效的替换设置
        $info['mail_title'] = $this->pregMailContent($info['mail_title'],$info['preg_content']);
        $info['mail_content'] = $this->pregMailContent($info['mail_content'],$info['preg_content']);
        return $info;
    }

    /**
     * 自定义模版标签替换
     *
     * 替换默认设置，优先顺序：覆盖自定义->代码级->系统级
     * 系统代码级别的设置，需要在自定义替换代码之前调用，否则会替换掉自定义设置。
     *
     * @param $content
     * @param $preg
     * @return mixed
     */
    private function pregMailContent($content,$preg){

        if(!empty($preg)){
            //转圜自定义模版标签替换数组
            $arr = explode("\r\n",$preg);
            foreach ($arr as $v){
                if($v){
                    $a = explode(":",$v);
                    $this->preg[$a[0]]=$a[1];
                }
            }
        }
        //替换掉邮件内容中的模版标签
        $pattern =array_keys($this->preg);
        foreach($pattern as $k=>$v){
            $pattern[$k]= '/\{'.$v.'\}/';
        }
        $replacement =array_values($this->preg);
        $content = preg_replace($pattern,$replacement,$content);

        return $content;
    }

    /**
     * 代码级模版标签替换
     *
     * 替换默认设置，覆盖优先顺序：自定义->代码级->方法内的默认值
     * 系统代码级别的设置，需要在自定义替换代码之前调用，否则会替换掉自定义设置。
     * @param $uid 受影响的用户uid
     * @param $preg 替换数组 array( '匹配内容1'=>'替换内容1',['匹配内容2'=>'替换内容2'],...)
     */
    private function setPreg($uid,$preg){

        $this->preg['userName'] = get_username($uid);
        if($preg){
            foreach ($preg as $k=>$v){
                $this->preg[$k]=$v;
            }
        }

    }


}

