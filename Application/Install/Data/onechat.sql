-- -----------------------------
-- 说明：本SQL中的新建表均会增加Tchat前缀，表名中不包含Tchat的即为系统自带表
-- 除非是文档模型扩展
-- 本SQL命令文件大多数情况下只向表中添加必要数据，轻易不更改其表原结构
-- 对原表结构的更改统一放在 change.sql中
-- -----------------------------

/*向管理员组添加权限主要为添加TCHAT权限*/
UPDATE `onethink_auth_group` SET `title`='管理员组',`description` = '拥有系统最高权限的管理帐号组',
  `rules` = '1,2,3,5,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,46,51,53,61,62,63,64,65,66,67,68,69,70,71,72,73,74,79,80,81,82,83,84,86,87,88,107,108,109,110,195,204,211,212,214,215,216,258,259,260,261,262,263,300,301,302,303,304,305,306,310,311,312,313,314,315,320,321,322,323,324,325,330,331,332,333,334,335,336,350,351,352,353,354,355,356,357,358,359,360,361,362,366,367,368,369,370,371,372,373,374,375,380,381,382,383,390,391,392,393,394,395,396,397,398,399,420,421,422,423,424,425,426,427,428,429,440,441,442,443,460,461,462,463,480,481,482,483,500,,520,521,522,523,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,1600,1601,1602,1603,1604,1605,1606,1607,1608,1609,1610,1611,1612,1620,1621,1622,1623,1624,1650,1651,1652,1653,1654,1655,1656,1657,16558,1660,1661,1662,1663,1664' WHERE `id` ='2' limit 1;

-- -----------------------------
-- Records of  `onethink_addons`
-- -----------------------------

/*Tchat插件*/
INSERT INTO `onethink_addons` VALUES ('300', 'HomeControl', '前台内容控制插件', '基于bootstrap 3.2.0实现的前台首页内容控制插件，需要Admin板块控制器和视图配合，及创建菜单和权限', '1', '', 'Tealun Du', '0.2', '1380273962', '0');
INSERT INTO `onethink_addons` VALUES ('301', 'Friendship', '友情链接插件', '用于管理友情链接', '1', '', 'Tealun Du', '0.1', '1380273962', '0');
INSERT INTO `onethink_addons` VALUES ('302', 'RecommendedApps', '推荐应用', '我们向您推荐诸多有用的应用信息', '1', '{\"title\":\"\\u63a8\\u8350\\u5e94\\u7528\",\"width\":\"2\",\"display\":\"1\"}', 'Tealun Du', '0.1', '1380273962', '0');

-- -----------------------------
-- Table structure for `onethink_friendship`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_friendship`;
CREATE TABLE `onethink_friendship` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '友链ID',
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图标ID',
  `title` VARCHAR (20) NOT NULL DEFAULT '' COMMENT '标题',
	`url` varchar(255) NOT NULL COMMENT '链接地址',
	`description` varchar(255) NOT NULL DEFAULT '' COMMENT '链接描述',
	`group` varchar(255) NOT NULL DEFAULT 'default' COMMENT '分组',
	`type` tinyint(2) NOT NULL DEFAULT '2' COMMENT '类型，1文本，2图标',
	`status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态，-1为删除，0禁用，1启用',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '友情链接列表';

-- -----------------------------
-- Records of  `onethink_hooks`
-- 升级后在最后增加了"status"状态字段
-- -----------------------------
/*更新HomeControl的钩子*/
UPDATE `onethink_hooks` SET `addons` = 'Attachment,HomeControl' WHERE `id` = '4' LIMIT 1;
UPDATE `onethink_hooks` SET `addons` = 'HomeControl' WHERE `id` = '5' LIMIT 1;
UPDATE `onethink_hooks` SET `addons` = 'SiteStat,SystemInfo,RecommendedApps' WHERE `id` = '13' LIMIT 1;
/*Tchat插件钩子*/

INSERT INTO `onethink_hooks`  VALUES ('31', 'homeSlide', '首页幻灯片位置', '1', '1397114797', 'HomeControl', '1');
INSERT INTO `onethink_hooks`  VALUES ('32', 'homeLogo', '站点LOGO图片展示', '1', '1397114797', 'HomeControl', '1');
INSERT INTO `onethink_hooks`  VALUES ('33', 'homeFeature', '站点首页特色栏目展示', '1', '1397114797', 'HomeControl', '1');
INSERT INTO `onethink_hooks`  VALUES ('34', 'banner', '挂载Banner图片或幻灯片', '1', '1397114797', 'HomeControl', '1');
INSERT INTO `onethink_hooks`  VALUES ('35', 'homeLists', '挂载指定分类文章列表', '1', '1397114797', 'HomeControl', '1');
INSERT INTO `onethink_hooks`  VALUES ('36', 'friendshipLink', '挂载友情链接', '1', '1397114797', 'Friendship', '1');
-- -----------------------------
-- Records of  `onethink_attribute`
-- -----------------------------
INSERT INTO `onethink_attribute` VALUES ('190','index_pic','索引图片', 'int(10) unsigned NOT NULL ', 'picture', '0', '0-无索引图，大于0-索引图片ID，用于列表方形图片索引，尺寸建议为200*200像素', '1', '', '1', '0', '1', '1384147827', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('191','tags','标签', 'varchar(250) NOT NULL ', 'string', '', '多个标签请用英文半角逗号隔开', '1', '', '1', '0', '1', '1394597230', '1394597230', '', '0', '', '', '', '0', '');
-- Attribute of keyword group model
INSERT INTO `onethink_attribute` VALUES ('202', 'name', '关键词标题', 'varchar(100) NOT NULL ', 'string', '', '为本组关键词取个标题', '1', '', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('205', 'flow_id', '回复内容', 'int(10) NOT NULL ', 'select', '0', '请选择回复内容', '1', '0:请选择', '5', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('206', 'action_data', '数据', 'varchar(300) NOT NULL ', 'textarea', '', '请填写所需数据', '1', '', '5', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('207', 'start_time', '生效时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '设置关键词的<strong>生效时间</strong>', '1', '', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('208', 'deadline', '失效时间', 'int(10) unsigned NOT NULL ', 'datetime', '0', '设置关键词的<strong>失效时间</strong>', '1', '', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('209', 'dead_text', '失效后回复文本', 'varchar(255) NOT NULL ', 'string', '55', '', '1', '', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('210', 'status', '是否现在启用', 'tinyint(2) NOT NULL ', 'bool', '1', '设置添加后的状态', '1', '1:启用\r\n0:禁用', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('211', 'uid', '用户ID', 'int(10) unsigned NOT NULL ', 'num', '0', '', '0', '', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('212', 'create_time', '创建时间', 'int(10) unsigned NOT NULL ', 'datetime', 'CURRENT_TIMESTAMP', '', '0', '', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('213', 'keywords', '关键词', 'varchar(300) NOT NULL ', 'string', '', '设置关键词（多个关键词请用英文半角逗号隔开）', '1', '', '5', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');

-- Attribute of events model
INSERT INTO `onethink_attribute` VALUES ('400', 'flow_id', '回复内容', 'int(10) NOT NULL ', 'select', '0', '请选择回复内容', '1', '0:请选择', '50', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('401', 'action_data', '数据', 'varchar(300) NOT NULL ', 'textarea', '', '请填写所需数据', '1', '', '50', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- Attribute of qrcode model
INSERT INTO `onethink_attribute` VALUES ('410', 'scene', '应用场景', 'varchar(50) NOT NULL ', 'string', '', '该二维码使用的场景名称，比如：机场海报用、美团网站用等', '1', '', '51', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('411', 'flow_id', '回复内容', 'int(10) NOT NULL ', 'select', '0', '请选择回复内容', '1', '0:请选择', '51', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('412', 'action_data', '数据', 'varchar(300) NOT NULL ', 'textarea', '', '请填写所需数据', '1', '', '51', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('413', 'status', '是否现在启用', 'tinyint(2) NOT NULL ', 'bool', '1', '设置添加后的状态', '1', '1:启用\r\n0:禁用', '51', '0', '1', '1394597354', '1394597354', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('414', 'ticket', '应用场景', 'varchar(250) NOT NULL ', 'string', '', '二维码的ticket', '0', '', '51', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- Attribute of album model
INSERT INTO `onethink_attribute` VALUES ('420', 'name', '相册标识', 'varchar(200) NOT NULL ', 'string', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('421', 'title', '相册名称', 'varchar(200) NOT NULL ', 'string', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('422', 'description', '相册描述', 'varchar(255) NOT NULL ', 'textarea', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('423', 'content', '相册详细内容', 'text NOT NULL ', 'editor', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('424', 'cat_id', '所属分类ID', 'int(10) NOT NULL ', 'select', '0', '', '1', '0:无分类', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('425', 'allow_publish', '是否允许投稿',  'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '1', '0:不允许\r\n1:允许', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('426', 'reply', '是否允许回复',  'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '1', '0:不允许\r\n1:允许', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('427', 'check', '发布是否需要审核',  'tinyint(3) unsigned NOT NULL ', 'select', '0', '', '1', '0:不需要\r\n1:需要', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('428', 'meta_title', 'SEO的网页标题', 'varchar(255) NOT NULL ', 'textarea', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('429', 'keywords', '关键字', 'varchar(255) NOT NULL ', 'string', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('430', 'link_id', '外链', 'varchar(255) NOT NULL ', 'string', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('431', 'display', '是否私有', 'tinyint(2) NOT NULL ', 'bool', '1', '设置相册私有性', '0', '1:公开\r\n0:私有', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('432', 'max_limit', '上传数量限制',  'int(10) NOT NULL ', 'num', '9999', '默认9999', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('433', 'vote', '投票设置', 'tinyint(2) NOT NULL ', 'bool', '0', '是否启用投票', '1', '1:是\r\n0:否', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('434', 'template', '自定义模版', 'varchar(100) NOT NULL ','string', '', '', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('435', 'extend', '属性扩展', 'text NOT NULL', 'textarea', '', '每行一个属性扩展，格式： “属性名称：属性值”，如： “spacial:美”', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('436', 'list_row', '每页展示图片数量',  'tinyint(3) unsigned NOT NULL ', 'num', '12', '默认12', '1', '', '61', '0', '1', '1396970452', '1396970452', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('437', 'sort', '排序', 'int(10) NOT NULL ', 'string', '1', '相册的排列顺序，同一分类下有效', '1', '', '61', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- Attribute of menu model
INSERT INTO `onethink_attribute` VALUES ('450', 'sort', '排序', 'tinyint(2) NOT NULL ', 'string', '0', '菜单的排列顺序，同一级别下有效', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('451', 'menu_type', '菜单类型', 'varchar(20) NOT NULL ', 'select', 'click', '设置本菜单的类型，是作为一级菜单还是获取回复信息又或者是跳转到网页', '1', 'click:获取回复\r\nbutton:一级菜单\r\nview:转到网址\r\nminiprogram:跳转小程序\r\npic_photo_or_album:拍照或发图', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('452', 'name', '显示名称', 'varchar(50) NOT NULL ', 'string', '', '菜单上显示的名称', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('453', 'appid', '跳转的小程序', 'varchar(20) NOT NULL ', 'select', '', '请选择要跳转的小程序，若没有请先添加', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('454', 'flow_id', '回复内容', 'int(10) NOT NULL ', 'select', '0', '请选择回复内容', '1', '0:请选择', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('455', 'action_data', '数据', 'varchar(300) NOT NULL ', 'textarea', '', '请填写所需数据', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('456', 'status', '状态', 'tinyint(2) NOT NULL ', 'bool', '1', '是否现在就启用', '1', '1:启用\r\n0:禁用', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('457', 'pid', '上级菜单', 'int(10) unsigned NOT NULL ', 'string', '0', '请选择上级菜单', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('458', 'uid', '用户', 'int(10) unsigned NOT NULL ', 'string', '2', '', '0', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('459', 'create_time', '创建时间', 'int(10) unsigned NOT NULL ', 'string', '0', '', '0', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('460', 'update_time', '更新时间', 'int(10) unsigned NOT NULL ', 'string', '0', '', '0', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('461', 'url', '跳转链接', 'varchar(250) NOT NULL ', 'string', '0', '请填写点击后跳转到的网址，注意一定要带上"http://"或者"https://"', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('462', 'menuid', '所属菜单类型id', 'int(10) NOT NULL ', 'num', '0', '所属菜单的类型，自定义菜单为0，个性化菜单为menuid数值', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('463', 'page_path', '小程序页面路径', 'varchar(250) NOT NULL ', 'string', '0', '请填写跳转的小程序页面路径，若不清楚，可先咨询小程序开发者。', '1', '', '52', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- Attribute of menu_list model
INSERT INTO `onethink_attribute` VALUES ('470', 'menuid', '个性化菜单id', 'int(10) NOT NULL ', 'string', '0', '个性化菜单的ID，由微信服务器生成', '1', '', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
-- deleted id 471 'name' this field will fill automatic by model class
INSERT INTO `onethink_attribute` VALUES ('472', 'title', '名称', 'varchar(100) NOT NULL ', 'string', '', '个性化菜单名称', '1', '', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('473', 'tag_id', '适用标签编号', 'int(10) unsigned NOT NULL ', 'string', '0', '请输入适用的标签编号', '1', '', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('474', 'sex', '性别', 'tinyint(2) NOT NULL ', 'select', '0', '请选择适用的性别', '1', '0:不设置\r\n1:适用于男性\r\n2:适用于女性', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('475', 'country', '国家', 'varchar(100) NOT NULL ', 'string', '中国', '请输入适用的国家', '1', '', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('476', 'province', '省份', 'varchar(100) NOT NULL ', 'string', '', '请输入适用的省份', '1', '', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('477', 'city', '城市', 'varchar(100) NOT NULL ', 'string', '', '请输入适用的城市', '1', '', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('478', 'client_platform_type', '操作系统', 'tinyint(2) NOT NULL ', 'select', '0', '请选择适用的操作系统', '1', '0:不设置\r\n1:IOS\r\n2:Android\r\n3:其他', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('479', 'language', '语言', 'varchar(20) NOT NULL ', 'select', 'zh_CN', '请选择适用语言', '1', '0:不设置\r\nzh_CN:简体中文\r\nzh_TW:繁体中文TW\r\nzh_HK:繁体中文HK\r\nen:英文\r\nid:印尼\r\nms:马来\r\nes:西班牙\r\nko:韩国\r\nit:意大利\r\nja:日本\r\npl:波兰\r\npt:葡萄牙\r\nru:俄国\r\nth:泰文\r\nvi:越南\r\nar:阿拉伯语\r\nhi:北印度\r\nhe:希伯来\r\ntr:土耳其\r\nde:德语\r\nfr:法语', '53', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- Attribute of Wechat Reply Flow
INSERT INTO `onethink_attribute` VALUES ('500', 'module', '处理模块', 'varchar(20) NOT NULL ', 'string', '', '业务逻辑的处理模块，如：Admin', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('501', 'layer', '控制器目录', 'varchar(20) NOT NULL ', 'string', 'Event', '业务逻辑所在的目录，如:Event', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('502', 'Controller', '处理控制器', 'varchar(20) NOT NULL ', 'string', 'Reply', '处理回复内容的控制器(不需要全名和后缀)', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('503', 'action_name', '方法名称', 'varchar(20) NOT NULL ', 'string', '', '要执行的方法（不同的模块有不同的方法）', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('504', 'title', '回复流名称', 'varchar(20) NOT NULL ', 'string', '', '本回复流模版的名称，如：回复文本', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('505', 'action_tip', '方法描述', 'varchar(100) NOT NULL ', 'textarea', '', '对要执行的方法做简单描述，如：回复一个文本消息', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('506', 'need_data', '是否需要数据', 'tinyint(2) NOT NULL ', 'bool', '1', '是否需要传入数据，0不需要，1需要,默认需要', '1', '0:不需要\r\n1:需要', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('507', 'data_type', '数据类型', 'tinyint(2) NOT NULL ', 'select', '0', '需要传入的数据类型', '1', '0:字符串\r\n1:长文本\r\n2:数字\r\n3:数组', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('508', 'data_default', '数据默认值', 'varchar(100) NOT NULL ', 'string', '', '在调用该回复时默认填写的数据值,如：1', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('509', 'data_tip', '数据填写说明', 'varchar(100) NOT NULL ', 'textarea', '', '简单描述数据的填写说明，如：请填写商品ID；或：可填多个，每行1个', '1', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('510', 'status', '状态', 'tinyint(2) NOT NULL ', 'select', '1', '是否启用', '1', '0:停用\r\n1:启用', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('511', 'create_time', '创建时间', 'int(10) unsigned NOT NULL ', 'string', '0', '', '0', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('512', 'update_time', '更新时间', 'int(10) unsigned NOT NULL ', 'string', '0', '', '0', '', '54', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- Attribute of Video model
INSERT INTO `onethink_attribute` VALUES ('530', 'appid', '小程序APPID', 'varchar(50) NOT NULL ', 'string', '', '小程序的APPID（在小程序后台查看）', '1', '', '55', '0', '1', '1427935908', '1427935908', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('531', 'name', '小程序名称', 'varchar(50) NOT NULL ', 'string', '', '小程序的名称', '1', '', '55', '0', '1', '1427935908', '1427935908', '', '0', '', '', '', '0', '');

-- Attribute of Video model
INSERT INTO `onethink_attribute` VALUES ('560', 'content', '视频详情', 'text NOT NULL', 'editor', '', '视频的详细介绍', '1', '', '56', '1', '1', '1427935908', '1427935908', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `onethink_attribute` VALUES ('561', 'video_url', '视频地址', 'varchar(255) NOT NULL', 'string', '', '视频所在网页的地址(优酷等)', '1', '', '56', '1', '1', '1428307615', '1428307615', '', '3', '', 'regex', '', '3', 'function');
INSERT INTO `onethink_attribute` VALUES ('562', 'author', '视频作者', 'varchar(50) NOT NULL ', 'string', 'Tchat', '视频作者', '1', '', '56', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- Attribute of Friendship model
INSERT INTO `onethink_attribute` VALUES ('580', 'pic_id', '图标', 'int(10) unsigned NOT NULL ', 'picture', '0', '链接的图片', '1', '', '70', '0', '1', '1384147827', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('581', 'title', '标题', 'varchar(20) NOT NULL', 'string', '', '链接标题', '1', '', '70', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('582', 'url', '链接地址', 'varchar(255) NOT NULL', 'string', '', '链接的网址', '1', '', '70', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('583', 'description', '链接描述', 'varchar(255) NOT NULL', 'textarea', '', '简单描述', '1', '', '70', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('584', 'group', '分组', 'varchar(255) NOT NULL', 'string', 'default', '设置分组', '1', '', '70', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('585', 'type', '类型', 'tinyint(2) NOT NULL ', 'select', '2', '链接类型', '1', '1:文字\r\n2:图标', '70', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('586', 'status', '状态', 'tinyint(2) NOT NULL ', 'select', '1', '是否启用', '1', '0:停用\r\n1:启用', '70', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('587', 'sort', '排序', 'int(10) NOT NULL ', 'string', '0', '排序（同一分组有效）', '1', '', '70', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');

-- -----------------------------
-- Records of  `onethink_model`
-- 升级后的model数据表在第9列新增”attribute_alias“列
-- -----------------------------
INSERT INTO `onethink_model`  VALUES ('5', 'group', '关键词分组', '0', '', '1', '{\"1\":[\"202\",\"213\",\"200\",\"205\",\"206\",\"207\",\"208\",\"209\",\"210\",\"211\",\"212\"]}', '1:基础', '', '', '', '','', 'name:关键词组\r\nsegment:所属板块\r\nreply_type:回复类型\r\nstatus:状态', '10', '', '', '1394597354', '1394602636', '1', 'MyISAM');

INSERT INTO `onethink_model`  VALUES ('50', 'tchat_events', '微信事件', '0', '', '1', '{\"1\":[\"400\",\"401\"]}', '1:基础', '', '', '', '','', 'id:编号\r\nevent_name:事件名称\r\nflow_id:回复模版号\r\naction_data:所需数据', '10', '', '', '1407334493', '1407334851', '1', 'MyISAM');
INSERT INTO `onethink_model`  VALUES ('51', 'tchat_qrcode', '微信二维码', '0', '', '1', '{\"1\":[\"410\",\"411\",\"412\",\"413\",\"414\"]}', '1:基础', '', '', '', '','', 'id:二维码编号\r\nflow_id:回复模版号\r\naction_data:所需数据\r\nstatus:状态', '10', '', '', '1407334493', '1407334851', '1', 'MyISAM');
INSERT INTO `onethink_model`  VALUES ('52', 'tchat_menu', '自定义菜单', '0', '', '1', '{\"1\":[\"462\",\"457\",\"450\",\"452\",\"451\",\"454\",\"455\",\"453\",\"463\",\"461\",\"456\"]}', '1:基础', '', '', '', '','', 'menuid:所属菜单类型编号\r\nid:条目编号\r\nsort:排序\r\nname:显示名称\r\nevent_key:系统识别码\r\nmenu_type:菜单类型\r\nappid:跳转的小程序\r\npage_path:小程序路径\r\nurl:跳转链接\r\nflow_id:回复模版号\r\naction_data:所需数据\r\nstatus:状态\r\nuid:用户\r\n', '10', '', '', '1407334493', '1407334851', '1', 'MyISAM');
INSERT INTO `onethink_model`  VALUES ('53', 'tchat_menu_list', '个性化菜单', '0', '', '1', '{\"1\":[\"471\",\"472\",\"473\",\"474\",\"475\",\"476\",\"477\",\"478\",\"479\"]}', '1:基础', '', '', '', '','', 'menuid:编号\r\nname:标识\r\ntitle:显示名称\r\ntag_id:适用标签编号\r\nsex:适用性别\r\ncountry:适用国家\r\nprovince:适用省份\r\ncity:适用城市\r\nclient_platform_type:适用操作系统\r\nlanguage:适用语言\r\n', '10', '', '', '1407334493', '1407334851', '1', 'MyISAM');
INSERT INTO `onethink_model`  VALUES ('54', 'tchat_reply_flow', '微信回复流程', '0', '', '1', '{\"1\":[\"504\",\"500\",\"501\",\"502\",\"503\",\"505\",\"506\",\"507\",\"508\",\"509\",\"510\",\"511\",\"512\"]}', '1:基础', '', '', '', '','', 'id:编号\r\ntitle:回复流名称\r\nmodule:处理模块\r\nlayer:控制器目录\r\ncontroller:控制器名\r\naction_name:方法名\r\nneed_data:是否需要数据\r\ndata_type:数据类型\r\ncreate_time:创建时间\r\n', '10', '', '', '1407334493', '1407334851', '1', 'MyISAM');
INSERT INTO `onethink_model`  VALUES ('55', 'tchat_miniprogram', '绑定小程序', '0', '', '1', '{\"1\":[\"530\",\"531\"]}', '1:基础', '', '', '', '','', 'id:编号\r\nappid:小程序APPID\r\nname:小程序名称\r\ncreate_time:创建时间\r\n', '10', '', '', '1407334493', '1407334851', '1', 'MyISAM');

INSERT INTO `onethink_model`  VALUES ('56', 'video', '视频', '1', '', '1', '{\"1\":[\"3\",\"561\",\"562\",\"560\",\"2\",\"191\",\"5\",\"9\",\"16\",\"19\"],\"2\":[\"10\",\"12\",\"190\",\"13\",\"11\",\"20\",\"14\",\"17\"]}', '1:基础,2:高级', '', '', '', '','', 'id:编号\r\ntitle:标题\r\nvideo_url:视频网址\r\ncreate_time:创建时间\r\nview:浏览次数', '10', '', '', '1427935705', '1428307651', '1', 'MyISAM');

INSERT INTO `onethink_model`  VALUES ('61', 'tchat_album', '相册', '0', '', '1', '{\"1\":[\"424\",\"420\",\"421\",\"422\",\"423\",\"429\",\"430\",\"436\"],\"2\":[\"425\",\"426\",\"427\",\"432\",\"428\",\"433\",\"434\",\"435\",\"437\"]}', '1:基础,2:高级', '', '', '', '','', 'name:相册标识\r\ntitle:相册标题\r\ndescription:相册描述\r\ncontent:相册详细内容\r\nact_type:所属分类ID\r\nallow_publish:是否允许投稿\r\nreply:是否允许回复\r\ncheck:发布是否需要审核\r\nmeta_title:SEO的网页标题\r\nkeywords:关键字\r\nlink_id:外链\r\ndisplay:是否私有\r\nmax_limit:上传数量限制\r\nvote:投票设置', '10', '', '', '1396970451', '1396970451', '1', 'MyISAM');

INSERT INTO `onethink_model`  VALUES ('70', 'friendship', '友情链接', '0', '', '1', '{\"1\":[\"581\",\"582\",\"583\",\"584\",\"585\",\"580\",\"586\",\"587\"]}', '1:基础', '', '', '', '','', 'title:链接标题\r\nurl:链接URL\r\ndescription:链接描述\r\ntype:链接类型\r\npic_id:链接图标\r\ngroup:链接分组\r\nstatus:状态', '10', '', '', '1396970451', '1396970451', '1', 'MyISAM');

-- -----------------------------
-- 新建视频文档模型扩展表 `onethink_document_video`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_document_video`;
CREATE  TABLE `onethink_document_video` (
  `id` int(10) unsigned NOT NULL COMMENT '视频文档ID',
  `content` text NOT NULL COMMENT '视频详情',
  `video_url` varchar(255) NOT NULL COMMENT '视频地址',
	`author` varchar(50) NOT NULL DEFAULT 'Tchat' COMMENT '视频作者',
PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '视频文档模型表';

-- -----------------------------
-- Records of  `onethink_auth_extend`
-- -----------------------------

INSERT INTO `onethink_auth_extend` VALUES ('2','1','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','2','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','11','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','12','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','13','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','14','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','21','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','22','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','23','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','24','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','31','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','32','1');
INSERT INTO `onethink_auth_extend` VALUES ('2','33','1');

INSERT INTO `onethink_auth_extend` VALUES ('4','1','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','2','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','11','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','12','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','13','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','14','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','21','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','22','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','23','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','24','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','31','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','32','1');
INSERT INTO `onethink_auth_extend` VALUES ('4','33','1');

INSERT INTO `onethink_auth_extend` VALUES ('5','21','1');
INSERT INTO `onethink_auth_extend` VALUES ('5','23','1');

-- -----------------------------
-- Records of `onethink_auth_group`
-- -----------------------------
INSERT INTO `onethink_auth_group` VALUES ('3', 'admin', '1', '领导组', '用于领导人员的帐号组', '1', '1,2,3,17,23,24,26,27,107,108,109,110,195,204,211,212,214,215,216,258,259,260,261,262,263,300,301,302,303,304,305,306,310,311,312,313,314,315,320,321,322,323,324,325,330,331,332,333,334,335,336,350,351,352,353,354,355,356,357,358,359,360,361,362,366,367,368,369,370,371,372,373,374,375,380,381,382,383,390,391,392,393,394,395,396,397,398,399,1660,1661,1662');
INSERT INTO `onethink_auth_group` VALUES ('4', 'admin', '1', '行政组', '用于行政人员的帐号组', '1', '1,2,7,8,9,10,11,12,13,14,15,16,17,18,53,79,107,108,109,110,211,258,260,261,262,263,1660,1661,1662');
INSERT INTO `onethink_auth_group` VALUES ('5', 'admin', '1', '执行组', '用于执行人员的帐号组', '1', '1,2,3,7,8,9,10,11,12,13,14,15,16,17,18,53,79,108,109,211,258,260,261,262,263');
INSERT INTO `onethink_auth_group` VALUES ('6', 'admin', '1', '终端组', '用于面向客户的工作人员的帐号组', '1', '1,3,26,107,108,109,110,260,261,262,263,1660,1661,1662');

-- -----------------------------
-- Records of `onethink_auth_group_access`
-- -----------------------------

INSERT INTO `onethink_auth_group_access` VALUES ('2','2');

-- -----------------------------
-- Records of `onethink_auth_rule`
-- -----------------------------
-- 纠正原OneThink中没有给这两个菜单给出权限ID
INSERT INTO `onethink_auth_rule` VALUES ('258', 'admin', '1', 'Admin/article/index', '文档列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('259', 'admin', '1', 'Admin/think/lists', '数据列表', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('260', 'admin', '1', 'Admin/Member/myInfo', '用户资料列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('261', 'admin', '1', 'Admin/Member/updateInfo', '编辑资料', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('262', 'admin', '1', 'Admin/Member/submitInfo', '提交编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('263', 'admin', '1', 'Admin/Member/bind', '轮询绑定状态', '1', '');
-- 微信板块权限设置

INSERT INTO `onethink_auth_rule` VALUES ('300', 'admin', '2', 'Admin/Wechat/index', '微信', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('301', 'admin', '1', 'Admin/Wechat/index', '微信首页', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('302', 'admin', '1', 'Admin/WechatKeyword/index','关键词列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('303', 'admin', '1', 'Admin/WechatKeyword/create','新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('304', 'admin', '1', 'Admin/WechatKeyword/edit','编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('305', 'admin', '1', 'Admin/WechatKeyword/update','保存', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('306', 'admin', '1', 'Admin/WechatKeyword/setStatus','改变状态', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('310', 'admin', '1', 'Admin/WechatKeyword/analytical','关键词分析', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('311', 'admin', '1', 'Admin/WechatKeyword/deadList','过期箱', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('312', 'admin', '1', 'Admin/WechatKeyword/disabled','禁用箱', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('313', 'admin', '1', 'Admin/WechatKeyword/recycle','回收站', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('314', 'admin', '1', 'Admin/WechatKeyword/restore','还原', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('315', 'admin', '1', 'Admin/WechatKeyword/clear','彻底删除', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('320', 'admin', '1', 'Admin/WechatClient/index','粉丝列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('321', 'admin', '1', 'Admin/WechatClient/detail', '详情','1', '');
INSERT INTO `onethink_auth_rule` VALUES ('322', 'admin', '1', 'Admin/WechatClient/tags','标签管理', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('323', 'admin', '1', 'Admin/WechatClient/setClientInfo','设置粉丝属性', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('324', 'admin', '1', 'Admin/WechatClient/importClient','同步粉丝列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('325', 'admin', '1', 'Admin/WechatClient/getProgress','获取同步进度', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('330', 'admin', '1', 'Admin/WechatMessage/index','消息列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('331', 'admin', '1', 'Admin/WechatMessage/setStar','星标消息', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('332', 'admin', '1', 'Admin/WechatMessage/archive','存档消息', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('333', 'admin', '1', 'Admin/WechatMessage/analytical','消息分析', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('334', 'admin', '1', 'Admin/WechatMessage/handle','处理消息', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('335', 'admin', '1', 'Admin/WechatMessage/delete','删除消息', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('336', 'admin', '1', 'Admin/WechatMessage/reply','回复消息', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('350', 'admin', '1', 'Admin/WechatMenu/viewMenu','查看菜单', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('351', 'admin', '1', 'Admin/WechatMenu/add','新增菜单细项', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('352', 'admin', '1', 'Admin/WechatMenu/edit','编辑菜单细项', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('353', 'admin', '1', 'Admin/WechatMenu/update','更新菜单细项', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('354', 'admin', '1', 'Admin/WechatMenu/setStatus','改变状态', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('355', 'admin', '1', 'Admin/WechatMenu/remove','删除菜单细项', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('356', 'admin', '1', 'Admin/WechatMenu/setMenu','生成至微信公众号', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('357', 'admin', '1', 'Admin/WechatMenu/menuList','查看个性菜单', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('358', 'admin', '1', 'Admin/WechatMenu/addMenuList','新增个性菜单', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('359', 'admin', '1', 'Admin/WechatMenu/editMenuList','编辑个性菜单', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('360', 'admin', '1', 'Admin/WechatMenu/updateMenuList','更新个性菜单', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('361', 'admin', '1', 'Admin/WechatMenu/delMenuList','删除个性菜单', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('362', 'admin', '1', 'Admin/WechatMenu/publishMenuList','发布个性菜单', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('366', 'admin', '1', 'Admin/WechatEvent/index','事件列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('367', 'admin', '1', 'Admin/WechatEvent/edit','编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('368', 'admin', '1', 'Admin/WechatEvent/update','更新', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('369', 'admin', '1', 'Admin/WechatEvent/setStatus','改变状态', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('370', 'admin', '1', 'Admin/WechatQrcode/index','二维码列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('371', 'admin', '1', 'Admin/WechatQrcode/create','新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('372', 'admin', '1', 'Admin/WechatQrcode/setStatus','改变状态', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('373', 'admin', '1', 'Admin/WechatQrcode/getTicket','获得Ticket', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('374', 'admin', '1', 'Admin/WechatQrcode/showQrcode','查看二维码', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('375', 'admin', '1', 'Admin/WechatQrcode/update','查看二维码', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('380', 'admin', '1', 'Admin/WechatText/index','文本列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('381', 'admin', '1', 'Admin/WechatText/create','新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('382', 'admin', '1', 'Admin/WechatText/edit','编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('383', 'admin', '1', 'Admin/WechatText/remove','删除', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('390', 'admin', '1', 'Admin/WechatMass/createMassMessage','创建群发消息','1','');
INSERT INTO `onethink_auth_rule` VALUES ('391', 'admin', '1', 'Admin/WechatMass/sendMassMessage','触发发送','1','');
INSERT INTO `onethink_auth_rule` VALUES ('392', 'admin', '1', 'Admin/WechatMass/previewMassMessage','预览群发','1','');
INSERT INTO `onethink_auth_rule` VALUES ('393', 'admin', '1', 'Admin/WechatMass/listMassMessage','查看群发消息','1','');
INSERT INTO `onethink_auth_rule` VALUES ('394', 'admin', '1', 'Admin/WechatMass/setStatus','更改状态','1','');
INSERT INTO `onethink_auth_rule` VALUES ('395', 'admin', '1', 'Admin/WechatMass/renewSendStatus','刷新状态','1','');
INSERT INTO `onethink_auth_rule` VALUES ('396', 'admin', '1', 'Admin/WechatMass/draftBox','草稿箱','1','');
INSERT INTO `onethink_auth_rule` VALUES ('397', 'admin', '1', 'Admin/WechatMass/editMassMessage','编辑','1','');
INSERT INTO `onethink_auth_rule` VALUES ('398', 'admin', '1', 'Admin/WechatMass/recycle','回收站','1','');
INSERT INTO `onethink_auth_rule` VALUES ('399', 'admin', '1', 'Admin/WechatMass/deleteMessage','彻底删除','1','');

INSERT INTO `onethink_auth_rule` VALUES ('420', 'admin', '1', 'Admin/WechatMaterial/index','素材管理首页','1','');

INSERT INTO `onethink_auth_rule` VALUES ('421', 'admin', '1', 'Admin/WechatNews/index','查看图文素材列表','1','');
INSERT INTO `onethink_auth_rule` VALUES ('422', 'admin', '1', 'Admin/WechatNews/addNews','新增图文素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('423', 'admin', '1', 'Admin/WechatNews/editNews','编辑图文素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('424', 'admin', '1', 'Admin/WechatNews/findDocument','查找文章条目内容','1','');
INSERT INTO `onethink_auth_rule` VALUES ('425', 'admin', '1', 'Admin/WechatNews/updateNews','更新图文素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('426', 'admin', '1', 'Admin/WechatNews/deleteNews','删除图文素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('427', 'admin', '1', 'Admin/WechatNews/uploadNews','上传本地图文素材到微信','1','');
INSERT INTO `onethink_auth_rule` VALUES ('428', 'admin', '1', 'Admin/WechatNews/downloadNews','下载微信图文素材到本地','1','');
INSERT INTO `onethink_auth_rule` VALUES ('429', 'admin', '1', 'Admin/WechatNews/addTchatArticle','新增本地素材文章','1','');

INSERT INTO `onethink_auth_rule` VALUES ('440', 'admin', '1', 'Admin/WechatPicture/index','查看图片素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('441', 'admin', '1', 'Admin/WechatPicture/addPicture','查看图片素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('442', 'admin', '1', 'Admin/WechatPicture/deletePicture','删除图片素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('443', 'admin', '1', 'Admin/WechatPicture/downloadPicture','从微信下载图片素材','1','');

INSERT INTO `onethink_auth_rule` VALUES ('460', 'admin', '1', 'Admin/WechatVoice/index','查看语音素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('461', 'admin', '1', 'Admin/WechatVoice/addVoice','查看语音素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('462', 'admin', '1', 'Admin/WechatVoice/deleteVoice','删除语音素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('463', 'admin', '1', 'Admin/WechatVoice/downloadVoice','从微信下载语音素材','1','');

INSERT INTO `onethink_auth_rule` VALUES ('480', 'admin', '1', 'Admin/WechatVideo/index','查看视频素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('481', 'admin', '1', 'Admin/WechatVideo/addVideo','新增视频素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('482', 'admin', '1', 'Admin/WechatVideo/deleteVideo','删除视频素材','1','');
INSERT INTO `onethink_auth_rule` VALUES ('483', 'admin', '1', 'Admin/WechatVideo/downloadVideo','从微信下载视频素材','1','');

INSERT INTO `onethink_auth_rule` VALUES ('500', 'admin', '1', 'Admin/WechatMaterial/online','查看线上素材','1','');

INSERT INTO `onethink_auth_rule` VALUES ('520', 'admin', '1', 'Admin/WechatMiniProgram/index','查看绑定小程序列表','1','');
INSERT INTO `onethink_auth_rule` VALUES ('521', 'admin', '1', 'Admin/WechatMiniProgram/create','新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('522', 'admin', '1', 'Admin/WechatMiniProgram/edit','编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('523', 'admin', '1', 'Admin/WechatMiniProgram/remove','删除', '1', '');

-- 相册板块权限设置
INSERT INTO `onethink_auth_rule` VALUES ('550', 'admin', '2', 'Admin/Album/index','相册', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('551', 'admin', '1', 'Admin/Album/index','相册列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('552', 'admin', '1', 'Admin/Album/detail','相册详情', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('553', 'admin', '1', 'Admin/Album/create','新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('554', 'admin', '1', 'Admin/Album/edit','编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('555', 'admin', '1', 'Admin/Album/update','更新', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('556', 'admin', '1', 'Admin/Album/setStatus','改变状态', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('557', 'admin', '1', 'Admin/Album/remove','删除', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('558', 'admin', '1', 'Admin/Album/deletePic','删除图片', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('559', 'admin', '1', 'Admin/Album/movePic','移动图片', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('560', 'admin', '1', 'Admin/Album/category','相册分类', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('561', 'admin', '1', 'Admin/Album/addCategory','新增', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('562', 'admin', '1', 'Admin/Album/editCategory','编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('563', 'admin', '1', 'Admin/Album/updateCategory','更新', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('564', 'admin', '1', 'Admin/Album/deleteCategory','删除', '1', '');


-- 前台控制板块权限设置

INSERT INTO `onethink_auth_rule` VALUES ('1600', 'admin', '2', 'Admin/Home/index', '前台', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1601', 'admin', '1', 'Admin/Home/index', '前台配置', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('1602', 'admin', '1', 'Admin/HomeControl/logo', 'LOGO配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1603', 'admin', '1', 'Admin/HomeControl/saveLogo', '保存LOGO', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1604', 'admin', '1', 'Admin/HomeControl/slide', '幻灯片配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1605', 'admin', '1', 'Admin/HomeControl/saveSlide', '保存幻灯片', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1606', 'admin', '1', 'Admin/HomeControl/article?part=BeforeArticle', '文前配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1607', 'admin', '1', 'Admin/HomeControl/article?part=AfterArticle', '文尾配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1608', 'admin', '1', 'Admin/HomeControl/saveArticle', '保存文前文尾配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1609', 'admin', '1', 'Admin/HomeControl/feature', '特色内容配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1610', 'admin', '1', 'Admin/HomeControl/saveFeature', '保存特色内容', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1611', 'admin', '1', 'Admin/HomeControl/theme', '前台主题模板配置', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1612', 'admin', '1', 'Admin/HomeControl/clear', '清除配置', '1', '');

INSERT INTO `onethink_auth_rule` VALUES ('1620', 'admin', '1', 'Admin/Friendship/index', '友情链接列表', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1621', 'admin', '1', 'Admin/Friendship/create', '添加', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1622', 'admin', '1', 'Admin/Friendship/edit', '编辑', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1623', 'admin', '1', 'Admin/Friendship/update', '更新', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1624', 'admin', '1', 'Admin/Friendship/setStatus', '设置状态', '1', '');

-- 系统升级权限设置
INSERT INTO `onethink_auth_rule` VALUES ('1650', 'admin', '1', 'Admin/OnechatUpdate/index', '查看版本', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1651', 'admin', '1', 'Admin/Update/index', '升级系统', '1', '');

-- 邮件管理权限设置
INSERT INTO `onethink_auth_rule` VALUES ('1652', 'admin', '1', 'Admin/Mail/index', '邮件模版', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1653', 'admin', '1', 'Admin/Mail/info', '预览邮件模版', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1654', 'admin', '1', 'Admin/Mail/create', '新建邮件模版', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1655', 'admin', '1', 'Admin/Mail/edit', '编辑邮件模版', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1656', 'admin', '1', 'Admin/Mail/update', '更新邮件模版', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1657', 'admin', '1', 'Admin/Mail/setStatus', '邮件状态', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1658', 'admin', '1', 'Admin/Mail/mail', '发送邮件', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1659', 'admin', '1', 'Admin/Config/group?id=6', '配置邮件', '1', '');

-- 查看列表权限设置
INSERT INTO `onethink_auth_rule` VALUES ('1660', 'admin', '1', 'Admin/Wechat/contentGuide?segment=category', '查看内容引导', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1661', 'admin', '1', 'Admin/Wechat/contentGuide?segment=text', '查看内容引导', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1662', 'admin', '1', 'Admin/Wechat/contentGuide?segment=article', '查看内容引导', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1663', 'admin', '1', 'Admin/Wechat/contentGuide?segment=albumCategory', '查看内容引导', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1664', 'admin', '1', 'Admin/Wechat/contentGuide?segment=album', '查看内容引导', '1', '');

-- -----------------------------
-- Records of `onethink_category`
-- 默认设置的目录
-- 升级后的分类表在第十五字段上增加了一个"model_sub"字段，用以设置子文档绑定的模型
-- 在最后增加了“groups”分组定义字段
-- -----------------------------
INSERT INTO `onethink_category` VALUES ('11', 'about', '关于', '0', '0', '10', '', '', '', '', '', '', '', '2','2', '2,1', '0', '0', '1', '0', '0', '1', '', '1379474947', '1382701539', '1', '0','','');
INSERT INTO `onethink_category` VALUES ('12', 'about_info', '公司介绍', '11', '1', '10', '', '', '', '', '', '', '', '2','2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '0','','');
INSERT INTO `onethink_category` VALUES ('13', 'about_business', '业务范围', '11', '2', '10', '', '', '', '', '', '', '', '2','2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '0','','');
INSERT INTO `onethink_category` VALUES ('14', 'culture', '企业文化', '11', '3', '10', '', '', '', '', '', '', '', '2','2','2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '0','','');

INSERT INTO `onethink_category` VALUES ('21', 'news', '新闻',  '0', '0', '10', '', '', '', '', '', '', '', '2', '2','2,1', '0', '0', '1', '0', '0', '1', '', '1379474947', '1382701539', '1', '0','','');
INSERT INTO `onethink_category` VALUES ('22', 'company_news', '公司新闻', '21', '1', '10', '', '', '', '', '', '', '', '2','2','2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '0','','');
INSERT INTO `onethink_category` VALUES ('23', 'activity_news', '活动优惠', '21', '2', '10', '', '', '', '', '', '', '', '2','2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '0','','');
INSERT INTO `onethink_category` VALUES ('24', 'industry_news', '行业动态', '21', '3', '10', '', '', '', '', '', '', '', '2','2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '31','','');

INSERT INTO `onethink_category` VALUES ('31', 'share', '分享',  '0', '0', '10', '', '', '', '', '', '', '', '2','2', '2,1', '0', '0', '1', '0', '0', '1', '', '1379474947', '1382701539', '1', '0','','');
INSERT INTO `onethink_category` VALUES ('32', 'share_know', '行业知识', '31', '1', '10', '', '', '', '', '', '', '', '2','2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '31','','');
INSERT INTO `onethink_category` VALUES ('33', 'share_story', '行业故事', '31', '2', '10', '', '', '', '', '', '', '', '2','2', '2,1,3', '0', '1', '1', '0', '1', '1', '', '1379475028', '1386839751', '1', '31','','');

-- -----------------------------
-- Records of `onethink_channel`
-- -----------------------------
DELETE FROM `onethink_channel` WHERE `id` = '2' limit 1;
DELETE FROM `onethink_channel` WHERE `id` = '3' limit 1;
INSERT INTO `onethink_channel` VALUES ('4', '0', '关于', 'Home/Article/index?category=about', '2', '1379475131', '1379483713', '1', '0','默认分组');
INSERT INTO `onethink_channel` VALUES ('5', '0', '新闻', 'Home/Article/index?category=news', '3', '1379475131', '1379483713', '1', '0','默认分组');
INSERT INTO `onethink_channel` VALUES ('6', '0', '分享', 'Home/Article/index?category=share', '4', '1379475131', '1379483713', '1', '0','默认分组');

-- -----------------------------
-- Records of `onethink_config`
-- -----------------------------
INSERT INTO `onethink_config` VALUES ('40', 'WECHAT_GHID', '1', '原始ID', '5', '', '公众帐号原始ID', '1378898976', '1379235841', '1', '', '1');
INSERT INTO `onethink_config` VALUES ('41', 'WECHAT_NAME', '1', '微信号', '5', '', '公众帐号官方账号', '1378898976', '1379235841', '1', '', '2');
INSERT INTO `onethink_config` VALUES ('42', 'WECHAT_NICKNAME', '1', '名称', '5', '', '公众帐号名称', '1378898976', '1379235841', '1', '', '0');
INSERT INTO `onethink_config` VALUES ('43', 'WECHAT_TOKEN', '1', '微信TOKEN', '5', '', '公众帐号的接入本地验证码,可自行设定，请确保微信后台开发者模式TOKEN填写与此保持一致', '1378898976', '1379235841', '1', 'Tchat', '5');
INSERT INTO `onethink_config` VALUES ('44', 'WECHAT_APP_ID', '1', 'APPID', '5', '', '公众帐号的APPID', '1378898976', '1379235841', '1', '', '6');
INSERT INTO `onethink_config` VALUES ('45', 'WECHAT_APP_SECRET', '1', 'APP_SECRET', '5', '', '公众帐号的APPSECRET', '1378898976', '1379235841', '1', '', '7');
INSERT INTO `onethink_config` VALUES ('46', 'WECHAT_ACCOUNT_TYPE', '4', '账号类型', '5', '0:订阅号\r\n1:服务号\r\n2:企业号', '公众帐号类型', '1378898976', '1379235841', '1', '0', '3');
INSERT INTO `onethink_config` VALUES ('47', 'WECHAT_ACCOUNT_RZ', '4', '认证状态', '5', '0:未认证\r\n1:已认证\r\n2:微博认证', '公众账号的认证状态', '1378898976', '1379235841', '1', '0', '4');
INSERT INTO `onethink_config` VALUES ('48', 'WECHAT_CUSTOM_SERVICE', '4', '多客服服务', '5', '0:未开通\r\n1:已开通', '公众帐号后台控制，只有已认证的服务号才可开通', '1378898976', '1379235841', '1', '0', '8');
INSERT INTO `onethink_config` VALUES ('49', 'WECHAT_EncodingAESKey', '1', '消息加密EncodingAESKey', '5', '', '加密消息EncodingAESKey', '1378898976', '1379235841', '1', '', '8');

INSERT INTO `onethink_config` VALUES ('60', 'COMPANY_TELEPHONE', '1', '联系电话', '1', '', '公司联系电话', '1378898976', '1379235841', '1', '', '20');
INSERT INTO `onethink_config` VALUES ('61', 'COMPANY_ADDRESS', '1', '地址', '1', '', '公司地址', '1378898976', '1379235841', '1', '', '21');
INSERT INTO `onethink_config` VALUES ('62', 'COMPANY_NAME', '1', '名称', '1', '', '公司名称', '1378898976', '1379235841', '1', '', '22');
INSERT INTO `onethink_config` VALUES ('63', 'COPYRIGHT_ANNOUNCE', '2', '版权声明', '1', '', '版权声明', '1378898976', '1379235841', '1', '', '23');
INSERT INTO `onethink_config` VALUES ('64', 'SERVICE_QQ', '3', '客服QQ', '1', '', '客服QQ号码，每行一个', '1378898976', '1379235841', '1', '', '24');
INSERT INTO `onethink_config` VALUES ('65', 'WEBSITE_TYPE', '4', '网站类型', '1', '0:电脑版(含响应式布局)\r\n1:手机版\r\n2:自动切换(不同模版)', '设置网站对外的类型', '1378898976', '1379235841', '1', '', '25');
INSERT INTO `onethink_config` VALUES ('66', 'WEBSITE_SEO_TITLE', '2', 'SEO站点标题', '1', '', '用于搜索引擎的SEO标题设置', '1378898976', '1379235841', '1', '', '1');

INSERT INTO `onethink_config` VALUES ('80', 'MAIL_STATUS', '4', '邮件功能开关', '6', '0:关闭邮件功能\r\n1:全部开启\r\n2:仅限通知邮件\r\n3:仅限自定义邮件', '设置邮件功能的使用范围', '1378898976', '1379235841', '1', '1', '0');
INSERT INTO `onethink_config` VALUES ('81', 'MAIL_SMTP_HOST', '1', 'SMTP邮件发送服务器', '6', '', '设置SMTP发送邮件的服务器', '1378898976', '1379235841', '1', 'smtp.exmail.qq.com', '1');
INSERT INTO `onethink_config` VALUES ('82', 'MAIL_SMTP_PORT', '1', 'SMTP邮件发送端口号', '6', '', '设置SMTP发送邮件的端口号', '1378898976', '1379235841', '1', '465', '2');
INSERT INTO `onethink_config` VALUES ('83', 'MAIL_SMTP_USERNAME', '1', 'SMTP登录用户名', '6', '', '用于登录服务器的帐号', '1378898976', '1379235841', '1', 'noreply@guilins.com', '3');
INSERT INTO `onethink_config` VALUES ('84', 'MAIL_SMTP_PASSWORD', '1', 'SMTP登录密码', '6', '', '用于登录服务器的密码', '1378898976', '1379235841', '1', 'vEWChNUYYmMCD2xx', '4');
INSERT INTO `onethink_config` VALUES ('85', 'MAIL_FROM_NAME', '1', '发件人姓名', '6', '', '设置对外发送邮件显示的名称', '1378898976', '1379235841', '1', 'OneChat', '5');
INSERT INTO `onethink_config` VALUES ('86', 'MAIL_FROM_ADDRESS', '1', '发件人邮箱', '6', '', '设置对外发送邮件显示及用于回复的邮箱帐号', '1378898976', '1379235841', '1', 'noreply@guilins.com', '6');

-- -----------------------------
-- Records of `onethink_menu`
-- 升级后的目录表在最后增加了一个”status“状态字段
-- -----------------------------

INSERT INTO `onethink_menu` VALUES ('150', '我的资料', '16', '0', 'Admin/Member/myInfo', '0', '用户个人资料预览', '用户资料详情', '0','1');
INSERT INTO `onethink_menu` VALUES ('151', '编辑账户信息', '16', '0', 'Admin/Member/updateInfo', '0', '编辑个人资料', '用户资料详情', '0','1');
INSERT INTO `onethink_menu` VALUES ('152', '提交', '151', '0', 'Admin/Member/submitInfo', '0', '提交个人资料', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('153', '修改账户密码', '16', '0', 'Admin/User/updatePassword', '0', '编辑个人密码', '用户资料详情', '0','1');
INSERT INTO `onethink_menu` VALUES ('154', '轮询绑定状态', '150', '0', 'Admin/Member/bind', '0', '轮询绑定状态', '', '0','1');
-- 新增微信板块管理目录

INSERT INTO `onethink_menu` VALUES ('300', '微信', '0', '2', 'Admin/Wechat/index', '0', '微信后台模块管理目录', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('301', '微信管理首页', '300', '1', 'Admin/Wechat/index', '1', '微信后台模块管理概况', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('306', '获取分类', '301', '0', 'Admin/Wechat/contentGuide?segment=category', '0', '获取分类列表', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('307', '获取文本', '301', '1', 'Admin/Wechat/contentGuide?segment=text', '0', '获取文本列表', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('308', '获取文章', '301', '2', 'Admin/Wechat/contentGuide?segment=article', '0', '获取文章列表', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('309', '获取相册分类', '301', '2', 'Admin/Wechat/contentGuide?segment=albumCategory', '0', '获取相册分类列表', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('310', '获取相册列表', '301', '2', 'Admin/Wechat/contentGuide?segment=album', '0', '获取相册列表', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('321', '关键词列表', '300', '0', 'Admin/WechatKeyword/index', '0', '查看关键词组的列表', '关键词管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('322', '新增', '321', '1', 'Admin/WechatKeyword/create', '0', '新增一组关键词组', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('323', '编辑', '321', '2', 'Admin/WechatKeyword/edit', '0', '编辑一组关键词组', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('324', '改变状态', '321', '4', 'Admin/WechatKeyword/setStatus', '0', '更改当前关键词组的状态', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('325', '关键词分析', '300', '1', 'Admin/WechatKeyword/analytical', '0', '查看关键词效果分析', '关键词管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('326', '过期箱', '325', '0', 'Admin/WechatKeyword/deadList', '0', '查看已经过期的关键词组', '关键词管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('327', '禁用箱', '325', '1', 'Admin/WechatKeyword/disabled', '0', '查看禁用的关键词组', '关键词管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('328', '回收站', '325', '2', 'Admin/WechatKeyword/recycle', '0', '查看已经删除的关键词组', '关键词管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('329', '还原', '325', '3', 'Admin/WechatKeyword/restore', '0', '还原一组关键词组', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('330', '彻底删除', '325', '4', 'Admin/WechatKeyword/clear', '0', '彻底删除一组关键词组', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('331', '保存', '321', '3', 'Admin/WechatKeyword/update', '0', '更新关键词', '', '0','1');

-- 原本活动归到微信下的，但出于管理方便，另行开设顶级菜单，占了此处340-360的ID

INSERT INTO `onethink_menu` VALUES ('361', '粉丝列表', '300', '2', 'Admin/WechatClient/index', '0', '查看粉丝列表', '粉丝管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('362', '详情', '361', '0', 'Admin/WechatClient/detail', '0', '查看粉丝详细信息', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('363', '设置', '361', '1', 'Admin/WechatClient/setClientInfo', '0', '设置粉丝属性（备注、标签等）', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('364', '同步', '361', '2', 'Admin/WechatClient/importClient', '0', '同步粉丝列表', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('365', '进度', '361', '3', 'Admin/WechatClient/getProgress', '0', '查看同步粉丝进度', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('366', '标签管理', '300', '3', 'Admin/WechatClient/tags', '0', '查看和管理微信粉丝标签', '粉丝管理', '0','1');

INSERT INTO `onethink_menu` VALUES ('371', '消息列表', '300', '4', 'Admin/WechatMessage/index', '1', '查看粉丝消息列表', '消息管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('372', '星标消息', '371', '0', 'Admin/WechatMessage/setStar', '1', '对粉丝消息进行星标操作', '消息管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('373', '存档消息', '371', '1', 'Admin/WechatMessage/archive', '0', '存档粉丝消息条目', '消息管理', '0','1');

INSERT INTO `onethink_menu` VALUES ('374', '消息分析', '300', '5', 'Admin/WechatMessage/analytical', '1', '查看粉丝消息的分析', '消息管理', '0','1');

INSERT INTO `onethink_menu` VALUES ('375', '处理消息', '300', '6', 'Admin/WechatMessage/handle', '1', '处理当前粉丝的消息', '消息管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('376', '删除消息', '375', '0', 'Admin/WechatMessage/delete', '0', '删除粉丝消息', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('377', '回复消息', '375', '1', 'Admin/WechatMessage/reply', '0', '回复客服消息', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('391', '默认菜单', '300', '7', 'Admin/WechatMenu/viewMenu', '0', '查看默认菜单(含个性化菜单，需传个性菜单ID参数)', '自定义菜单', '0','1');
INSERT INTO `onethink_menu` VALUES ('392', '新增', '391', '0', 'Admin/WechatMenu/add', '0', '新增一项菜单细项', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('393', '编辑', '391', '1', 'Admin/WechatMenu/edit', '0', '编辑一条菜单细项', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('394', '状态', '391', '2', 'Admin/WechatMenu/setStatus', '0', '改变一条菜单细项的启用状态', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('395', '删除', '391', '3', 'Admin/WechatMenu/remove', '0', '删除一条菜单细项', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('396', '更新', '391', '4', 'Admin/WechatMenu/update', '0', '更新一条菜单细项的设置', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('397', '生成', '391', '5', 'Admin/WechatMenu/setMenu', '0', '将本地菜单设置生成到微信公众平台', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('398', '个性化菜单', '300', '8', 'Admin/WechatMenu/menuList', '0', '查看个性化菜单列表', '自定义菜单', '0','1');
INSERT INTO `onethink_menu` VALUES ('399', '新增', '398', '0', 'Admin/WechatMenu/addMenuList', '0', '新增一组个性化菜单', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('400', '编辑', '398', '1', 'Admin/WechatMenu/editMenuList', '0', '编辑一组个性化菜单的设置', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('401', '更新', '398', '2', 'Admin/WechatMenu/updateMenuList', '0', '更新一组个性化菜单的设置', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('402', '删除', '398', '3', 'Admin/WechatMenu/delMenuList', '0', '删除一组个性化菜单', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('403', '发布', '398', '4', 'Admin/WechatMenu/publishMenuList', '0', '发布一组个性化菜单到微信公众号', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('410','文本素材','300','9','Admin/WechatText/index','0','查看文本数据列表','素材管理','0','1');
INSERT INTO `onethink_menu` VALUES ('411','编辑','410','0','Admin/WechatText/edit','0','编辑文本内容','','0','1');
INSERT INTO `onethink_menu` VALUES ('412','删除','410','1','Admin/WechatText/remove','0','删除文本内容','','0','1');
INSERT INTO `onethink_menu` VALUES ('413','新增','410','2','Admin/WechatText/create','0','新建文本内容','','0','1');

INSERT INTO `onethink_menu` VALUES ('421', '二维码列表', '300', '10', 'Admin/WechatQrcode/index', '0', '所有场景值二维码的列表', '二维码管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('422', '新增二维码', '300', '11', 'Admin/WechatQrcode/create', '0', '新增一个场景值二维码', '二维码管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('423', '获取TICKET', '422', '0', 'Admin/WechatQrcode/getTicket', '0', '获取公众账号二维码的TICKET', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('424', '查看二维码', '421', '0', 'Admin/WechatQrcode/showQrcode', '0', '查看二维码详情', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('425', '改变状态', '421', '1', 'Admin/WechatQrcode/setStatus', '0', '更改二维码的启用状态', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('426', '更新', '422', '1', 'Admin/WechatQrcode/update', '0', '更新二维码', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('430','事件列表','300','12','Admin/WechatEvent/index','0','查看事件回复设置状态','事件设置','0','1');
INSERT INTO `onethink_menu` VALUES ('431','编辑','430','0','Admin/WechatEvent/edit','0','设置某一事件回复内容','','0','1');
INSERT INTO `onethink_menu` VALUES ('432','更新','430','1','Admin/WechatEvent/update','0','更新事件设置内容','','0','1');
INSERT INTO `onethink_menu` VALUES ('433','更改状态','430','2','Admin/WechatEvent/setStatus','0','更改事件状态','','0','1');

INSERT INTO `onethink_menu` VALUES ('440','创建群发消息','300','13','Admin/WechatMass/createMassMessage','1','创建一条新的群发消息','群发管理','0','1');
INSERT INTO `onethink_menu` VALUES ('441','触发发送','440','0','Admin/WechatMass/sendMassMessage','0','触发一条消息的发送动作','','0','1');
INSERT INTO `onethink_menu` VALUES ('442','预览消息','440','1','Admin/WechatMass/previewMassMessage','0','预览一条编辑后的群发消息','','0','1');
INSERT INTO `onethink_menu` VALUES ('443','查看群发消息','300','14','Admin/WechatMass/listMassMessage','1','查看当前所有的群发消息','群发管理','0','1');
INSERT INTO `onethink_menu` VALUES ('444','更改状态','443','0','Admin/WechatMass/setStatus','0','更改一条上传了的群发消息的发送状态','','0','1');
INSERT INTO `onethink_menu` VALUES ('445','刷新状态','443','1','Admin/WechatMass/renewSendStatus','0','刷新一条上传了的群发消息的发送状态','','0','1');
INSERT INTO `onethink_menu` VALUES ('446','草稿箱','300','15','Admin/WechatMass/draftBox','1','查看草稿箱里的群发消息','群发管理','0','1');
INSERT INTO `onethink_menu` VALUES ('447','编辑','443','0','Admin/WechatMass/editMassMessage','0','编辑一条草稿箱中的群发消息','','0','1');
INSERT INTO `onethink_menu` VALUES ('448','回收站','300','16','Admin/WechatMass/recycle','1','查看已在本地删除的群发消息','群发管理','0','1');
INSERT INTO `onethink_menu` VALUES ('449','彻底删除','448','0','Admin/WechatMass/deleteMessage','0','彻底删除本地和微信上的一条群发消息','','0','1');

INSERT INTO `onethink_menu` VALUES ('460','素材首页','300','14','Admin/WechatMaterial/index','0','素材管理首页','素材管理','0','1');

INSERT INTO `onethink_menu` VALUES ('461','图文素材','300','15','Admin/WechatNews/index','0','查看图文素材列表','素材管理','0','1');
INSERT INTO `onethink_menu` VALUES ('462','新增','461','0','Admin/WechatNews/addNews','0','新增图文素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('463','编辑','461','0','Admin/WechatNews/editNews','0','编辑图文素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('464','查找文章','461','1','Admin/WechatNews/findDocument','0','查找文章条目内容','','0','1');
INSERT INTO `onethink_menu` VALUES ('465','更新','461','2','Admin/WechatNews/updateNews','0','更新图文素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('466','删除','461','2','Admin/WechatNews/deleteNews','0','删除图文素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('467','上传','461','3','Admin/WechatNews/uploadNews','0','上传本地图文素材到微信','','0','1');
INSERT INTO `onethink_menu` VALUES ('468','下载','461','2','Admin/WechatNews/downloadNews','0','下载微信图文素材到本地','','0','1');
INSERT INTO `onethink_menu` VALUES ('469','新增文章','461','2','Admin/WechatNews/addTchatArticle','0','新增本地素材文章','','0','1');

INSERT INTO `onethink_menu` VALUES ('480','图片素材','300','16','Admin/WechatPicture/index','0','查看图片素材','素材管理','0','1');
INSERT INTO `onethink_menu` VALUES ('481','新增','480','0','Admin/WechatPicture/addPicture','0','新增图片素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('482','删除','480','1','Admin/WechatPicture/deletePicture','0','删除图片素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('483','从微信下载','480','2','Admin/WechatPicture/downloadPicture','0','从微信下载图片素材','','0','1');

INSERT INTO `onethink_menu` VALUES ('500','语音素材','300','17','Admin/WechatVoice/index','0','查看语音素材','素材管理','0','1');
INSERT INTO `onethink_menu` VALUES ('501','新增','500','0','Admin/WechatVoice/addVoice','0','新增语音素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('502','删除','500','1','Admin/WechatVoice/deleteVoice','0','删除语音素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('503','从微信下载','500','2','Admin/WechatVoice/downloadVoice','0','从微信下载语音素材','','0','1');

INSERT INTO `onethink_menu` VALUES ('520','视频素材','300','18','Admin/WechatVideo/index','0','查看视频素材','素材管理','0','1');
INSERT INTO `onethink_menu` VALUES ('521','新增','520','0','Admin/WechatVideo/addVideo','0','新增视频素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('522','删除','520','1','Admin/WechatVideo/deleteVideo','0','删除视频素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('523','从微信下载','520','2','Admin/WechatVideo/downloadVideo','0','从微信下载视频素材','','0','1');

INSERT INTO `onethink_menu` VALUES ('540','线上素材','300','19','Admin/WechatMaterial/online','0','查看线上素材','素材管理','0','1');

INSERT INTO `onethink_menu` VALUES ('560','小程序列表','300','20','Admin/WechatMiniprogram/index','0','查看绑定小程序','小程序管理','0','1');
INSERT INTO `onethink_menu` VALUES ('561','新增','560','0','Admin/WechatMiniprogram/create','0','新增绑定','','0','1');
INSERT INTO `onethink_menu` VALUES ('562','删除','560','1','Admin/WechatMiniprogram/edit','0','删除视频素材','','0','1');
INSERT INTO `onethink_menu` VALUES ('563','从微信下载','560','2','WechatMiniprogram/remove','0','从微信下载视频素材','','0','1');
-- --------------------
-- 前台控制管理目录
-- --------------------

INSERT INTO `onethink_menu` VALUES ('1600', '前台', '0', '6', 'Admin/Home/index', '0', '前台控制模块管理目录', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('1601', '首页', '1600', '0', 'Admin/Home/index', '1', '前台配置首页', '通用配置', '0','1');

INSERT INTO `onethink_menu` VALUES ('1611', 'LOGO配置', '1600', '1', 'Admin/HomeControl/logo', '0', '用于设置前台LOGO', '通用配置', '0','1');
INSERT INTO `onethink_menu` VALUES ('1612', '保存', '1611', '0', 'Admin/HomeControl/saveLogo', '0', '保存前台LOGO', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1613', '文前配置', '1600', '2', 'Admin/HomeControl/article?part=BeforeArticle', '0', '配置位于前台文章页面正文前的部分', '通用配置', '0','1');
INSERT INTO `onethink_menu` VALUES ('1614', '文尾配置', '1600', '3', 'Admin/HomeControl/article?part=AfterArticle', '0', '用于配置前台文章末尾展示的内容', '通用配置', '0','1');
INSERT INTO `onethink_menu` VALUES ('1615', '保存', '1614', '0', 'Admin/HomeControl/saveArticle', '0', '保存文前末尾展示的内容', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('1616', '幻灯片配置', '1600', '2', 'Admin/HomeControl/slide', '0', '用于配置首页幻灯片的显示', '首页栏目', '0','1');
INSERT INTO `onethink_menu` VALUES ('1617', '保存', '1616', '0', 'Admin/HomeControl/saveSlide', '0', '保存首页幻灯片', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1618', '特色内容配置', '1600', '4', 'Admin/HomeControl/feature', '0', '用于配置首页特色内容展示', '首页栏目', '0','1');
INSERT INTO `onethink_menu` VALUES ('1619', '保存', '1618', '0', 'Admin/HomeControl/saveFeature', '0', '保存首页特色内容展示', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('1630', '主题模板', '1600', '3', 'Admin/HomeControl/theme', '0', '用于配置前台默认的主题模板', '模版配置', '0','1');
INSERT INTO `onethink_menu` VALUES ('1631', '清除', '1630', '0', 'Admin/HomeControl/clear', '0', '清除当前前台设置部分的配置', '前台配置', '0','1');

INSERT INTO `onethink_menu` VALUES ('1640', '链接列表', '1600', '4', 'Admin/Friendship/index', '0', '查看友情链接列表', '友情链接', '0','1');
INSERT INTO `onethink_menu` VALUES ('1641', '新增', '1640', '1', 'Admin/Friendship/create', '0', '新增友情链接', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1642', '编辑', '1640', '2', 'Admin/Friendship/edit', '0', '编辑友情链接', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1643', '更新', '1640', '3', 'Admin/Friendship/update', '0', '更新友情链接内容', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1644', '状态', '1640', '4', 'Admin/Friendship/setStatus', '0', '更改友情链接的启用状态', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('1670', '邮件模版', '68', '4', 'Admin/Mail/index', '0', '用于查看所有邮件模版', '邮件管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('1671', '预览', '1670', '4', 'Admin/Mail/info', '0', '查看指定的邮件详情', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1672', '新增', '1670', '4', 'Admin/Mail/create', '0', '新增一个邮件模版', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1673', '编辑', '1670', '4', 'Admin/Mail/edit', '0', '编辑一个邮件模版', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1674', '更新', '1670', '4', 'Admin/Mail/update', '0', '更新邮件模版内容', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1675', '状态', '1670', '4', 'Admin/Mail/setStatus', '0', '更改邮件的启用状态', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('1676', '发送邮件', '68', '0', 'Admin/Mail/mail', '0', '自定义发送邮件', '邮件管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('1677', '配置邮箱', '68', '0', 'Admin/Config/group?id=6', '0', '配置发送邮件用的邮箱', '邮件管理', '0','1');

INSERT INTO `onethink_menu` VALUES ('1680', '查看版本', '68', '4', 'Admin/OnechatUpdate/index', '0', '用于查看是否有新OneChat版本更新', '系统升级', '0','1');
INSERT INTO `onethink_menu` VALUES ('1681', '升级系统', '1680', '0', 'Admin/Update/index', '0', '升级现有系统版本到新版本', '', '0','1');


-- 若有其他板块的目录，可在此添加或在功能板块安装程序中向onethink_menu中添加目录数据，比如针对房地产行业的【地产】板块，建议ID号从2000以后开始

-- --------------------
-- 新增相册板块管理目录
-- --------------------

INSERT INTO `onethink_menu` VALUES ('2000', '相册', '0', '5', 'Admin/Album/index', '0', '相册模块管理目录', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('2001', '相册列表', '2000', '0', 'Admin/Album/index', '0', '查看当前的相册列表', '相册管理', '0','1');
INSERT INTO `onethink_menu` VALUES ('2002', '详情', '2001', '0', 'Admin/Album/detail', '0', '查看一个相册详情', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2003', '更新', '2001', '0', 'Admin/Album/update', '0', '更新一个相册', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2004', '编辑', '2001', '0', 'Admin/Album/edit', '0', '编辑一个相册', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2005', '改变状态', '2001', '0', 'Admin/Album/setStatus', '0', '改变一个相册的启用状态', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2006', '删除', '2001', '0', 'Admin/Album/remove', '0', '删除一个相册', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2007', '删除图片', '2001', '0', 'Admin/Album/deletePic', '0', '删除相册内图片', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2008', '移动图片', '2001', '0', 'Admin/Album/movePic', '0', '移动相册内图片', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('2009', '新增相册', '2000', '0', 'Admin/Album/create', '0', '新增一个相册', '相册管理', '0','1');

INSERT INTO `onethink_menu` VALUES ('2010', '分类列表', '2000', '0', 'Admin/Album/category', '0', '管理相册分类', '相册分类', '0','1');
INSERT INTO `onethink_menu` VALUES ('2011', '编辑', '2010', '0', 'Admin/Album/editCategory', '0', '编辑相册分类', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2012', '更新', '2010', '0', 'Admin/Album/updateCategory', '0', '更新相册分类', '', '0','1');
INSERT INTO `onethink_menu` VALUES ('2013', '删除', '2010', '0', 'Admin/Album/deleteCategory', '0', '删除相册分类', '', '0','1');

INSERT INTO `onethink_menu` VALUES ('2020', '新增分类', '2000', '0', 'Admin/Album/addCategory', '0', '新增相册分类', '相册分类', '0','1');


-- -----------------------------
-- Table structure for `onethink_tchat_album`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_album`;
CREATE TABLE `onethink_tchat_album` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT'相册ID',
	`name` varchar(200) NOT NULL COMMENT '相册标识',
	`title` varchar(200) NOT NULL COMMENT '相册名称',
	`sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
  `content` text NOT NULL DEFAULT '' COMMENT '相册详细介绍',
	`cat_id` int(10) NOT NULL COMMENT '所属分类ID',
	`allow_publish` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许投稿',
	`list_row` tinyint(3) unsigned NOT NULL DEFAULT '12' COMMENT '每页展示数量',
	`reply` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '是否允许回复',
	`check` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT '发布的图片是否需要审核',
	`meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的网页标题',
	`keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
	`cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面图片',
	`link_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '外链',
	`display` tinyint(2) NOT NULL DEFAULT '1' COMMENT '可见性，0私有，1公开',
	`max_limit` int(4) NOT NULL DEFAULT '0' COMMENT '最多上传图片数量，0限制数目为最多9999',
	`vote` tinyint(2) NOT NULL DEFAULT '0' COMMENT '投票设置,1为启用，0为禁用',
	`likes` int(10) NOT NULL DEFAULT '0' COMMENT '赞数',
	`template` varchar(100) NOT NULL COMMENT '自定义模板',
	`extend` text NOT NULL COMMENT '扩展属性',
	`status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '相册状态(0禁用，1正常，-1删除)',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '相册所属用户ID',
	PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT '相册存储表';

INSERT INTO `onethink_tchat_album` VALUES (1, 'collecting box', '收集箱', '0', '这是系统自带的收集箱，将不会在前台展示', '<h1>收集箱功能介绍:</h1><p>收集箱主要有一下几种功能：</p><ol><li>暂时</li></ol>', '1', '0', '12', '0', '0', '系统相册【收集箱】', '收集箱,杂物箱,未归置,相册,album', '0', '0', '0', '9999', '0', '0', '', '', '1', '1486740491','1');

-- -----------------------------
-- Table structure for `onethink_tchat_album_category`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_album_category`;
CREATE TABLE `onethink_tchat_album_category` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类ID',
	`name` varchar(30) NOT NULL COMMENT '标识',
	`title` varchar(50) NOT NULL COMMENT '标题',
	`sort` int(10) unsigned NOT NULL DEFAULT '1' COMMENT '排序',
	`list_row` tinyint(3) unsigned NOT NULL DEFAULT '10' COMMENT '列表每页行数',
	`meta_title` varchar(50) NOT NULL DEFAULT '' COMMENT 'SEO的网页标题',
	`keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
	`description` varchar(255) NOT NULL DEFAULT '' COMMENT '简介',
	`template_lists` varchar(100) NOT NULL COMMENT '列表页模板',
	`template_detail` varchar(100) NOT NULL COMMENT '详情页模板',
	`display` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '可见性,0后台可见,1前台可见,2登录可见',
	`reply_model` varchar(100) NOT NULL DEFAULT '',
	`extend` text NOT NULL COMMENT '扩展设置',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	`status` tinyint(4) NOT NULL DEFAULT '1' COMMENT '数据状态',
	`icon` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '分类图标',
	PRIMARY KEY (`id`),
	UNIQUE KEY `uk_name` (`name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT '相册分类表';

INSERT INTO `onethink_tchat_album_category` VALUES ('1', 'default', '默认分类', '0', '10', '默认相册分类', '默认,系统,自带', '这是一个系统自带的默认分类', '', '', '1', '', '', '1486740474', '1486740474', '1', '0');;

-- -----------------------------
-- Table structure for `onethink_tchat_articles`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_articles`;
CREATE TABLE `onethink_tchat_articles` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '素材文章ID',
	`media_id` VARCHAR(50) NOT NULL COMMENT '所属图文素材的meida_id',
	`sort` tinyint(2) DEFAULT '1' COMMENT '图文内排序0-9',
	`model_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '对应的系统模型ID',
	`article_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '对应该系统模型的条目ID',
	`title` VARCHAR(100) NOT NULL DEFAULT '' COMMENT'文章标题',
	`thumb_media_id` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '图文消息的封面图片素材id（必须是永久mediaID）',
	`author` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '文章作者',
	`digest` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '图文消息的摘要，仅有单图文消息才有摘要，多图文此处为空',
	`show_cover_pic` tinyint(2) DEFAULT '0' COMMENT '是否显示封面，0为false，即不显示，1为true，即显示',
	`content` text NOT NULL DEFAULT '' COMMENT '文章内容',
	`content_source_url` VARCHAR(200) NOT NULL DEFAULT '' COMMENT'图文消息的原文地址，即点击“阅读原文”后的URL',
  `status` tinyint(2) NOT NULL DEFAULT '0' COMMENT '内容数据状态 0：本地修改 1：已处理（上传正文内图片）',
	`uid` int(10) unsigned NOT NULL DEFAULT '2' COMMENT '用户ID',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '微信图文素材文章表';

-- -----------------------------
-- Table structure for `onethink_tchat_client`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_client`;
CREATE TABLE `onethink_tchat_client` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '微信粉丝ID',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '绑定用户ID',
	`openid` VARCHAR(100) NOT NULL COMMENT '微信粉丝openID',
	`subscribe` tinyint(2) DEFAULT '1' COMMENT '微信粉丝关注状态 1为正在关注 0为取消关注',
	`nickname` VARCHAR(100) NOT NULL DEFAULT '' COMMENT'微信粉丝昵称',
	`sex` tinyint(4) DEFAULT '0' COMMENT '微信粉丝性别，1男2女0未知',
	`city` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '微信粉丝所在城市',
	`country` VARCHAR(20) NOT NULL DEFAULT '中国' COMMENT '微信粉丝所在国家',
	`province` VARCHAR(20) NOT NULL DEFAULT '' COMMENT '微信粉丝所在省份',
	`language` VARCHAR(10) NOT NULL DEFAULT 'zh_CN' COMMENT '微信粉丝语言',
	`headimgurl` VARCHAR(200) NOT NULL DEFAULT '' COMMENT'微信粉丝头像',
	`subscribe_time` int(10) NOT NULL DEFAULT '0' COMMENT'微信粉丝关注时间',
	`unionid` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '只有在用户将公众号绑定到微信开放平台帐号后，才会出现该字段。',
	`remark` VARCHAR(100) NOT NULL DEFAULT '' COMMENT '公众号运营者对粉丝的备注',
	`groupid` int(10) unsigned NOT NULL COMMENT '用户所在的分组ID',
	`tagid_list` VARCHAR(50) NOT NULL DEFAULT '' COMMENT '粉丝身上的标签',
	`event_key` VARCHAR(32) NOT NULL DEFAULT '' COMMENT '扫描关注时的EventKey',
	PRIMARY KEY(`id`),
  UNIQUE KEY `uk_openid` (`openid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '微信粉丝信息表';

-- -----------------------------
-- Table structure for `onethink_tchat_client_extra`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_client_extra`;
CREATE TABLE `onethink_tchat_client_extra` (
  `client_id` int(10) NOT NULL COMMENT '微信粉丝ID',
	`name` varchar(50) NOT NULL COMMENT '姓名',
	`phone` varchar(50) NOT NULL COMMENT '电话',
	`mobile` varchar(50) NOT NULL COMMENT '手机',
	`email` varchar(100) NOT NULL COMMENT '邮箱',
	PRIMARY KEY (`client_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT '粉丝扩展属性表';

-- -----------------------------
-- Table structure for `onethink_tchat_client_photo`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_client_photo`;
CREATE TABLE `onethink_tchat_client_photo` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT'微信粉丝图片ID',
	`client_id` int(10) NOT NULL COMMENT '微信粉丝ID',
	`Album_id` int(10) NOT NULL COMMENT '所属相册ID',
	`likes` int(10) NOT NULL DEFAULT '0' COMMENT '赞数',
	`path` varchar(300) NOT NULL COMMENT '图片存储路径',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT '粉丝传送图片存储表';

-- -----------------------------
-- Table structure for `onethink_tchat_events`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_events`;
CREATE TABLE `onethink_tchat_events` (
	`id` int(2) unsigned NOT NULL AUTO_INCREMENT COMMENT '事件ID',
	`event_name` varchar(100) NOT NULL COMMENT '事件名称,用于中文注释识别',
	`event_type` varchar(50) NOT NULL COMMENT '对应微信事件类型',
	`flow_id` int(10) NOT NULL DEFAULT '0' COMMENT '回复流ID',
	`action_data` varchar(300) NOT NULL DEFAULT '' COMMENT '回复流所需数据',
	`status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '数据状态 -1：暂未开启 0：已经禁用 1：已经启用',
  `comment` varchar(250) NOT NULL DEFAULT '' COMMENT '事件说明',
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '事件判断回复表';

-- -----------------------------
-- Records of `onethink_tchat_events`
-- -----------------------------
INSERT INTO `onethink_tchat_events` VALUES ('1','客户关注','subscribe','2','1','1','当客户新关注公众帐号时的回复内容设置<br /><strong>注意：</strong>若通过二维码关注，且设置了有效的回复内容，则回复二维码回复内容');
INSERT INTO `onethink_tchat_events` VALUES ('2','扫描二维码','SCAN','2','1','1','客户扫描带参数二维码事件但没有设置或者设置失效时的回复内容。');
INSERT INTO `onethink_tchat_events` VALUES ('3','上报地理位置','LOCATION','3','1','-1','客户上报地理位置事件的回复内容。');
INSERT INTO `onethink_tchat_events` VALUES ('4','关键词没有结果','KeywordNoMatch','2','4','1','客户在发送关键词后没有查找到关键词对应内容时的回复。');
INSERT INTO `onethink_tchat_events` VALUES ('5','分类中没有文章','CategoryNoArticle','2','5','1','查找某个分类下的内容却没有任何正常状态下的文章时的回复。');

-- -----------------------------
-- Table structure for `onethink_tchat_keyword_group`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_keyword_group`;
CREATE TABLE `onethink_tchat_keyword_group` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '关键词组ID',
	`name` varchar(100) NOT NULL COMMENT '关键词组名',
	`keywords` varchar(200) NOT NULL COMMENT '关键词(多个请用英文状态逗号分割)',
	`flow_id` int(10) NOT NULL DEFAULT '0' COMMENT '回复流ID',
	`action_data` varchar(300) NOT NULL DEFAULT '' COMMENT '回复流所需数据',
	`start_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '生效时间',
	`deadline` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '失效时间',
	`dead_text` int(5) unsigned  NOT NULL DEFAULT '0' COMMENT '失效后回复文本',
	`status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '关键词使用状态，-1为删除，0为禁用，1为正常，',
	`uid` int(10) unsigned NOT NULL DEFAULT '2' COMMENT '用户ID',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '关键词组回复内容表';

INSERT INTO `onethink_tchat_keyword_group` VALUE ('1000','帮助','帮助,？,?,help','2','2','0','0','5','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_keyword_group` VALUE ('1001','公司新闻','新闻,动态,news','3','21','0','0','5','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_keyword_group` VALUE ('1002','关于我们','关于,你好,了解,hi,about','3','11','0','0','5','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_keyword_group` VALUE ('1003','优惠新闻','优惠,活动,折扣,打折,有什么优惠,有什么活动','3','23','0','0','5','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_keyword_group` VALUE ('1004','建议','建议,意见,投诉,申诉,[生气],[愤怒],不满','7','','0','0','5','1','2','1407334493','1407334493');

-- -----------------------------
-- Table structure for `onethink_tchat_mail`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_mail`;
CREATE TABLE `onethink_tchat_mail` (
	`id` int(10) unsigned NOT NULL auto_increment COMMENT '配置ID',
	`name` varchar(100) NOT NULL DEFAULT '' COMMENT '配置标识',
	`description` varchar(100) NOT NULL DEFAULT '无标题' COMMENT '配置说明',
	`mail_title` varchar(100) NOT NULL DEFAULT '无标题' COMMENT '邮件标题',
	`mail_content` text NOT NULL DEFAULT '' COMMENT '邮件内容，支持HTML格式',
	`preg_content` text NOT NULL DEFAULT '' COMMENT '替换内容标签',
	`sign_name`  varchar(250)  NOT NULL DEFAULT '' COMMENT '邮件签名',
	`status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '配置启用状态',
	`mail_type` tinyint(2) NOT NULL DEFAULT '0' COMMENT '配置类型：0系统配置1自定义配置',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY  (`id`),
  INDEX(`mail_title`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='邮件模版配置表';

INSERT INTO `onethink_tchat_mail` VALUE ('1','register','注册邮件','感谢您注册{webSite},这是您的注册信息','<p>感谢您注册{webSite},以下是您在我们网站的注册信息，请您牢记如下资料：</p><p>您的帐号ID：{uid}</p><p>您的帐号登录名：{username}</p><p>您的帐号昵称：{nickname}</p><p>您的密码：{password}</p><p>您的邮箱：{email}</p><p>您的手机号：{phone}</p><p>{signName}</p>','','<br />--------<br /> 我们将竭诚为您服务<br />Tchat 开发组','1','0','1407334493','1407334493');
INSERT INTO `onethink_tchat_mail` VALUE ('2','change_profile','账户信息更新','请注意！您在{webSite}的注册信息已经变更！','<p>尊敬的用户{userName}：</p><p style="text-intent:2em;">您好！</p><p style="text-intent:2em;">我们注意到，您的账户信息于<span style="color:red">{dateTime}</span>{dateTime}</span>发生过更改，如果您确定是由您自己操作的，您可以忽略本邮件，如果您没有操作过，很可能您的帐号密码已经泄漏，请尽快点击下面的链接(有效期7天)重置您的账户密码:</p><p><a href="{urlResetPassword}" target="_blank">{urlResetPassword}</a></p><p>{signName}</p>','','<br />--------<br /> 我们将竭诚为您服务<br />OneCchat 开发组','1','0','1407334493','1407334493');
INSERT INTO `onethink_tchat_mail` VALUE ('3','to_reset_password','提交密码重置邮件','{userName}重置密码申请！- {webSite}','<p>尊敬的用户{userName}：</p><p style="text-intent:2em;">您好！</p><p style="text-intent:2em;">您收到这封邮件是因为您的账户于<span style="color:red">{dateTime}</span>提交了密码重置的申请，如果是由您本人提交，请在24小时内，点击下面的链接重置您的登录密码:</p><p><a href="{urlResetPassword}" target="_blank">{urlResetPassword}</a></p><p>如果您没有提交过重置密码的申请，请您忽略本邮件。</p><p>{signName}</p>','','<br />--------<br /> 我们将竭诚为您服务<br />OneCchat 开发组','1','0','1407334493','1407334493');
INSERT INTO `onethink_tchat_mail` VALUE ('4','reset_password','密码已经重置邮件','请注意！您在{webSite}的密码已经重置！','<p>尊敬的用户{userName}：</p><p style="text-intent:2em;">您好！</p><p style="text-intent:2em;">我们注意到，您的账户于<span style="color:red">{dateTime}</span>操作了密码重置，如果您确定本次重置是由您本次操作的，您可以忽略本邮件，如果您没有操作过，很可能您的帐号密码已经泄漏，请尽快点击下面的链接(有效期7天)重置您的账户密码:</p><p><a href="{urlResetPassword}" target="_blank">{urlResetPassword}</a></p><p>{signName}</p>','','<br />--------<br /> 我们将竭诚为您服务<br />OneCchat 开发组','1','0','1407334493','1407334493');

-- -----------------------------
-- Table structure for `onethink_tchat_mass_message`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_mass_message`;
CREATE TABLE `onethink_tchat_mass_message` (
	`id` int(10) unsigned NOT NULL auto_increment COMMENT '本地消息ID',
	`msg_id` int(10) unsigned NOT NULL COMMENT'云端消息ID',
	`msgtype` varchar(100) NOT NULL DEFAULT 'text' COMMENT '消息类型',
	`tag_id` tinyint(2) NOT NULL DEFAULT '0' COMMENT '用户分组ID，0为发送到全部，逻辑处理时留空，同时is_to_all设置为true,填写为数字表示要发送到的标签组,is_to_all设置为False',
	`touser` text NOT NULL DEFAULT '' COMMENT '当指定发送到批量openid时使用发送到的用户ID集合，留空为全部',
	`content` text NOT NULL DEFAULT '' COMMENT '消息内容，为字符串json数据格式',
	`status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '消息状态，-1为本地删除[回收站] 0为本地[草稿]未上传 1为已上传微信端[待发送] 2为已提交在[发送中]可刷新状态 3为发送[成功] 4为发送[失败]可重发',
	`uid` int(10) unsigned NOT NULL DEFAULT '2' COMMENT '用户ID',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY  (`id`),
  INDEX(`msg_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信群发消息数据表';

-- -----------------------------
-- Table structure for `onethink_tchat_media`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_media`;
CREATE TABLE `onethink_tchat_media` (
  `id` int(10) unsigned NOT NULL auto_increment COMMENT '本地素材编号',
  `media_id` varchar(50) NOT NULL DEFAULT '' COMMENT '微信素材编号',
  `file_id` int(10) NOT NULL DEFAULT '0' COMMENT '系统文件ID，图片、语音、视频和缩略图用到',
  `media_type` varchar(20) NOT NULL DEFAULT '' COMMENT '素材类型,图文（news）、图片（image）、语音（voice）、视频（video）和缩略图（thumb）',
  `status` tinyint(2) NOT NULL default '0' COMMENT '状态,-1已删除，0本地草稿,1已同步',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信永久素材存储表';

-- -----------------------------
-- Table structure for `onethink_tchat_menu`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_menu`;
CREATE TABLE `onethink_tchat_menu` (
  `id` int(10) unsigned NOT NULL auto_increment COMMENT '编号',
  `menuid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '个性化菜单编号',
  `menu_type` varchar(20) NOT NULL DEFAULT '' COMMENT '菜单类型',
  `name` varchar(50) NOT NULL DEFAULT '' COMMENT '显示名称',
  `event_key` varchar(50) NOT NULL DEFAULT '' COMMENT '系统识别码',
  `appid` varchar(200) NOT NULL DEFAULT '' COMMENT '跳转的小程序APPID',
  `page_path` varchar(200) NOT NULL DEFAULT '' COMMENT '小程序页面路径',
  `sort` tinyint(2) NOT NULL default '0' COMMENT '排序',
  `url` varchar(250) NOT NULL DEFAULT '' COMMENT '跳转地址',
  `flow_id` int(10) NOT NULL DEFAULT '0' COMMENT '回复流ID',
	`action_data` varchar(300) NOT NULL DEFAULT '' COMMENT '回复流所需数据',
  `pid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '上级菜单ID',
  `status` tinyint(2) NOT NULL default '1' COMMENT '状态',
  `uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信菜单数据表';

INSERT INTO `onethink_tchat_menu` VALUE ('1','0','button','关于我们','ABOUTUS','','','0','','0','','0','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('2','0','view','查看官网','WEBSITE','','','0','http://www.juexin.pro','0','','1','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('3','0','click','公司介绍','gongsijieshao','','','1','','3','12','1','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('4','0','click','业务范围','yewufanwei','','','2','','3','13','1','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('5','0','click','企业文化','qiyewenhua','','','3','','3','14','1','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('6','0','button','新闻动态','NEWS','','','1','','0','','0','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('7','0','click','全部新闻','AllNews','','','1','','3','21','6','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('8','0','click','最新优惠','XinYouHui','','','2','','3','13','6','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('9','0','click','上线大吉','shangxian','','','0','','4','1','6','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('10','0','button','互动中心','HUDONG','','','2','','0','','0','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('11','0','click','我要提建议','TiJianYi','','','0','','7','','10','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('12','0','click','我找客服妹子','LiaoKeFu','','','1','','6','','10','1','2','1407334493','1407334493');

INSERT INTO `onethink_tchat_menu` VALUE ('20','1','button','会员资讯','HUIYUANZIXUN','','','0','','0','','0','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('21','1','click','会员最优惠','HUIYUANZUIYOUHUI','','','0','','3','13','20','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('22','1','click','会员活动','HUIYUANHUODONG','','','1','','2','55','20','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('23','1','view','会员论坛','LUNTAN','','','0','http://www.juexin.pro','0','','0','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('24','1','button','会员中心','VIPCENTER','','','2','','0','','0','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('25','1','click','我要提建议','TiJianYi','','','0','','7','','24','1','2','1407334493','1407334493');
INSERT INTO `onethink_tchat_menu` VALUE ('26','1','click','我找客服妹子','LiaoKeFu','','','1','','6','','24','1','2','1407334493','1407334493');

-- -----------------------------
-- Table structure for `onethink_tchat_menu_list`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_menu_list`;
CREATE TABLE `onethink_tchat_menu_list` (
  `id` int(10) unsigned NOT NULL auto_increment COMMENT '编号',
  `menuid` int(10) unsigned NOT NULL COMMENT '个性化菜单编号',
  `name` varchar(100) NOT NULL default '' COMMENT '个性化菜单标识',
  `title` varchar(100) NOT NULL default '' COMMENT '个性化菜单名称',
  `tag_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '适用标签编号',
  `sex` tinyint(2) NOT NULL default '0' COMMENT '适用性别，0不限制，1男2女',
  `country` varchar(20) NOT NULL default '中国' COMMENT '适用国家',
  `province` varchar(50) NOT NULL default '' COMMENT '适用省份',
  `city` varchar(50) NOT NULL default '' COMMENT '适用城市',
  `client_platform_type` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '适用操作系统 IOS(1), Android(2),Others(3) 0不做匹配',
  `language` varchar(20) NOT NULL default 'zh_CN' COMMENT '适用语言',
  `status` tinyint(2) NOT NULL default '0' COMMENT '发布状态，0未发布，1已发布',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信个性化菜单列表';

INSERT INTO `onethink_tchat_menu_list` VALUE ('1','100000000','sdredf','星标组菜单','2','0','中国','','','0','zh_CN','0','1407334493','1407334493');

-- -----------------------------
-- Table structure for `onethink_tchat_miniprogram`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_miniprogram`;
CREATE TABLE `onethink_tchat_miniprogram` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '绑定小程序ID',
	`appid`	varchar(50) NOT NULL UNIQUE COMMENT '绑定小程序的APPID',
	`name` varchar(50) NOT NULL COMMENT '绑定小程序的名称',
	`uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '小程序绑定列表';

-- -----------------------------
-- Table structure for `onethink_tchat_music`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_music`;
CREATE TABLE `onethink_tchat_music` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '音乐ID',
	`music_url`	varchar(200) NOT NULL UNIQUE COMMENT '音乐链接地址',
	`music_content` varchar(250) NOT NULL COMMENT '音乐简介',
	`uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '音乐类型回复内容表';


-- -----------------------------
-- Table structure for `onethink_tchat_picture`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_picture`;
CREATE TABLE `onethink_tchat_picture` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '相册内图片ID',
	`pic_id` int(10) unsigned NOT NULL COMMENT'系统存储的图片ID',
	`uid` int(10) NOT NULL COMMENT '上传用户UID',
	`album_id` int(10) NOT NULL COMMENT '所属相册ID',
	`title` varchar(255) NOT NULL DEFAULT '' COMMENT '标题',
	`author` varchar(255) NOT NULL DEFAULT '' COMMENT '作者',
	`location` varchar(255) NOT NULL DEFAULT '' COMMENT '位置',
	`keywords` varchar(255) NOT NULL DEFAULT '' COMMENT '关键字',
	`description` varchar(255) NOT NULL DEFAULT '' COMMENT '描述',
	`likes` int(10) NOT NULL DEFAULT '0' COMMENT '赞数',
	PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COMMENT 'Tchat相册图片信息表';

-- -----------------------------
-- Table structure for `onethink_tchat_reply_flow`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_reply_flow`;
CREATE TABLE `onethink_tchat_reply_flow` (
  `id` int(10) unsigned NOT NULL auto_increment COMMENT '编号',
  `module` varchar(20) NOT NULL default '' COMMENT '处理模块',
  `layer` varchar(20) NOT NULL default '' COMMENT '控制器目录',
  `controller` varchar(20) NOT NULL default '' COMMENT '处理控制器（不需要全名和后缀）',
  `action_name` varchar(100) NOT NULL default '' COMMENT '处理方法标识（必须在对应模块Event目录下Replay控制器里有对应处理流程）',
  `title` varchar(100) NOT NULL default '' COMMENT '回复流名称',
  `action_tip` varchar(100) NOT NULL default '' COMMENT '使用描述',
  `need_data` tinyint(2) NOT NULL default '1' COMMENT '是否需要数据，0不需要，1需要,默认需要',
  `data_type` tinyint(2) NOT NULL default '0'  COMMENT '数据类型(0文本，1长文本，2数字,3数组)',
  `data_default` varchar(100) NOT NULL default ''  COMMENT '数据默认值(input的value默认值)',
  `data_tip` varchar(100) NOT NULL default '' COMMENT '数据填写描述(placeholder或者其他方式的提示)',
  `status` tinyint(2) NOT NULL default '1' COMMENT '状态，0停用，1有效',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='微信回复流程注册表';

INSERT INTO `onethink_tchat_reply_flow` VALUE ('1','Admin','Event','Reply','keyword','触发关键词','系统回复一个指定关键词的获取结果','1','0','','请输入需要回复的关键词','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('2','Admin','Event','Reply','text','回复文本','系统回复一段文本信息','1','1','','请输入需要回复的文本或文本素材的ID','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('3','Admin','Event','Reply','category','指定分类','回复站点指定分类下的文章','1','0','','请输入需要获取的分类ID编号','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('4','Admin','Event','Reply','document','指定文章','回复站点指定的文章或文章集合','1','0','','请输入需要获取的文章ID编号，多个请以英文状态下的逗号分割','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('5','Admin','Event','Reply','author','指定作者','回复指定的作者的文章集合','1','0','','多个作者请以逗号分开','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('6','Admin','Event','Reply','service','转接客服','转接客服号','1','0','','输入多客服帐号，不输入则随机指派','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('7','Admin','Event','Reply','suggest','收集建议','收集客户的建议','0','0','','','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('8','Admin','Event','Reply','albumCategory','相册分类','回复指定的相册分类下的相册合辑','1','0','','请输入相册分类ID编号','1','1379235841','1379235841');
INSERT INTO `onethink_tchat_reply_flow` VALUE ('9','Admin','Event','Reply','album','指定相册','回复站点指定的相册详情','1','0','','请输入需要获取的相册ID编号，多个请以英文状态下的逗号分割','1','1379235841','1379235841');

-- -----------------------------
-- Table structure for `onethink_tchat_suggestion`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_suggestion`;
CREATE TABLE `onethink_tchat_suggestion` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '建议条目ID',
	`client_id` int(10) NOT NULL COMMENT '微信粉丝ID',
	`content` varchar(250) NOT NULL COMMENT '建议内容',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '微信粉丝建议内容表';

-- -----------------------------
-- Table structure for `onethink_tchat_text`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_text`;
CREATE TABLE `onethink_tchat_text` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '文本型回复内容ID',
	`content` varchar(250) NOT NULL UNIQUE COMMENT '文本回复内容',
	`uid` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '用户ID',
	`create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY(`id`),
	INDEX(`content`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '文本类型回复内容表';

-- -----------------------------
-- Records of `onethink_tchat_text`
-- 默认的文本内容
-- -----------------------------
INSERT INTO `onethink_tchat_text` VALUES ('1','感谢您的关注，如需了解如何使用请回复“帮助”或“?”','2','1393292899','1393292899');
INSERT INTO `onethink_tchat_text` VALUES ('2','您可以通过回复如下关键字获取我们的信息\n帮助：获得账号的使用帮助\n关于：认识下我们吧\n新闻：了解我们的最新新闻或动态\n优惠：了解我们的最新优惠\n建议：给我们提提您的建议','2','1393292899','1393292899');
INSERT INTO `onethink_tchat_text` VALUES ('3','非常感谢您的配合，祝您生活愉快。','2','1393292899','1393292899');
INSERT INTO `onethink_tchat_text` VALUES ('4','/::< 抱歉，没有为您找到想要的结果。','2','1393292899','1393292899');
INSERT INTO `onethink_tchat_text` VALUES ('5','哎呀，仓库里空空如也，啥也没找到！>_<|||\n你有哆啦A梦的口袋么？','2','1393292899','1393292899');
INSERT INTO `onethink_tchat_text` VALUES ('55','哎呀，真不好意思，这个活动已经截止啦，好遗憾没能在那个疯狂的日子里见到你的身影，不过我们还有很多其他的活动正在进行，您可以回复“优惠”或“活动”，看看我们有哪些活动吧，这次可不要再错过咯。','2','1393292899','1393292899');
INSERT INTO `onethink_tchat_text` VALUES ('56','感谢您的配合，您的建议我们会送达相关部门处理，如有需要我们会跟您取得联系，谢谢。','2','1393292899','1393292899');

-- -----------------------------
-- Table structure for `onethink_tchat_qrcode`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_tchat_qrcode`;
CREATE TABLE `onethink_tchat_qrcode` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '微信二维码ID',
	`scene` varchar(50) NOT NULL DEFAULT'' COMMENT '应用场景',
	`ticket` varchar(250) NOT NULL UNIQUE DEFAULT'' COMMENT 'Ticket编码',
	`module` varchar(20) NOT NULL DEFAULT'' COMMENT '应用模块',
  `flow_id` int(10) NOT NULL DEFAULT '0' COMMENT '回复流ID',
	`action_data` varchar(300) NOT NULL DEFAULT '' COMMENT '回复流所需数据',
	`qrcode_type` varchar(20) NOT NULL DEFAULT 'QR_LIMIT_SCENE' COMMENT '二维码类型，QR_SCENE为临时,QR_LIMIT_SCENE为永久,QR_LIMIT_STR_SCENE为永久的字符串参数值',
	`scan_num` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '二维码扫描次数', 	
  `status` tinyint(2) NOT NULL default '1' COMMENT '状态',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY(`id`),
	INDEX(`ticket`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '二维码存储表';

