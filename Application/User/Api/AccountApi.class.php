<?php
// +----------------------------------------------------------------------
// | OneThink [ WE CAN DO IT JUST THINK IT ]
// +----------------------------------------------------------------------
// | Copyright (c) 2013 http://www.onethink.cn All rights reserved.
// +----------------------------------------------------------------------
// | Author: 麦当苗儿 <zuojiazi@vip.qq.com> <http://www.zjzit.cn>
// +----------------------------------------------------------------------

namespace User\Api;


class AccountApi extends UserApi{

    /**
     * 构建重置密码链接
     * @param int $uid          要重置密码的帐号UID
     * @param bool $system    是否是系统邮件来获取重置链接
     * @return mixed
     * @author TealunDu <tealun@guilins.com>
     */
    public function buildResetPasswordUrl($uid,$system=false){

        //获取账户基本信息
        $info = $this->info($uid);

        //使用thinkphp系统加密函数加密UID
        // !important 不要设置过期时间
        //因为这样加密后的密文会根据当前时间变动
        //会导致虽然UID解密后相同，但没办法进行申请次数的计算
        $uid=think_encrypt($uid,UC_AUTH_KEY);

        //获取随机字符串
        $token =get_rand_chars(10);
        //使用think加密函数构建CODE
        $mail = $info[2];
        $phone= $info[3];
        $code = think_ucenter_md5($token.$mail.$phone, UC_AUTH_KEY);

        if($system){
            $sum = 0;//系统邮件将次数更改为0
            $time= 0;//有效期开始时间设置为当前时间
            $expire = 604800;//系统邮件发送中的重置链接有效期为7天
        }else{
            //检查是否已有重置密码的缓存
            if($s = S('reset'.$uid)){
                $sum = $s['sum']+1;//次数+1
                //增加是否是系统邮件获取所做的缓存判断，然后赋值相应的时间和过期时间
                $time= $sum == 1?time():$s['time'];//有效期开始时间设置为第一次申请的时间
                $expire = $sum == 1?86400:86400+$s['time']-time();//缓存有效期为(第一次申请的时间+86400-当前时间)
                S($s['token'],null);//删除掉之前token的缓存,下面会更新新的token的缓存
            }else{//24小时后或第一次申请时
                $sum = 1;//次数为1
                $time=time();//有效期开始时间设置为当前时间
                $expire = 86400;//有效期为1天
            }
            if($sum > 3){
                return false;
            }
        }

        //设置账户的缓存
        S('reset'.$uid,array('token'=>$token,'sum'=>$sum,'time'=>$time),$expire);

        //设置token的缓存 UID为加密后的字符串

        S($token,array('uid'=>$uid,'status'=>0),86400);

        return 'http://'.$_SERVER['HTTP_HOST'].'/index.php?s=/Home/User/resetPassWord/to/'.$token.'/co/'.$code;

    }

    /**
     * 检测URL合法性
     * @param string $token 系统生成的加密token
     *
     * @return bool
     *
     * @author TealunDu <tealun@guilins.com>
     */
    public function checkResetUrl($token){
        $tokenSave = S($token);
        //判断token所含UID是否有缓存
        $s = S('reset'.$tokenSave['uid']);
        //该UID缓存的token信息是否与链接中的token对应
        if($token == $s['token']){
            return true; //对应则返回true
        }
        //所有非相等的情况 统一返回false
        return false;
    }

    /**
     * 检测账户信息
     * @param string $token 系统的加密token
     * @param string $code  链接的验证CODE
     * @param string $email 用户验证输入的Email账户
     *
     * @return bool
     *
     * @author TealunDu <tealun@guilins.com>
     */
    public function checkResetCode($token,$code,$mail){

        //没有传入token和code的情况
        if(empty($token) || empty($code) ){
            return -2;
        }

        //还原加密过的用户ID，获取用户信息
        $s = S($token);
        $uid = think_decrypt($s['uid'],UC_AUTH_KEY);
        $User = new UserApi();
        $info = $User->info($uid);

        //如果提交的email与数据库email不一致
        if($mail !== $info[2]){
            return -1;
        }

        //构建效验CODE
        $phone= $info[3];//用户手机号
        $checkCode = think_ucenter_md5($token.$mail.$phone, UC_AUTH_KEY);

        //效验客户提交code
        if($checkCode !== $code){
            return 0;
        }else{
            return 1;
        }

    }

}
