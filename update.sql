-- -----------------------------
-- 说明：本SQL文件用于升级到更新SQL
-- -----------------------------
-- 添加管理员权限
UPDATE `onethink_auth_group` SET `rules` = CONCAT(rules,',441,442,443,461,462,463,481,482,483,1659,1663,1664') WHERE `id` ='2' limit 1;

INSERT IGNORE INTO `onethink_tchat_reply_flow` VALUE ('8','Admin','Event','Reply','albumCategory','相册分类','回复指定的相册分类下的相册合辑','1','0','','请输入相册分类ID编号','1','1379235841','1379235841');
INSERT IGNORE INTO `onethink_tchat_reply_flow` VALUE ('9','Admin','Event','Reply','album','指定相册','回复站点指定的相册详情','1','0','','请输入需要获取的相册ID编号，多个请以英文状态下的逗号分割','1','1379235841','1379235841');

UPDATE `onethink_auth_rule` SET `name` = 'Admin/WechatNews/index' WHERE `id` = '421' limit 1;
UPDATE `onethink_auth_rule` SET `name` = 'Admin/WechatPicture/index' WHERE `id` = '440' limit 1;
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('441', 'admin', '1', 'Admin/WechatPicture/addPicture','查看图片素材','1','');
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('442', 'admin', '1', 'Admin/WechatPicture/deletePicture','删除图片素材','1','');
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('443', 'admin', '1', 'Admin/WechatPicture/downloadPicture','从微信下载图片素材','1','');

UPDATE `onethink_auth_rule` SET `name` = 'Admin/WechatVoice/index' WHERE `id` = '460' limit 1;
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('461', 'admin', '1', 'Admin/WechatVoice/addVoice','查看语音素材','1','');
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('462', 'admin', '1', 'Admin/WechatVoice/deleteVoice','删除语音素材','1','');
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('463', 'admin', '1', 'Admin/WechatVoice/downloadVoice','从微信下载语音素材','1','');

UPDATE `onethink_auth_rule` SET `name` = 'Admin/WechatVideo/index' WHERE `id` = '480' limit 1;
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('481', 'admin', '1', 'Admin/WechatVideo/addVideo','新增视频素材','1','');
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('482', 'admin', '1', 'Admin/WechatVideo/deleteVideo','删除视频素材','1','');
INSERT IGNORE INTO `onethink_auth_rule` VALUES ('483', 'admin', '1', 'Admin/WechatVideo/downloadVideo','从微信下载视频素材','1','');

UPDATE `onethink_auth_rule` SET `name` = 'Admin/OnechatUpdate/index' WHERE `id` = '1650' limit 1;
UPDATE `onethink_menu` SET `url` = 'Admin/OnechatUpdate/index',`tip`='用于查看是否有新OneChat版本更新' WHERE `id` = '1680' limit 1;

INSERT IGNORE INTO `onethink_auth_rule` VALUES ('1659', 'admin', '1', 'Admin/Config/group?id=6', '配置邮件', '1', '');

DELETE FROM `onethink_auth_rule` WHERE `id` in (1663,1664,1665,1666,1667,1668);
INSERT INTO `onethink_auth_rule` VALUES ('1663', 'admin', '1', 'Admin/Wechat/contentGuide?segment=albumCategory', '查看内容引导', '1', '');
INSERT INTO `onethink_auth_rule` VALUES ('1664', 'admin', '1', 'Admin/Wechat/contentGuide?segment=album', '查看内容引导', '1', '');

INSERT IGNORE INTO `onethink_menu` VALUES ('309', '获取相册分类', '301', '2', 'Admin/Wechat/contentGuide?segment=albumCategory', '0', '获取相册分类列表', '', '0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('310', '获取相册列表', '301', '2', 'Admin/Wechat/contentGuide?segment=album', '0', '获取相册列表', '', '0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('331', '保存', '321', '3', 'Admin/WechatKeyword/update', '0', '更新关键词', '', '0','1');

INSERT IGNORE INTO `onethink_menu` VALUES ('481','新增','480','0','Admin/WechatPicture/addPicture','0','新增图片素材','','0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('482','删除','480','1','Admin/WechatPicture/deletePicture','0','删除图片素材','','0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('483','从微信下载','480','2','Admin/WechatPicture/downloadPicture','0','从微信下载图片素材','','0','1');

INSERT IGNORE INTO `onethink_menu` VALUES ('501','新增','500','0','Admin/WechatVoice/addVoice','0','新增语音素材','','0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('502','删除','500','1','Admin/WechatVoice/deleteVoice','0','删除语音素材','','0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('503','从微信下载','500','2','Admin/WechatVoice/downloadVoice','0','从微信下载语音素材','','0','1');

INSERT IGNORE INTO `onethink_menu` VALUES ('521','新增','520','0','Admin/WechatVideo/addVideo','0','新增视频素材','','0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('522','删除','520','1','Admin/WechatVideo/deleteVideo','0','删除视频素材','','0','1');
INSERT IGNORE INTO `onethink_menu` VALUES ('523','从微信下载','520','2','Admin/WechatVideo/downloadVideo','0','从微信下载视频素材','','0','1');

UPDATE `onethink_addons` SET `status` = '1' WHERE `id` = '4' limit 1;
DELETE FROM `onethink_addons` WHERE `id` = '302';
INSERT INTO `onethink_addons` VALUES ('302', 'RecommendedApps', '推荐应用', '我们向您推荐诸多有用的应用信息', '1', '{\"title\":\"\\u63a8\\u8350\\u5e94\\u7528\",\"width\":\"2\",\"display\":\"1\"}', 'Tealun Du', '0.1', '1380273962', '0');
UPDATE `onethink_hooks` SET `addons` = 'SiteStat,SystemInfo,RecommendedApps' WHERE `id` = '13' LIMIT 1;
DELETE FROM `onethink_hooks` WHERE `id` = '30';

DELETE FROM `onethink_tchat_mail` WHERE `id` in (2,3,4);
INSERT INTO `onethink_tchat_mail` VALUE ('2','change_profile','账户信息更新','请注意！您在{webSite}的注册信息已经变更！','<p>尊敬的用户{userName}：</p><p style="text-intent:2em;">您好！</p><p style="text-intent:2em;">我们注意到，您的账户信息于<span style="color:red">{dateTime}</span>{dateTime}</span>发生过更改，如果您确定是由您自己操作的，您可以忽略本邮件，如果您没有操作过，很可能您的帐号密码已经泄漏，请尽快点击下面的链接(有效期7天)重置您的账户密码:</p><p><a href="{urlResetPassword}" target="_blank">{urlResetPassword}</a></p><p>{signName}</p>','','<br />--------<br /> 我们将竭诚为您服务<br />OneCchat 开发组','1','0','1407334493','1407334493');
INSERT INTO `onethink_tchat_mail` VALUE ('3','to_reset_password','提交密码重置邮件','{userName}重置密码申请！- {webSite}','<p>尊敬的用户{userName}：</p><p style="text-intent:2em;">您好！</p><p style="text-intent:2em;">您收到这封邮件是因为您的账户于<span style="color:red">{dateTime}</span>提交了密码重置的申请，如果是由您本人提交，请在24小时内，点击下面的链接重置您的登录密码:</p><p><a href="{urlResetPassword}" target="_blank">{urlResetPassword}</a></p><p>如果您没有提交过重置密码的申请，请您忽略本邮件。</p><p>{signName}</p>','','<br />--------<br /> 我们将竭诚为您服务<br />OneCchat 开发组','1','0','1407334493','1407334493');
INSERT INTO `onethink_tchat_mail` VALUE ('4','reset_password','密码已经重置邮件','请注意！您在{webSite}的密码已经重置！','<p>尊敬的用户{userName}：</p><p style="text-intent:2em;">您好！</p><p style="text-intent:2em;">我们注意到，您的账户于<span style="color:red">{dateTime}</span>操作了密码重置，如果您确定本次重置是由您本次操作的，您可以忽略本邮件，如果您没有操作过，很可能您的帐号密码已经泄漏，请尽快点击下面的链接(有效期7天)重置您的账户密码:</p><p><a href="{urlResetPassword}" target="_blank">{urlResetPassword}</a></p><p>{signName}</p>','','<br />--------<br /> 我们将竭诚为您服务<br />OneCchat 开发组','1','0','1407334493','1407334493');
