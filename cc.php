<?php

//清文件缓存
$dirs = array(
   '缓存目录'=> './Runtime/Cache' ,//运行缓存目录
   '运行日志'=> './Runtime/Logs' ,//运行日志目录
   '临时目录'=> './Runtime/Temp' ,//临时目录
   '缓存字段'=>'./Runtime/Data/_fields'//缓存字段列表
);
    echo '<div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h3 id="myModalLabel">清理结果</h3>
    </div>
    <div class="modal-body">';
//清理缓存
foreach ( $dirs as $key => $value ) {
    rmdirr($value);
    echo '<div class="alert alert-info">
            <p> '.date('Y-m-d H:i:s').' <strong>'.$key.'</strong> 清理完毕! </p>
            </div>';
}

    echo '</div>
    <div class="modal-footer">
        <button class="btn" data-dismiss="modal" aria-hidden="true">关闭</button>
    </div>';

@mkdir($dirname , 0777 , true);

function rmdirr($dirname) {
    if ( !file_exists($dirname) ) {
        return false;
    }
    if ( is_file($dirname) || is_link($dirname) ) {
        return unlink($dirname);
    }
    $dir = dir($dirname);
    if ( $dir ) {
        while ( false !== $entry = $dir->read() ) {
            if ( $entry == '.' || $entry == '..' ) {
                continue;
            }
            rmdirr($dirname . DIRECTORY_SEPARATOR . $entry);
        }
    }
    $dir->close();

    return rmdir($dirname);
}

function U() {
    return false;
}

?>