<?php

namespace Addons\RecommendedApps;
use Common\Controller\Addon;

/**
 * 推荐应用插件
 * @author Tealun Du
 */

    class RecommendedAppsAddon extends Addon{

        public $info = array(
            'name'=>'RecommendedApps',
            'title'=>'推荐应用',
            'description'=>'我们向您推荐诸多有用的应用信息',
            'status'=>1,
            'author'=>'Tealun Du',
            'version'=>'0.1'
        );

        public $admin_list = array(
            'model'=>'RecommendedApps',		//要查的表
			'fields'=>'*',			//要查的字段
			'map'=>'',				//查询条件, 如果需要可以在插件类的构造方法里动态重置这个属性
			'order'=>'id desc',		//排序,
			'list_grid'=>array( 		//这里定义的是除了id序号外的表格里字段显示的表头名和模型一样支持函数和链接
                'cover_id|preview_pic:封面图标',
                'name:应用标识',
                'title:应用名称',
                'description:应用描述',
                'url:链接地址',
                'update_time|time_format:更新时间',
                'id:操作:[EDIT]|编辑,[DELETE]|删除'
            ),
        );

        public function install(){
//            $sql = "
//            -- -----------------------------
//            -- Table structure for `onethink_recommended_apps`
//            -- -----------------------------
//            DROP TABLE IF EXISTS `onethink_recommended_apps`;
//            CREATE TABLE `onethink_recommended_apps` (
//                `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'ID',
//                `cover_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '封面图标',
//                `name` VARCHAR (20) NOT NULL DEFAULT '' COMMENT '应用标识',
//                `title` VARCHAR (20) NOT NULL DEFAULT '' COMMENT '应用标题',
//                `url` varchar(255) NOT NULL COMMENT '链接地址',
//                 `description` varchar(255) NOT NULL DEFAULT '' COMMENT '应用描述',
//                `update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
//                PRIMARY KEY(`id`)
//            ) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '友情链接列表';";
//
//            if(mssql_query($sql)){
//                return true;
//            }else{
//                return false;
//            };
            return true;
        }

        public function uninstall(){
//            $sql = "DROP TABLE IF EXISTS `onethink_recommended_apps`;";
//            if(mssql_query($sql)){
//                return true;
//            }else{
//                return false;
//            };
            return true;
        }

        //实现的AdminIndex钩子方法
        public function AdminIndex($param){
            $config = $this->getConfig();
            $this->assign('addons_config', $config);
            $this->display('widget');
        }

    }