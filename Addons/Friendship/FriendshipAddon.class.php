<?php

namespace Addons\Friendship;
use Common\Controller\Addon;

/**
 * DevProgress插件
 * @author Tealun Du
 */

    class FriendshipAddon extends Addon{

        public $info = array(
            'name'=>'friendship',
            'title'=>'友情链接插件',
            'description'=>'用于管理友情链接',
            'status'=>1,
            'author'=>'Tealun Du',
            'version'=>'0.1'
        );
        public function install() {
/*            $install_sql = './Addons/Friendship/install.sql';
            if (file_exists ( $install_sql )) {
                execute_sql_file ( $install_sql );
            }*/
            return true;
        }
        public function uninstall() {
/*            $uninstall_sql = './Addons/Friendship/install.sql';
            if (file_exists ( $uninstall_sql )) {
                execute_sql_file ( $uninstall_sql );
            }*/
            return true;
        }

        //实现的AdminIndex钩子方法
        public function friendshipLink($data=array()){
            $group = $data['group']?$data['group']:'default'; //如果没有传入分组，则取默认分组
            $type = $data['type']?$this->getType($data['type']):'1'; //如果没有传入类型，则取文本类型
            $map = array(
                'group'=>$group,
                'type'=>$type,
                'status'=>1
            );
            $list = M('friendship')->where($map)->order('sort desc')->select();
            $this->assign('Links',$list);
            $this->assign('Type',$type);
            $this->display('widget');
        }
        //解析链接的类型
        private function getType($type){
            if(!is_numeric($type)){
                $type = $type == 'icon'?'2':'1';//只有icon才是图标类型
            }
            return $type;
        }

    }