-- -----------------------------
-- Table structure for `onethink_friendship`
-- -----------------------------
DROP TABLE IF EXISTS `onethink_friendship`;
CREATE TABLE `onethink_friendship` (
	`id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT '友链ID',
  `pic_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '图标ID',
  `title` VARCHAR (20) NOT NULL DEFAULT '' COMMENT '标题',
	`url` varchar(255) NOT NULL COMMENT '链接地址',
	`description` varchar(255) NOT NULL DEFAULT '' COMMENT '链接描述',
	`group` varchar(255) NOT NULL DEFAULT 'default' COMMENT '分组',
	`type` tinyint(2) NOT NULL DEFAULT '2' COMMENT '类型，1文本，2图标',
	`status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态，-1为删除，0禁用，1启用',
  `sort` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '排序',
  `create_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
	`update_time` int(10) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
	PRIMARY KEY(`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT '友情链接列表';

INSERT INTO `onethink_model`  VALUES ('', 'friendship', '友情链接', '0', '', '1', '{\"1\":[\"581\",\"582\",\"583\",\"584\",\"585\",\"580\",\"586\",\"587\"]}', '1:基础', '', '', '', '','', 'title:链接标题\r\nurl:链接URL\r\ndescription:链接描述\r\ntype:链接类型\r\npic_id:链接图标\r\ngroup:链接分组\r\nstatus:状态', '10', '', '', '1396970451', '1396970451', '1', 'MyISAM');

-- Attribute of Friendship model
INSERT INTO `onethink_attribute` VALUES ('', 'pic_id', '图标', 'int(10) unsigned NOT NULL ', 'picture', '0', '链接的图片', '1', '', '0', '0', '1', '1384147827', '1383891233', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('', 'title', '标题', 'varchar(20) NOT NULL', 'string', '', '链接标题', '1', '', '0', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('', 'url', '链接地址', 'varchar(255) NOT NULL', 'string', '', '链接的网址', '1', '', '0', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('', 'description', '链接描述', 'varchar(255) NOT NULL', 'textarea', '', '简单描述', '1', '', '0', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('', 'group', '分组', 'varchar(255) NOT NULL', 'string', 'default', '设置分组', '1', '', '0', '1', '1', '1428307615', '1428307615', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('', 'type', '类型', 'tinyint(2) NOT NULL ', 'select', '2', '链接类型', '1', '1:文字\r\n2:图标', '0', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('', 'status', '状态', 'tinyint(2) NOT NULL ', 'select', '1', '是否启用', '1', '0:停用\r\n1:启用', '0', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
INSERT INTO `onethink_attribute` VALUES ('', 'sort', '排序', 'int(10) NOT NULL ', 'string', '0', '排序（同一分组有效）', '1', '', '0', '0', '1', '1407334493', '1407334493', '', '0', '', '', '', '0', '');
